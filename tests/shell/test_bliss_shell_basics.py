# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2022 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import os
import re
import sys
import glob
import logging
import contextlib
from unittest import mock
from prompt_toolkit.input.defaults import create_pipe_input
from prompt_toolkit.output import DummyOutput
from bliss.common.logtools import Elogbook
from bliss.shell.standard import elog_add
import bliss.shell.cli
import pytest
import gevent

from bliss.shell.cli.repl import (
    BlissRepl,
    PromptToolkitOutputWrapper,
    install_excepthook,
)


class DummyTestOutput(DummyOutput):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._data = []

    def write(self, text):
        self._data.append(text)

    def flush(self):
        print("".join(self._data))
        self._data = []


def _feed_cli_with_input(
    text, check_line_ending=True, local_locals=None, local_globals=None, timeout=10
):
    """
    Create a Prompt, feed it with the given user input and return the CLI
    object.

    Inspired by python-prompt-toolkit/tests/test_cli.py
    """
    # If the given text doesn't end with a newline, the interface won't finish.
    if check_line_ending:
        assert text.endswith("\r")

    with create_pipe_input() as inp:

        if local_locals is None:
            local_locals = {}

        def mylocals():
            return local_locals

        if local_globals:

            def myglobals():
                return local_globals

        else:
            myglobals = None

        try:
            inp.send_text(text)
            br = BlissRepl(
                input=inp,
                output=DummyTestOutput(),
                session="test_session",
                style="default",
                get_locals=mylocals,
                get_globals=myglobals,
            )

            def exit_after_timeout(app, timeout):
                gevent.sleep(timeout)
                app.exit()

            br.app.output = PromptToolkitOutputWrapper(br.app.output)
            gtimeout = gevent.spawn(exit_after_timeout, br.app, timeout)
            result = br.app.run()
            return result, br.app, br

        finally:
            gtimeout.kill()
            BlissRepl.instance = None  # BlissRepl is a Singleton


def run_repl_once(bliss_repl, text):
    bliss_repl.app.input.send_text(text)
    try:
        res = bliss_repl.eval(bliss_repl.app.run())
    except KeyboardInterrupt:  # KeyboardInterrupt doesn't inherit from Exception.
        raise
    except SystemExit:
        return
    except BaseException as e:
        bliss_repl._handle_exception(e)
    else:
        if res is not None:
            bliss_repl.show_result(res)


def test_shell_exit():
    try:
        _feed_cli_with_input(chr(0x4) + "y", check_line_ending=False)
    except EOFError:
        assert True


def test_shell_exit2():
    try:
        _feed_cli_with_input(chr(0x4) + "\r", check_line_ending=False)
    except EOFError:
        assert True


def test_shell_noexit():
    result, cli, br = _feed_cli_with_input(
        chr(0x4) + "nprint 1 2\r", check_line_ending=True
    )
    assert result == "print(1,2)"


def test_shell_ctrl_r():

    result, cli, br = _feed_cli_with_input(
        chr(0x12) + "bla blub\r\r", check_line_ending=True
    )
    assert result == ""

    result, cli, br = _feed_cli_with_input(
        "from bliss import setup_globals\rfrom subprocess import Popen\r"
        + chr(0x12)
        + "from bl\r\r",
        check_line_ending=True,
    )
    assert result == "from bliss import setup_globals"


def test_shell_comma_backets():
    result, cli, _ = _feed_cli_with_input("print 1 2\r")
    assert result == "print(1,2)"


def test_shell_string_input():
    result, cli, _ = _feed_cli_with_input("a='to to'\r")
    assert result == "a='to to'"


def test_shell_string_parameter():
    result, cli, _ = _feed_cli_with_input("print 'bla bla'\r")
    assert result == "print('bla bla')"


def test_shell_function_without_parameter():
    result, cli, _ = _feed_cli_with_input("print\r")
    assert result == "print"

    def f():
        pass

    result, cli, _ = _feed_cli_with_input("f\r", local_locals={"f": f})
    assert result == "f()"


def test_shell_function_with_return_only():
    result, cli, _ = _feed_cli_with_input("\r")
    assert result == ""


def test_shell_callable_with_args():
    result, cli, _ = _feed_cli_with_input("sum\r")
    assert result == "sum"

    def f(arg):
        pass

    result, cli, _ = _feed_cli_with_input("f\r", local_locals={"f": f})
    assert result == "f"


def test_shell_callable_with_kwargs_only():
    result, cli, _ = _feed_cli_with_input("property\r")
    assert result == "property()"

    def f(arg="bla"):
        pass

    result, cli, _ = _feed_cli_with_input("f\r", local_locals={"f": f})
    assert result == "f()"


def test_shell_callable_with_args_and_kwargs():
    result, cli, _ = _feed_cli_with_input("compile\r")
    assert result == "compile"

    def f(arg, kwarg="bla"):
        pass

    result, cli, _ = _feed_cli_with_input("f\r", local_locals={"f": f})
    assert result == "f"


def test_shell_list():
    result, cli, _ = _feed_cli_with_input("list\r")
    assert result == "list"

    lst = list()
    result, cli, _ = _feed_cli_with_input("lst\r", local_locals={"lst": lst})
    assert result == "lst"


def test_shell_ScanSaving(beacon):
    from bliss.scanning.scan_saving import ScanSaving

    s = ScanSaving()

    result, cli, _ = _feed_cli_with_input("s\r", local_locals={"s": s})
    assert result == "s"


def test_shell_func():
    def f():
        pass

    result, cli, _ = _feed_cli_with_input("f\r", local_locals={"f": f})
    assert result == "f()"


def test_shell_semicolon():
    result, cli, _ = _feed_cli_with_input("print 1 2;print 1\r")
    assert result == "print(1,2);print(1)"
    result, cli, _ = _feed_cli_with_input("print 1 2;print 1;print 23\r")
    assert result == "print(1,2);print(1);print(23)"


def test_shell_comma_outside_callable_assignment():
    result, cli, _ = _feed_cli_with_input("a=True \r")
    assert result == "a=True"


def test_shell_comma_outside_callable_bool():
    result, cli, _ = _feed_cli_with_input("True \r")
    assert result == "True"


def test_shell_comma_outside_callable_string():
    result, cli, _ = _feed_cli_with_input("'bla' \r")
    assert result == "'bla'"


def test_shell_comma_outside_callable_number():
    result, cli, _ = _feed_cli_with_input("1.1 + 1  \r")
    assert result == "1.1 + 1"


def test_shell_comma_after_comma():
    result, cli, _ = _feed_cli_with_input("1, \r")
    assert result == "1,"


@contextlib.contextmanager
def bliss_repl(locals_dict):
    with create_pipe_input() as inp:

        def mylocals():
            return locals_dict

        try:
            br = BlissRepl(
                input=inp,
                output=DummyOutput(),
                session="test_session",
                style="default",
                get_locals=mylocals,
            )
            yield inp, br
        finally:
            BlissRepl.instance = None  # BlissRepl is a Singleton


def test_protected_against_trailing_whitespaces(beacon):
    """Check that the number of spaces (N) after a command doesn't  make the command to be repeated N-1 times"""

    def f():
        print("Om Mani Padme Hum")

    result, cli, br = _feed_cli_with_input(f"f() {' '*5}\r", local_locals={"f": f})

    output = cli.output
    br.eval(result)

    assert output[-1].strip() == "Om Mani Padme Hum"


def test_info_dunder():
    class A:
        def __repr__(self):
            return "repr-string"

        def __str__(self):
            return "str-string"

        def __info__(self):
            return "info-string"

        def titi(self):
            return "titi-method"

    class B:
        def __repr__(self):
            return "repr-string"

    class C:
        pass

    # '__info__()' method called at object call.
    with bliss_repl({"A": A(), "B": B(), "C": C()}) as bliss_repl_ctx:
        inp, br = bliss_repl_ctx
        run_repl_once(br, "A\r")
        assert "info-string" in br.app.output[-1]

        run_repl_once(br, "[A]\r")
        assert "[repr-string]" in br.app.output[-1]

        # 2 parenthesis added to method if not present
        run_repl_once(br, "A.titi\r")
        assert "titi-method" in br.app.output[-1]

        # Closing parenthesis added if only opening one is present.
        run_repl_once(br, "A.titi(\r")
        assert "titi-method" in br.app.output[-1]

        # Ok if finishing by a closing parenthesis.
        run_repl_once(br, "A.titi()\r")
        assert "titi-method" in br.app.output[-1]

        # '__repr__()' used if no '__info__()' method is defined.
        run_repl_once(br, "B\r")
        assert "repr-string" in br.app.output[-1]

        # Default behaviour for object without specific method.
        run_repl_once(br, "C\r")
        assert "C object at " in br.app.output[-1]

    bliss.shell.cli.typing_helper_active = False
    try:
        with bliss_repl({"A": A, "B": B, "A.titi": A.titi}) as bliss_repl_ctx:
            inp, br = bliss_repl_ctx
            output = br.app.output
            run_repl_once(br, "A\r")
            assert (
                "<class 'test_bliss_shell_basics.test_info_dunder.<locals>.A'>\r\n\n"
                == output[-1]
            )
    finally:
        bliss.shell.cli.typing_helper_active = True


def test_shell_dict_list_not_callable():
    result, cli, _ = _feed_cli_with_input("d \r", local_locals={"d": dict()})
    assert result == "d"


def test_property_evaluation():
    class Bla:
        def __init__(self):
            self.i = 0

        @property
        def test(self):
            self.i += 1
            return self.i

    b = Bla()

    result, cli, _ = _feed_cli_with_input("b.test     \r", local_locals={"b": b})
    assert b.test == 1
    result, cli, _ = _feed_cli_with_input("b.test;print 1\r", local_locals={"b": b})
    result, cli, _ = _feed_cli_with_input("b.test;print 1\r", local_locals={"b": b})
    result, cli, _ = _feed_cli_with_input("b.test;print 1\r", local_locals={"b": b})
    assert b.test == 2


def test_func_no_args():
    f = lambda: None
    result, cli, _ = _feed_cli_with_input("f \r", local_locals={"f": f})
    assert result == "f()"


def _repl_out_to_string(out):
    ansi_escape = re.compile(r"\x1B[@-_][0-?]*[ -/]*[@-~]")
    return ansi_escape.sub("", out)


def test_nested_property_evaluation():
    class A:
        def __init__(self):
            self.count = 0

        @property
        def foo(self):
            self.count += 1
            return self.count

    class B:
        def __init__(self):
            self.a = A()

        @property
        def bar(self):
            return self.a

    b = B()

    result, cli, _ = _feed_cli_with_input("b.bar.foo\r", local_locals={"b": b})
    result, cli, _ = _feed_cli_with_input("b.bar.foo\r", local_locals={"b": b})
    assert b.bar.foo == 1


def test_deprecation_warning(beacon, capfd, log_context):
    def test_deprecated():
        print("bla")
        from bliss.common.deprecation import deprecated_warning

        deprecated_warning(
            kind="function",
            name="ct",
            replacement="sct",
            reason="`ct` does no longer allow to save data",
            since_version="1.5.0",
            skip_backtrace_count=5,
            only_once=False,
        )

    with bliss_repl({"func": test_deprecated}) as bliss_repl_ctx:
        inp, br = bliss_repl_ctx
        inp.send_text("func()\r")
        result = br.app.run()
        br.eval(result)
        captured = capfd.readouterr()

    out = _repl_out_to_string(captured.out)
    err = _repl_out_to_string(captured.err)
    assert "bla" in out
    assert "Function ct is deprecated since" in err


def test_captured_output():
    def f(num):
        print(num + 1)
        return num + 2

    def fnone():
        pass

    def fnoneprint():
        print("hello\nworld")
        pass

    def decorated_cell_output(s):
        return f"{s}\r\n\n"

    with bliss_repl(
        {"f": f, "fnone": fnone, "fnoneprint": fnoneprint}
    ) as bliss_repl_ctx:
        br = bliss_repl_ctx[1]
        output = br.app.output

        run_repl_once(br, "f(1)\r")
        run_repl_once(br, "f(3)\r")
        run_repl_once(br, "fnone()\r")
        run_repl_once(br, "f(4)\r")
        run_repl_once(br, "fnoneprint()\r")
        run_repl_once(br, "f(5)\r")

        assert output[1] == decorated_cell_output("2\n3")
        assert output[2] == decorated_cell_output("4\n5")
        assert output[3] is None
        assert output[4] == decorated_cell_output("5\n6")
        assert output[5] == "hello\nworld\n"
        assert output[6] == decorated_cell_output("6\n7")

        assert output[-1] == decorated_cell_output("6\n7")
        assert output[-2] == "hello\nworld\n"
        assert output[-3] == decorated_cell_output("5\n6")
        assert output[-4] is None
        assert output[-5] == decorated_cell_output("4\n5")
        assert output[-6] == decorated_cell_output("2\n3")

        with pytest.raises(
            IndexError, match=re.escape("the first available cell is OUT [1]")
        ):
            output[-7]
        with pytest.raises(IndexError, match=re.escape("the last cell is OUT [6]")):
            output[7]
        with pytest.raises(
            IndexError, match=re.escape("the first available cell is OUT [1]")
        ):
            output[0]

        # Fill the output history with None's
        MAXLEN = output._MAXLEN
        for _ in range(MAXLEN - 6):
            run_repl_once(br, "fnone()\r")

        assert output[1] == decorated_cell_output("2\n3")

        # Push the first entry out of the history
        run_repl_once(br, "fnone()\r")

        with pytest.raises(
            IndexError, match=re.escape("the first available cell is OUT [2]")
        ):
            output[1]

        # Push the second entry out of the history
        run_repl_once(br, "fnone()\r")

        with pytest.raises(
            IndexError, match=re.escape("the first available cell is OUT [3]")
        ):
            output[1]

        with pytest.raises(
            IndexError, match=re.escape("the first available cell is OUT [3]")
        ):
            output[-MAXLEN - 3]
        with pytest.raises(
            IndexError, match=re.escape(f"the last cell is OUT [{MAXLEN+2}]")
        ):
            output[MAXLEN + 3]
        with pytest.raises(
            IndexError, match=re.escape("the first available cell is OUT [3]")
        ):
            output[0]


def test_elogbook_elog_add(beacon, shell_excepthook):
    def return_arg(s):
        return s

    results = [None, "hello", None, "hello\nworld"]

    def assert_elog_add(idx, *expected_args, **expected_kw):
        nonlocal results, br
        with mock.patch.object(
            Elogbook, "comment", return_value=None
        ) as mock_elogbook_comment:
            if idx is None:
                idx = -1
                run_repl_once(br, "elog_add()\r")
            else:
                run_repl_once(br, f"elog_add({idx})\r")

        if not expected_args:
            # The output of this cell is empty
            mock_elogbook_comment.assert_not_called()
        else:
            # The output of this cell is not empty
            mock_elogbook_comment.assert_called_once_with(*expected_args, **expected_kw)
        results.append(None)  # output of the elog_add call itself

    def decorated_cell_output(s):
        return f"{repr(s)}\r\n\n"

    repl_locals = {"return_arg": return_arg, "elog_add": elog_add}
    with bliss_repl(repl_locals) as bliss_repl_ctx:
        br = bliss_repl_ctx[1]

        for s in results:
            if s is None:
                run_repl_once(br, "return_arg(None)\r")
            else:
                run_repl_once(br, f"return_arg({repr(s)})\r")

        # `elog_add()` logs the last output by default
        assert_elog_add(None, decorated_cell_output("hello\nworld"), beamline_only=None)

        # `elog_add(i)` index should correspond to the cell index
        assert_elog_add(1)
        assert_elog_add(2, decorated_cell_output("hello"), beamline_only=None)
        assert_elog_add(3)
        assert_elog_add(4, decorated_cell_output("hello\nworld"), beamline_only=None)

        idx = 4 - len(results) - 1
        assert results[idx] == "hello\nworld"
        assert_elog_add(idx, decorated_cell_output("hello\nworld"), beamline_only=None)

        idx = 2 - len(results) - 1
        assert results[idx] == "hello"
        assert_elog_add(idx, decorated_cell_output("hello"), beamline_only=None)


def test_elogbook_cmd_log_and_elog_add(beacon, shell_excepthook):
    logger = logging.getLogger()

    def f(num):
        print(num + 1)
        logger.info("my info")
        logger.error("my error")
        return num + 2

    with bliss_repl({"f": f, "elog_add": elog_add}) as bliss_repl_ctx:
        br = bliss_repl_ctx[1]

        # calling when no command has been issued yet should not raise exception
        elog_add()

        with mock.patch.object(
            Elogbook, "command", return_value=None
        ) as mock_elogbook_command:
            run_repl_once(br, "f(1)\r")

            output = br.app.output
            captured = output[-1]

        mock_elogbook_command.assert_called_once_with("f(1)")
        assert captured == "2\n3\r\n\n"

        with mock.patch.object(
            Elogbook, "comment", return_value=None
        ) as mock_elogbook_comment:
            run_repl_once(br, "elog_add()\r")

        mock_elogbook_comment.assert_called_once_with(captured, beamline_only=None)

        with mock.patch.object(
            Elogbook, "error", return_value=None
        ) as mock_elogbook_error:
            run_repl_once(br, "1/0\r")

        mock_elogbook_error.assert_called_once_with(
            "ZeroDivisionError: division by zero"
        )

        with mock.patch.object(
            Elogbook, "comment", return_value=None
        ) as mock_elogbook_comment:
            run_repl_once(br, "elog_add()\r")

        # The last cell raised a ZeroDivisionError so there
        # is no output for the logbook.
        mock_elogbook_comment.assert_not_called()


def test_getattribute_evaluation():
    n = None

    class A:
        def __init__(self):
            global n
            n = 0

        def __getattribute__(self, value):
            global n

            if n < 1:
                n += 1
                raise RuntimeError
            return 0

    a = A()

    with gevent.timeout.Timeout(3):
        result, cli, _ = _feed_cli_with_input("a.foo()\r", local_globals={"a": a})


@pytest.fixture
def shell_excepthook():
    orig_excepthook = sys.excepthook
    try:
        install_excepthook()
        yield
    finally:
        sys.excepthook = orig_excepthook


def test_excepthook(shell_excepthook, default_session):
    print_output = []

    def test_print(*msg, **kw):
        print_output.append("\n".join(msg))

    logging.getLogger("exceptions").setLevel(
        1000
    )  # this is to silent exception logging via logger (which also calls 'print')

    with mock.patch("builtins.print", test_print):
        try:
            raise RuntimeError("excepthook test")
        except RuntimeError:
            sys.excepthook(*sys.exc_info())

    assert (
        "".join(print_output)
        == "!!! === RuntimeError: excepthook test === !!! ( for more details type cmd 'last_error()' )"
    )


def test_history_archiving(tmpdir):
    history_filename = tmpdir / ".bliss_pytest_dummy_history"

    # ensure there exist no such history files before testing
    for p in glob.glob(f"{history_filename}*"):
        os.remove(p)

    def dummy_entry(id):
        return f"# 1970-01-01 12:00:{id}.000000\n+dummy_cmd_{id}()\n\n"

    # create a fifty entries history file
    history = "".join([dummy_entry(i) for i in range(50)])
    with open(history_filename, "w") as f:
        f.write(history)

    # file size < threshold : no archiving
    bliss.shell.cli.repl._archive_history(history_filename, file_size_thresh=3000)
    with open(history_filename, "r") as f:
        assert f.read() == history

    # file size > threshold : archiving
    bliss.shell.cli.repl._archive_history(
        history_filename, file_size_thresh=2000, keep_active_entries=15
    )
    with open(history_filename, "r") as f:
        assert f.read() == "".join([dummy_entry(i) for i in range(50 - 15, 50)])

    archive_file = glob.glob(f"{history_filename}_*")[0]
    with open(archive_file, "r") as f:
        assert f.read() == "".join([dummy_entry(i) for i in range(50 - 15)])
