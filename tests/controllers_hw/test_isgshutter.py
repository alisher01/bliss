import pytest
from bliss.controllers.isgshutter import IsgShutter
from bliss.common.shutter import BaseShutterState


@pytest.fixture
def closed_isgshutter(mocker):
    """
    Create a IsgShutter using a simulated serial line.

    It behave as a closed shutter.
    """

    class Serial:
        def write_readline(self, msg):
            msg = msg.strip()
            if msg == b"?STATE":
                return b"ADOWN_BDOWN"
            assert False, "Unknown request %a" % msg
            return b""

    mocker.patch("bliss.controllers.isgshutter.get_comm", return_value=Serial())
    isg = IsgShutter("foo", {"closing_time": 1, "opening_time": 1})
    return isg


def test_is_open(closed_isgshutter):
    assert not closed_isgshutter.is_open


def test_is_closed(closed_isgshutter):
    assert closed_isgshutter.is_closed


def test_state(closed_isgshutter):
    assert closed_isgshutter.state == BaseShutterState.CLOSED
    assert closed_isgshutter.state_string == "Closed"
