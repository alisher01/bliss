# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2020 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import pytest
import os
import gevent

from bliss.common.scans import loopscan, DEFAULT_CHAIN
from bliss.controllers.lima.limatools import limatake
from bliss.testutils.process_utils import start_tango_server

# ==== TEST SUITE FOR LIMA: BCU LABO (lid00limace) =======

# === test performed with the maxipix camera (bcu_mpx_tpxatl25)


@pytest.fixture
def nexus_writer_service(ports):

    device_name = "id00/bliss_nxwriter/__DEFAULT__"
    device_fqdn = "tango://localhost:{}/{}".format(ports.tango_port, device_name)

    with start_tango_server(
        "NexusWriterService", "test", "--log", "warning", device_fqdn=device_fqdn
    ) as dev_proxy:
        yield device_fqdn, dev_proxy


def print_scan_info(scan, *objs):

    print(f"\n=== SCAN INFO: {scan}")

    print(" \n* DEFAULT_CHAIN: \n", DEFAULT_CHAIN._settings)

    acqch = scan.acq_chain
    print(" \n* ACQ_TREE\n", acqch.tree)

    for obj in objs:
        acq_obj = list(acqch.get_node_from_devices(obj))[0]
        acqparams = acq_obj.acq_params
        print("object:", obj.name)
        for k, v in acqparams.items():
            print(f"   {k}: {v}")


def check_h5(npoints, scan, camname):
    data = scan.get_data("image")
    assert len(data) == npoints

    references = data.all_image_references()

    for i, (filename, _, image_nr, _) in enumerate(references):
        expected = f"{camname}"
        expected += "_{:04d}.h5".format(i)
        assert os.path.basename(filename) == str(expected)
        assert image_nr == 0  # each file only contains 1 image

    assert i == npoints - 1


@pytest.fixture
def get_test_objects(default_session, nexus_writer_service):

    # load objects
    cam = default_session.config.get("bcu_mpx_tpxatl25")
    rot1 = default_session.config.get("rot1")
    rot2 = default_session.config.get("rot2")

    for mot in (rot1, rot2):
        mot.sync_hard()
        mot.position = 0
        mot.offset = 0
        mot.velocity = 25
        mot.acceleration = 100

    # get camera mapped path and ensure path exists
    scan_saving = default_session.scan_saving
    print("\n=== scan saving base path:", scan_saving.base_path)
    mapped_path = cam.get_mapped_path(scan_saving.get_path())
    os.makedirs(mapped_path, exist_ok=True)
    print("\n=== Image saving path:", mapped_path)

    cam.saving.file_format = "HDF5"
    # ensure one frame_per_file to be able to use check_h5 to count the number of produced images
    cam.saving.mode = "ONE_FILE_PER_FRAME"
    scan_saving.writer = "nexus"

    print("\n", cam.saving.__info__())
    print("\n", cam.acquisition.__info__())
    yield default_session, cam, rot1, rot2


@pytest.mark.parametrize("mode", ["INTERNAL_TRIGGER_MULTI", "INTERNAL_TRIGGER"])
def test_lima_scan_software_triggering(get_test_objects, mode):

    default_session, cam, rot1, rot2 = get_test_objects
    npoints, expo = 10, 0.1

    # Test1: INTERNAL_TRIGGER_MULTI (default)
    DEFAULT_CHAIN.clear()
    DEFAULT_CHAIN.set_settings(
        [{"device": cam, "acquisition_settings": {"acq_trigger_mode": mode}}]
    )
    with gevent.Timeout(10, RuntimeError("Timeout waiting for end of scan")):
        s = loopscan(npoints, expo, cam.image, save=True)
    print_scan_info(s, cam)
    assert cam.acquisition.trigger_mode == mode
    check_h5(npoints, s, cam.name)


def test_limatake(get_test_objects):
    # limatake does not use default chain, it creates its own chain
    _, cam, _, _ = get_test_objects
    npoints, expo = 10, 0.1
    with gevent.Timeout(10, RuntimeError("Timeout waiting for end of scan")):
        s = limatake(expo, npoints, cam, save=True)
    print_scan_info(s, cam)
    check_h5(npoints, s, cam.name)


@pytest.mark.parametrize(
    "mode", ["INTERNAL_TRIGGER_MULTI", "EXTERNAL_TRIGGER_MULTI", "EXTERNAL_GATE"]
)
def test_lima_scan_hardware_triggering(get_test_objects, mode):

    default_session, cam, rot1, rot2 = get_test_objects
    npoints, expo = 10, 0.1

    DEFAULT_CHAIN.clear()

    # prepare default chain configurations
    chain_cfgs = {
        "EXTERNAL_TRIGGER_MULTI": default_session.config.get("test_chain_trig")[
            "chain_config"
        ],
        "EXTERNAL_GATE": default_session.config.get("test_chain_gate")["chain_config"],
    }
    DEFAULT_CHAIN.set_settings(chain_cfgs.get(mode, {}))

    # ensure signals are properly setup
    opiom = default_session.config.get("multiplexer_opiom1")
    opiom.switch("ACQ_TRIG_MODE", "COUNT")

    with gevent.Timeout(5, RuntimeError("Timeout waiting for end of scan")):
        s = loopscan(npoints, expo, cam, save=True)
    print_scan_info(s, cam)
    assert cam.acquisition.trigger_mode == mode
    check_h5(npoints, s, cam.name)


@pytest.mark.parametrize(
    "scan_type", ["ftimescan", "ftimescanlookup", "fscan", "fscan2d"]
)
def test_lima_fscans(get_test_objects, scan_type):
    default_session, cam, rot1, rot2 = get_test_objects

    timeout = 3
    total_npoints = 0
    scan_args = tuple()

    if scan_type == "ftimescan":
        npoints = 10
        scan_args = (0.1, npoints)
        total_npoints = npoints
    elif scan_type == "fscan":
        npoints = 10
        scan_args = (rot1, 0, 0.1, npoints, 0.1)
        total_npoints = npoints
    elif scan_type == "ftimescanlookup":
        timeout = 10
        npoints = 5
        scan_args = (((rot1, [0.1, 0.2, 0.3]), (rot2, [0, 1, 2])), 0.05, npoints)
        total_npoints = npoints * 3
    elif scan_type == "fscan2d":
        timeout = 15
        npoints = 5
        scan_args = (rot2, 0, 0.2, 2, rot1, 3, 0.01, npoints, 0.005)
        total_npoints = npoints * 2

    assert all((total_npoints, scan_args))

    DEFAULT_CHAIN.clear()

    # ensure signals are properly setup
    opiom = default_session.config.get("multiplexer_opiom1")
    opiom.switch("ACQ_TRIG_MODE", "FSCAN")

    fscan = default_session.config.get("fscan_test")
    scan_cmd = getattr(fscan, scan_type)

    with gevent.Timeout(timeout, RuntimeError("Timeout waiting for end of scan")):
        scan_cmd(*scan_args)
    s = default_session.scans[-1]
    check_h5(total_npoints, s, cam.name)

    # fscan.fscan3d
    # fscan.f2scan
    # fscan.fsweep
    # fscan.finterlaced
