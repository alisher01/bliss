# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2022 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import pytest
import numpy
import gevent
import logging

from bliss.physics.backend import MaterialBackend
from bliss.controllers.lima.roi import Roi
from bliss.data.scan import ScansWatcher, ScansObserver
from bliss.common.standard import ct


@pytest.fixture
def autof_session(beacon, scan_tmpdir, lima_simulator):
    mono = beacon.get("mono")
    mono.move(7)

    autof_session = beacon.get("test_autof_session")
    autof_session.setup()
    autof_session.scan_saving.base_path = str(scan_tmpdir)
    autof_session.scan_saving.writer = "null"

    logger = logging.getLogger("global.controllers.autofwheel_cu")
    logger.setLevel(logging.DEBUG)
    logger = logging.getLogger("bliss.common.auto_filter.chain_patching_inplace")
    logger.setLevel(logging.DEBUG)

    roi = Roi(0, 0, 1023, 1023)
    lima_simulator = autof_session.env_dict["lima_simulator"]
    lima_simulator.roi_counters["roi1"] = roi

    yield autof_session
    autof_session.close()


def test_autofilter_api(autof_session):
    # just call once all functions that are relevant to the user in autofilter1
    # and autofwheel_cu that are not related to a scan

    # get ready
    energy = autof_session.env_dict["energy"]
    energy.move(16)

    autofilter1 = autof_session.env_dict["autofilter1"]
    filterw = autof_session.env_dict["autofwheel_cu"]

    # no filter
    filterw.filter = 0
    assert autofilter1.transmission == 1

    # 20 micron copper
    # http://henke.lbl.gov/optical_constants/filter2.html
    # Cu Density=8.96 Thickness=20. microns
    #  Photon Energy (eV), Transmission
    #     16000.      0.32744
    filterw.filter = 2
    if MaterialBackend.BACKEND_NAME == "xraylib":
        rtol = 0.5  # % relative difference
    else:
        # Large difference due to photoelectric cross section
        rtol = 7  # % relative difference
    assert autofilter1.transmission == pytest.approx(0.32744, rel=rtol / 100)

    # change energy and see if transmission is updated
    filterw.filter = 2
    trans_16keV = autofilter1.transmission
    energy.move(12)
    trans_12keV = autofilter1.transmission
    assert trans_16keV != trans_12keV

    # set monitor counter
    diode = autof_session.config.get("diode")
    autofilter1.monitor_counter = diode


def perform_autofilter_scan(autof_session, *detectors, save=True, save_images=True):
    energy = autof_session.env_dict["energy"]
    energy.move(16)

    filterw = autof_session.env_dict["autofwheel_cu"]
    filterw.filter = 0
    sim = autof_session.env_dict["sim_autofilter1_ctrs"]

    with gevent.Timeout(20):  # normal time around 7 seconds
        scan = sim.auto_filter.ascan(
            sim.axis,
            0,
            1,
            sim.npoints,
            0.1,
            *sim.detectors,
            *detectors,
            save=save,
            save_images=save_images,
        )
    return sim, scan


def assert_sim_data(sim, scan_data, save=True, save_images=True):
    # check that all channels have the desired number of points
    nexpected = sim.npoints + 1
    for name, data in scan_data.items():
        if name.endswith(":image"):
            if not save_images:
                assert len(data) == 0, name
                continue
        assert len(data) == nexpected, name

    # check to positioner values
    measured = scan_data["axis:AutoFilterDetMon"]
    expected = numpy.linspace(0, 1, nexpected)
    numpy.testing.assert_allclose(measured, expected)

    # check that the corrected values are the expected ones
    measured = scan_data["autofilter1:sim_autofilter1_det_corr"]
    expected = sim._data * sim.mon_value()
    numpy.testing.assert_allclose(measured, expected)

    # check that corrected detector counter
    measured = scan_data["autofilter1:sim_autofilter1_det_corr"]
    expected = numpy.asarray(
        scan_data["AutoFilterDetMon:sim_autofilter1_det"]
    ) / numpy.asarray(scan_data["autofilter1:transm"])
    numpy.testing.assert_allclose(measured, expected)

    # check that all filters are out at beginning and end of scan
    assert scan_data["autofilter1:transm"][0] == 1.0
    assert scan_data["autofilter1:transm"][-1] == 1.0


def test_autofilter_ascan(autof_session):
    sim, scan = perform_autofilter_scan(autof_session, save=False)
    scan_data = scan.get_data()

    expected = {
        "axis:AutoFilterDetMon",
        "timer:elapsed_time",
        "timer:epoch",
        "AutoFilterDetMon:sim_autofilter1_mon",
        "AutoFilterDetMon:sim_autofilter1_det",
        "autofilter1:sim_autofilter1_det_corr",
        "autofilter1:curratt",
        "autofilter1:transm",
        "autofilter1:ratio",
    }
    assert set(scan_data.keys()) == expected
    assert_sim_data(sim, scan_data, save=False)


class AutofScanObserver(ScansObserver):
    def __init__(self, end_callback):
        self._end_callback = end_callback
        self.data = dict()
        self.lima_info = dict()
        self.scan_info = dict()
        self.images_per_file = -1

    def add_data(self, channel_name, data_bunch):
        data = self.data.setdefault(channel_name, list())
        data.extend(data_bunch)

    def on_scan_started(self, scan_db_name, scan_info):
        self.scan_info = scan_info

    def on_scan_finished(self, scan_db_name, scan_info):
        self._end_callback()

    def on_scalar_data_received(self, scan_db_name, channel_name, index, data_bunch):
        self.add_data(channel_name, data_bunch)

    def on_ndim_data_received(self, scan_db_name, channel_name, dim, index, data_bunch):
        self.add_data(channel_name, data_bunch)

    def on_lima_ref_received(
        self, scan_db_name, channel_name, dim, source_node, event_data
    ):
        self.add_data(channel_name, event_data.data)
        self.lima_info = source_node.info.get_all()
        if self.images_per_file == -1:
            self.images_per_file = source_node.images_per_file


@pytest.mark.parametrize("save", (True, False))
@pytest.mark.parametrize(" save_images", (True, False))
def test_autofilter_publishing(save, save_images, autof_session):
    watcher = ScansWatcher(autof_session.name)
    observer = AutofScanObserver(watcher.stop)
    watcher.set_observer(observer)

    gwatcher = gevent.spawn(watcher.run)
    watcher.wait_ready()

    lima_simulator = autof_session.env_dict["lima_simulator"]
    if save_images:
        frames_per_file = 10
        lima_simulator.saving.initialize()
        lima_simulator.saving.file_format = "HDF5"
        lima_simulator.saving.mode = "ONE_FILE_PER_N_FRAMES"
        lima_simulator.saving.frames_per_file = frames_per_file
    else:
        frames_per_file = None

    mca = autof_session.env_dict["simu1"]
    mca.rois.set("roi1", 100, 110)

    diode = autof_session.env_dict["diode"]

    sim, _ = perform_autofilter_scan(
        autof_session, lima_simulator, mca, diode, save=save, save_images=save_images
    )
    gwatcher.get(timeout=3)
    scan_data = observer.data

    expected = {
        "axis:AutoFilterDetMon",
        "timer:elapsed_time",
        "timer:epoch",
        "AutoFilterDetMon:sim_autofilter1_mon",
        "AutoFilterDetMon:sim_autofilter1_det",
        "autofilter1:sim_autofilter1_det_corr",
        "autofilter1:curratt",
        "autofilter1:transm",
        "autofilter1:ratio",
        "lima_simulator:image",
        "lima_simulator:roi_counters:roi1_sum",
        "lima_simulator:roi_counters:roi1_avg",
        "lima_simulator:roi_counters:roi1_std",
        "lima_simulator:roi_counters:roi1_min",
        "lima_simulator:roi_counters:roi1_max",
        "simu1:roi1",
        "simulation_diode_sampling_controller:diode",
    }
    for i in range(4):
        expected |= {
            f"simu1:deadtime_det{i}",
            f"simu1:trigger_livetime_det{i}",
            f"simu1:triggers_det{i}",
            f"simu1:events_det{i}",
            f"simu1:realtime_det{i}",
            f"simu1:energy_livetime_det{i}",
            f"simu1:icr_det{i}",
            f"simu1:ocr_det{i}",
            f"simu1:spectrum_det{i}",
            f"simu1:roi1_det{i}",
        }
    assert set(scan_data.keys()) == expected

    npoints = sim.npoints + 1
    for name, values in scan_data.items():
        if name == "lima_simulator:image" and not save_images:
            assert len(values) == 0, name
        else:
            assert len(values) == npoints, name

    measured = scan_data["lima_simulator:roi_counters:roi1_max"]
    expected = numpy.arange(61) * 100 + 100
    invalid_points = [18, 20, 22, 24, 27, 34, 36, 38, 40, 43]
    expected[invalid_points] = -1
    expected = expected[expected != -1]
    numpy.testing.assert_array_equal(measured, expected)

    assert_sim_data(sim, scan_data, save=save, save_images=save_images)

    assert observer.scan_info.get("npoints") == npoints
    assert observer.images_per_file == frames_per_file
    assert observer.lima_info.get("saving_frame_per_file") == frames_per_file
    assert observer.lima_info.get("shape") == (1024, 1024)
    assert observer.lima_info.get("dtype") is numpy.uint32


def test_ct_issue_2894(autof_session):
    autof = autof_session.env_dict["autofilter1"]
    ct_scan = ct(0.1, autof, run=False)
    ct_task = gevent.spawn(ct_scan.run)

    ct_task.join()

    assert ct_task.successful()
    for cnt in autof.counters:
        assert isinstance(ct_scan.get_data(cnt.name), numpy.ndarray)


def test_filterset_counters(autof_session):
    autofwheel_cu = autof_session.env_dict["autofwheel_cu"]
    autofwheel_pos_counter = autofwheel_cu.counters.autofwheel_cu_position
    autofwheel_transm_counter = autofwheel_cu.counters.autofwheel_cu_transm
    assert autofwheel_pos_counter
    assert autofwheel_transm_counter

    autofwheel_cu.filter = 3

    ct_scan = ct(0, autofwheel_cu, run=False)
    ct_scan.run()

    assert ct_scan.get_data()[autofwheel_pos_counter.name] == 3
    # assert ct_scan.get_data()[autofwheel_transm_counter.name]
