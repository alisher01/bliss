# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2022 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import pytest
import gevent
import subprocess
import sys
import os

from bliss.common.logtools import userlogger
from bliss.common.shutter import BaseShutterState
from bliss.comm.rpc import Client
from bliss.common import event
from .sim_shutter_test import get_simulation_shutter, SimulationShutter


@pytest.fixture
def simulation_shutter():
    return get_simulation_shutter()


@pytest.fixture
def simulation_shutter_in_another_process(beacon, beacon_host_port):
    p = subprocess.Popen(
        [
            sys.executable,
            "-u",
            os.path.join(os.path.dirname(__file__), "sim_shutter_test.py"),
            f"{beacon_host_port[0]}",
            f"{beacon_host_port[1]}",
        ],
        stdout=subprocess.PIPE,
    )
    line = p.stdout.readline()  # synchronize process start
    try:
        port = int(line)
    except ValueError:
        raise RuntimeError("server didn't start")

    shutter_in_another_process_proxy = Client(f"tcp://localhost:{port}")

    yield p, shutter_in_another_process_proxy

    p.terminate()
    shutter_in_another_process_proxy._rpc_connection.close()


def test_simulation_shutter(
    beacon, simulation_shutter, simulation_shutter_in_another_process
):
    _, external_simulation_shutter = simulation_shutter_in_another_process
    assert external_simulation_shutter.state_string == simulation_shutter.state_string
    assert simulation_shutter.state_string == "Closed"

    simulation_shutter.open()

    assert simulation_shutter.state_string == "Open"

    with gevent.Timeout(3):
        while external_simulation_shutter.is_closed:
            # let some time for channel propagation to the external process
            gevent.sleep(0.02)
    assert external_simulation_shutter.state_string == simulation_shutter.state_string

    cb_called_event = gevent.event.AsyncResult()

    def state_changed_event(state):
        cb_called_event.set(state)

    event.connect(simulation_shutter, "state", state_changed_event)

    # close shutter from the second process, and ensure we got called via state change event
    try:
        external_simulation_shutter.close()
        with gevent.Timeout(3):
            cb_called_event.wait()
        assert cb_called_event.value == BaseShutterState.CLOSED
    finally:
        event.disconnect(simulation_shutter, "state", state_changed_event)


def test_simulation_shutter_ext_control(default_session, log_context, capsys):
    userlogger.enable()

    # Get config.
    shutter_cfg = default_session.config.get_config("simulation_shutter_ext_control")

    # Keep a copy of the good version of the config.
    shutter_cfg_ok = shutter_cfg.clone()

    # Make a "deprecated" version of the config.
    shutter_cfg["external-control"] = shutter_cfg["external_control"]
    del shutter_cfg["external_control"]
    shutter = SimulationShutter(shutter_cfg["name"], shutter_cfg)

    # Test that "deprecated" version of the config produces a warning message.
    assert "please use 'external_control'" in capsys.readouterr().err

    # Now test the good version.
    shutter = SimulationShutter(shutter_cfg["name"], shutter_cfg_ok)
    shutter.external_control.set("CLOSED")
    shutter.mode = shutter.EXTERNAL
    assert shutter.state == BaseShutterState.CLOSED
    shutter.external_control.set("OPEN")
    assert shutter.state == BaseShutterState.OPEN
    shutter.close()
    assert shutter.external_control.get() == "CLOSED"
