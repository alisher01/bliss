import sys
from bliss.config.conductor import client
from bliss.config.conductor import connection
from bliss.comm.rpc import Server
from bliss.common.shutter import BaseShutterState, Shutter


class SimulationShutter(Shutter):
    def __init__(self, name, config):
        super().__init__(name, config)
        self.__internal_state = BaseShutterState.CLOSED

    def _state(self):
        return self.__internal_state

    def _set_mode(self, value):
        pass

    def _open(self):
        self.__internal_state = BaseShutterState.OPEN

    def _close(self):
        self.__internal_state = BaseShutterState.CLOSED


def get_simulation_shutter():
    return SimulationShutter("simulation_shutter", {"name": "simulation_shutter"})


if __name__ == "__main__":
    beacon_host = sys.argv[1]
    beacon_port = int(sys.argv[2])
    beacon_connection = connection.Connection(beacon_host, beacon_port)
    client._default_connection = beacon_connection

    server = Server(get_simulation_shutter())
    server.bind("tcp://0:0")
    port = server._socket.getsockname()[1]
    print(f"{port}")
    server.run()
