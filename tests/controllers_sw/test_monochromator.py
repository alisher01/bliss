# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2022 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import pytest
import numpy

from bliss.common.standard import mv, info


def approx(val, delta=0.0005):
    return pytest.approx(val, delta)


EXPECTED_TRACK_OFF = """
  FOCUS_TRACKER

              sim_c1    sim_c2
  Tracking       OFF        ON
      Mode     table   polynom
Trajectory         1         1
  On traj.        NO        NO

  SAMPLE_TRACKER

              sim_acc
  Tracking         ON
      Mode      table
Trajectory    default
  On traj.         NO

"""

EXPECTED_TRACK_ON = """
  FOCUS_TRACKER

              sim_c1    sim_c2
  Tracking        ON        ON
      Mode     table   polynom
Trajectory         1         1
  On traj.        NO        NO

  SAMPLE_TRACKER

              sim_acc
  Tracking         ON
      Mode      table
Trajectory    default
  On traj.         NO

"""


@pytest.fixture
def mono(beacon):
    mono = beacon.get("simul_mono")
    mono.xtal_sel = "Si220"
    yield mono
    mono.close()


@pytest.fixture
def mono_fix_exit(beacon):
    mono = beacon.get("simul_mono_fix_exit")
    mono.xtal_sel = "Si220"
    yield mono
    mono.close()


def test_fix_exit(beacon, mono_fix_exit):
    # for the moment, it is enough to check if the class can be instantiated
    _ = beacon.get("simul_mono_braggfe")
    info(mono_fix_exit)


def test_bragg2energy(mono):
    assert mono.bragg2energy(13.333) == approx(14)
    assert mono.energy2bragg(14) == approx(13.333)
    assert mono.energy2bragg(15) == approx(12.429)


def test_mono_xtals(mono):
    """Crystals manager"""
    info(mono)

    xtals = mono.xtals
    assert mono.xtal_is_in(xtals.xtal_names[0])
    assert mono.xtal_is_in(xtals.xtal_names[1])

    mono.xtal_sel = xtals.xtal_names[0]

    assert mono.xtal_sel == xtals.xtal_names[0]


def test_mono_tracking_parameters(beacon, mono):
    mot = beacon.get("sim_c1")
    # enetrack = mono.energy_track_motor

    mot.harmonic = 2
    with pytest.raises(ValueError):
        mot.harmonic = 3

    # assert numpy.isnan(enetrack.position)


def test_mono_tracking_onoff(beacon, mono, capsys):
    mot = beacon.get("sim_c1")
    enetrack = mono.energy_track_motor

    mono.track_off(mot)
    assert mot not in mono.tracked_axes
    assert mot.track is False
    mono.track_info()
    captured = capsys.readouterr()
    assert captured.out == EXPECTED_TRACK_OFF

    mono.track_on(mot)
    assert mot in mono.tracked_axes
    assert mot.track is True
    mono.track_info()
    captured = capsys.readouterr()
    assert captured.out == EXPECTED_TRACK_ON

    mono.track_off()
    assert len(mono.tracked_axes) == 0
    mono.trackers.focus_tracker.track_on()
    assert len(mono.tracked_axes) == 2
    mono.trackers.focus_tracker.track_off(mot)
    assert len(mono.tracked_axes) == 1

    mot.track = True
    assert mot in mono.tracked_axes

    mono.track_mode(mot, "table")
    assert mono.track_mode(mot) == mot.track_mode == "table"

    assert numpy.isnan(enetrack.position)


def test_mono_tracking_move(beacon, mono):
    c1 = beacon.get("sim_c1")
    c2 = beacon.get("sim_c2")
    acc = beacon.get("sim_acc")
    ene = mono.energy_motor
    enetrack = mono.energy_track_motor
    bragg = mono.bragg_motor

    def calculated_position(mot):
        return enetrack.controller.energy2tracker(mot, ene.position)

    mono.track_off(c1)
    mono.track_off(c2)
    mono.track_off(acc)

    mv(ene, 14)
    assert bragg.position == approx(13.333)
    assert ene.position == approx(14)
    assert enetrack.position == approx(14)
    assert calculated_position(c1) == approx(1.107)
    assert c1.position == 0

    mv(bragg, 10)
    assert ene.position == approx(18.593)
    assert enetrack.position == approx(18.593)
    assert calculated_position(c1) == approx(1.425)
    assert c1.position == 0

    c1.track = True

    mv(enetrack, 14)
    assert bragg.position == approx(13.333)
    assert ene.position == approx(14)
    assert enetrack.position == approx(14)
    assert calculated_position(c1) == approx(1.107)
    assert c1.position == approx(1.107)


def test_dataset_metadata(mono):
    mdata = mono.dataset_metadata()
    expected = {
        "crystal": {
            "d_spacing": approx(1.92e-10),
            "reflection": (2, 2, 0),
            "type": "Si",
        },
        "energy": float("inf"),
        "name": "simul_mono",
        "wavelength": 0.0,
    }
    assert mdata == expected

    mv(mono.energy_motor, 14)
    mdata = mono.dataset_metadata()

    expected = {
        "crystal": {
            "d_spacing": approx(1.92e-10),
            "reflection": (2, 2, 0),
            "type": "Si",
        },
        "energy": approx(14),
        "name": "simul_mono",
        "wavelength": approx(8.856e-11),
    }
    assert mdata == expected


def test_scan_metadata(mono):
    mdata = mono.scan_metadata()
    expected = {
        "@NX_class": "NXmonochromator",
        "crystal": {
            "@NX_class": "NXcrystal",
            "d_spacing": approx(1.92e-10),
            "d_spacing@units": "m",
            "reflection": (2, 2, 0),
            "type": "Si",
        },
        "energy": float("inf"),
        "wavelength": 0.0,
        "energy@units": "keV",
        "wavelength@units": "m",
    }
    assert mdata == expected

    mv(mono.energy_motor, 14)
    mdata = mono.scan_metadata()

    expected = {
        "@NX_class": "NXmonochromator",
        "crystal": {
            "@NX_class": "NXcrystal",
            "d_spacing": approx(1.92e-10),
            "d_spacing@units": "m",
            "reflection": (2, 2, 0),
            "type": "Si",
        },
        "energy": approx(14),
        "wavelength": approx(8.856e-11),
        "energy@units": "keV",
        "wavelength@units": "m",
    }
    assert mdata == expected
