# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2022 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import requests
import os
import ast
import glob
import itertools
import shutil
import json


def test_home(beacon, config_app_port, homepage_app_port):
    r = requests.get("http://localhost:%d" % config_app_port)
    assert r.status_code == 200  # OK

    r = requests.get("http://localhost:%d" % homepage_app_port)
    assert r.status_code == 200  # OK


def test_tree_files(beacon, config_app_port):
    r = requests.get("http://localhost:%d/tree/files" % config_app_port)
    assert r.status_code == 200  # OK


def test_db_tree(beacon, config_app_port):
    """Test that the skipped config is part of the dbtree"""
    # Get files list from the webapp.
    url = "http://localhost:%d/db_tree" % config_app_port
    r = requests.get(url)
    assert r.status_code == 200

    data = json.loads(r.text)
    assert "skipped_config" in data
    assert len(data["skipped_config"]) == 3


def test_db_files(beacon, config_app_port):
    """
    Compare config files list taken from:
    * web config application
    * config directory directly from file system
    """
    # get PATH
    cfg_test_files_path = os.path.abspath(
        os.path.join(os.path.dirname(__file__), "..", "test_configuration")
    )

    # Get files list from the webapp.
    url = "http://localhost:%d/db_files" % config_app_port
    r = requests.get(url)
    assert r.status_code == 200

    db_files = set(
        [os.path.join(cfg_test_files_path, f) for f in ast.literal_eval(r.text)]
    )

    # Get files list from file system.
    fs_files = set(
        itertools.chain(
            *[
                glob.glob(os.path.join(dp, "*.yml"))
                for dp, dn, fn in os.walk(cfg_test_files_path)
            ]
        )
    )
    fs_files = [f for f in fs_files if "skipped_config" not in f]

    # Compare the 2 lists.
    assert set(db_files) == set(fs_files)


def test_duplicated_key_on_new_file(beacon, beacon_directory, config_app_port):
    # copy the diode.yml to a new file
    diode_path = os.path.join(beacon_directory, "diode.yml")
    diode1_path = os.path.join(beacon_directory, "diode1.yml")

    shutil.copyfile(diode_path, diode1_path)
    r = requests.get("http://localhost:%d/config/reload" % config_app_port)
    os.remove(diode1_path)

    assert r.status_code == 200  # OK


def test_duplicated_key_on_same_file(beacon, beacon_directory, config_app_port):
    file_content = """\
-
  name: diode_test
  plugin: bliss
  class: simulation_diode
  independent: True
"""
    filename = "diode_duplicated_key.yml"
    filepath = os.path.join(beacon_directory, filename)
    with open(filepath, "w") as f:
        f.write(file_content)

    with open(filepath, "w") as f:
        f.write(file_content)
        f.write(file_content)  # write twice to raise key error

    r = requests.get("http://localhost:%d/config/reload" % config_app_port)
    os.remove(filepath)

    assert r.status_code == 200  # OK
