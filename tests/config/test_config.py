# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2022 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import sys
import os
from unittest.mock import patch
import pytest
import ruamel
import subprocess

from bliss.config.conductor import client
from bliss.config.static import ConfigNode
from bliss.comm.exceptions import CommunicationError
from ..motors.conftest import s1hg, s1vo, m0  # noqa: F401


@pytest.mark.parametrize(
    "file_name, node_name, copy",
    [
        ["read_write.yml", "rw_test", False],
        ["read_write_2.yml", "rw_test_2", False],
        ["read_write.yml", "rw_test", True],
        ["read_write_2.yml", "rw_test_2", True],
    ],
)
def test_config_save(beacon, beacon_directory, file_name, node_name, copy):
    test_file_path = os.path.join(beacon_directory, file_name)
    rw_cfg = beacon.get_config(node_name)
    if copy:
        rw_cfg = rw_cfg.clone()
    test_file_contents = client.get_text_file(file_name)

    with open(test_file_path, "rb") as f:
        assert f.read().decode() == test_file_contents

    assert rw_cfg["one"][0]["pink"] == "martini"
    assert rw_cfg["one"][0]["red"] == "apples"

    for comment in ("comment{}".format(i) for i in range(1, 6)):
        assert comment in test_file_contents

    rw_cfg["one"][0]["red"] = "strawberry"
    rw_cfg["one"][0]["pink"] = "raspberry"

    rw_cfg.save()
    beacon.reload()
    with open(test_file_path) as f:
        content = f.read()
        for comment in ("comment{}".format(i) for i in range(1, 6)):
            assert comment in content

    rw_cfg2 = beacon.get_config(node_name)

    assert id(rw_cfg2) != id(rw_cfg)
    assert rw_cfg2["one"][0]["red"] == "strawberry"
    assert rw_cfg2["one"][0]["pink"] == "raspberry"


@pytest.mark.parametrize("file_name, node_name", [["read_write.yml", "rw_test"]])
def test_config_save_delete_item(beacon, beacon_directory, file_name, node_name):
    test_file_path = os.path.join(beacon_directory, file_name)
    rw_cfg = beacon.get_config(node_name)
    test_file_contents = client.get_text_file(file_name)

    assert list(rw_cfg["three"]) == ["a", "b", "c"]
    rw_cfg["three"].pop(1)

    rw_cfg.save()
    beacon.reload()

    rw_cfg2 = beacon.get_config(node_name)

    assert list(rw_cfg2["three"]) == ["a", "c"]

    rw_cfg2["three"].append("d")
    rw_cfg2["three"].insert(0, "e")
    rw_cfg2.save()
    beacon.reload()
    rw_cfg3 = beacon.get_config(node_name)
    assert list(rw_cfg3["three"]) == ["e", "a", "c", "d"]


def test_yml_load_exception(beacon, beacon_directory):
    file_name = "bad.yml"
    new_file = f"{beacon_directory}/{file_name}"

    with open(new_file, "w") as f:
        f.write(
            """- name: bad_yml
    let:
    - 1
    - 2
"""
        )

    beacon.reload()
    assert file_name in beacon.invalid_yaml_files


def test_empty_yml(beacon, beacon_directory):
    file_name = "toto.yml"
    new_file = f"{beacon_directory}/{file_name}"

    with open(new_file, "w") as f:
        f.write(" ")

    beacon.reload()
    error_msg = beacon.invalid_yaml_files[file_name]
    assert error_msg == "File is empty"


def test_yml_load_error(beacon, beacon_directory):
    new_file = "%s/change_size_error.yml" % beacon_directory

    with open(new_file, "w") as f:
        f.write(
            """- name: change_size_error
  a:
    b:
      - c """
        )

    beacon.reload()
    beacon.get("change_size_error")


def test_yml_load_error2(beacon, beacon_directory):
    new_file = "%s/change_size_error.yml" % beacon_directory

    with open(new_file, "w") as f:
        f.write(
            """- name: change_size_error
  a:
    b:
      c:
        d:
        - e """
        )

    beacon.reload()
    beacon.get("change_size_error")


def test_ruamel_load_error(beacon, beacon_directory):
    file_name = "ruamel_breaker.yml"
    new_file = f"{beacon_directory}/{file_name}"

    with open(new_file, "w") as f:
        f.write(
            """a:

-

- b:  5"""
        )

    beacon.reload()
    error_msg = beacon.invalid_yaml_files[file_name]
    assert error_msg.startswith("YAML parser failure:")


def test_broken_session_yml(beacon, beacon_directory):
    broken_session = f"{beacon_directory}/broken_session.yml"

    with open(broken_session, "w") as f:
        f.write("foo")

    bliss_shell = subprocess.Popen(
        [sys.executable, "-m", "bliss.shell.main", "-s", "broken_session"],
        stdout=subprocess.PIPE,
    )
    output, err = bliss_shell.communicate()

    expected = "'broken_session' does not seem to be a valid session, "
    expected += "it may relate to the following yaml error(s):\n"
    expected += "Ignored 1 YAML file(s) due to parsing error(s):\n"
    expected += "    - broken_session.yml:\n"
    expected += "       |'str' object has no attribute 'items'\n"
    assert expected in output.decode()


@pytest.mark.parametrize(
    "object_name, get_func_name, copy",
    [["refs_test", "get", False], ["refs_test_cpy", "get_config", True]],
)
def test_references(
    beacon, object_name, get_func_name, copy, m0, s1hg, s1vo  # noqa: F811
):
    get_func = getattr(beacon, get_func_name)
    refs_cfg = get_func(object_name)
    if copy:
        refs_cfg = refs_cfg.clone()

    # ISSUE 2045
    with patch.object(ConfigNode, "__getitem__", side_effect=KeyError):
        with pytest.raises(KeyError):
            repr(refs_cfg["m0"])

    try:
        assert repr(refs_cfg["scan"]["axis"]) == repr(m0)
        assert repr(refs_cfg["slits"][0]["axis"]) == repr(s1hg)
        assert refs_cfg["slits"][0]["position"] == 0
        assert repr(refs_cfg["slits"][1]["axis"]) == repr(s1vo)
        assert refs_cfg["slits"][1]["position"] == 1
        assert repr(refs_cfg["m0"]) == repr(m0)
    finally:
        m0.__close__()
        s1hg.__close__()
        s1vo.__close__()


def test_issue_2118(beacon, s1hg, s1vo):  # noqa: F811
    cfg = beacon.get("fscan_eh3")
    cfg = cfg.to_dict()

    try:
        assert cfg["devices"]["musst"][0] is s1hg
        assert cfg["devices"]["musst"][1] is s1vo
    finally:
        s1hg.__close__()
        s1vo.__close__()


def test_issue_451_infinite_recursion(beacon):
    refs_cfg = beacon.get_config("refs_test")

    refs_cfg.get_inherited(
        "toto"
    )  # key which does not exist, was causing infinite recursion

    assert refs_cfg.parent == beacon.root
    assert refs_cfg in beacon.root["__children__"]

    assert beacon.root.parent is None


@pytest.fixture
def use_extralib_in_path():
    extralib_path = os.path.join(os.path.dirname(__file__), "..", "extralib")
    sys.path.append(extralib_path)
    try:
        yield
    finally:
        sys.path.remove(extralib_path)


def test_inherited_package(beacon, use_extralib_in_path):
    assert type(beacon.get("dummy1")).__name__ == "DummyObject"
    assert type(beacon.get("dummy2")).__name__ == "DummyObject"


def test_yaml_boolean(beacon):
    m = beacon.get("fake_multiplexer_config")

    assert m["outputs"][0]["ON"] == 1
    assert m["outputs"][0]["OFF"] == 0


def test_config_save_reference(beacon, beacon_directory):
    node_name = "rw_test_2"
    rw_cfg = beacon.get_config(node_name).clone()
    diode = beacon.get("diode")
    diode2 = beacon.get("diode2")

    rw_cfg["test_list"].append(diode2)
    rw_cfg["dict_list"].append({"cnt_channel": "c", "instance": diode})
    rw_cfg.save()

    beacon.reload()

    rw_cfg2 = beacon.get_config(node_name)
    assert id(rw_cfg2) != id(rw_cfg)
    assert len(rw_cfg2["test_list"]) == len(rw_cfg["test_list"])
    assert [x.name for x in rw_cfg2["test_list"]] == [
        x.name for x in rw_cfg["test_list"]
    ]
    assert len(rw_cfg2["dict_list"]) == len(rw_cfg["dict_list"])
    assert (
        rw_cfg2["dict_list"][2]["instance"].name
        == rw_cfg["dict_list"][2]["instance"].name
    )


def test_bad_icepap_host(beacon):
    bad_mot = beacon.get("v6biturbo")

    with pytest.raises(CommunicationError):
        bad_mot.position


def test_capital_letter_file(beacon):
    # files starting with capital letter were causing problems,
    # see merge request !1524
    # (this was because the files were read before __init__.yml)
    # This test just makes sure the object from 'A.yml' is properly
    # returned as expected
    x = beacon.get("Aunused")
    assert x


def test_bliss_import_error(beacon):
    with pytest.raises(RuntimeError) as excinfo:
        beacon.get("broken_ctrl3")

    # Non existing class
    with pytest.raises(ModuleNotFoundError) as excinfo:
        beacon.get("broken_ctrl")
    assert "CONFIG COULD NOT FIND CLASS" in str(excinfo.value)

    # faulty import in imported module
    with pytest.raises(ModuleNotFoundError) as excinfo:
        beacon.get("broken_ctrl2")
    assert "CONFIG COULD NOT FIND CLASS" not in str(excinfo.value)


def test_config_eval(beacon):
    obj_cfg = beacon.get_config("test_config_eval")

    # ensure the reference is properly evaluated
    roby = beacon.get("roby")
    assert obj_cfg["mapping"]["mot"] == roby.position == 0
    assert obj_cfg["mapping"]["names"] == ["roby", "robz"]

    roby.position = 1

    # ensure reference is re-evaluated each time
    assert obj_cfg["mapping"]["mot"] == roby.position == 1


def test_references_list_inside_subdict(beacon):
    node = beacon.get_config("config_test")
    motor_list = node["mydict"]["mysubdict"]["motors"]
    assert motor_list == [beacon.get("roby"), beacon.get("robz")]


def test_issue_1619(beacon):
    obj_cfg = beacon.get_config("config_test")

    x = obj_cfg.get("test")

    obj = beacon.get("config_test")

    # by definition of the default plugin, the default plugin returns the
    # config node as the object so let's ensure both 'get_config' and 'get'
    # return the same thing
    assert obj_cfg is obj
    assert obj["test"] == x == obj_cfg["test"]


def test_user_tags(beacon):
    objs = [("diode", "diode2", "diode3"), ("robz",)]
    assert beacon.user_tags_list == ["TEST.DIODE", "TEST.ROBZ"]
    for obj_list, tag in zip(objs, beacon.user_tags_list):
        assert set(
            [beacon.get_config(obj) for obj in obj_list]
        ) == beacon.get_user_tag_configs(tag)
        for obj in obj_list:
            beacon.get_config(obj).reload()
        assert set(
            [beacon.get_config(obj) for obj in obj_list]
        ) == beacon.get_user_tag_configs(tag)


def test_to_dict(beacon, s1hg, s1vo, m0):  # noqa F841
    """
    - name: refs_test
      plugin: default
      m0: $m0
      slits:
        - axis: $s1hg
          position: 0
        - axis: $s1vo
          position: 1
      scan:
        axis: $m0
    """
    refs_test = beacon.get("refs_test")

    refs_test_dict = refs_test.to_dict(resolve_references=False)
    assert refs_test_dict["m0"] == "$m0"
    assert refs_test_dict["slits"][0]["axis"] == "$s1hg"
    assert refs_test_dict["slits"][1]["axis"] == "$s1vo"
    assert refs_test_dict["scan"]["axis"] == "$m0"

    refs_test_dict = refs_test.to_dict()
    assert refs_test_dict["m0"] is m0
    assert refs_test_dict["slits"][0]["axis"] is s1hg
    assert refs_test_dict["slits"][1]["axis"] is s1vo
    assert refs_test_dict["scan"]["axis"] is m0
