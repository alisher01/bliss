# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2022 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import itertools
import pytest
import time
import gevent
import os
import datetime
import gevent.queue

from bliss.common.standard import loopscan
from bliss.common.session import set_current_session
from bliss.scanning.scan_saving import ESRFDataPolicyEvent, ScanSaving
from bliss.config import channels
from bliss.shell.standard import (
    newproposal,
    newsample,
    newcollection,
    newdataset,
    enddataset,
    endproposal,
    elog_print,
)
from bliss.common import logtools
from bliss.data.node import get_node

from . import icat_test_utils


def test_icat_backends(
    session, icat_subscriber, icat_logbook_subscriber, esrf_data_policy
):
    scan_saving = session.scan_saving
    icat_test_utils.assert_icat_received_current_proposal(scan_saving, icat_subscriber)
    scan_saving.writer = "hdf5"

    diode = session.config.get("diode")
    newproposal("totoproposal")
    icat_test_utils.assert_logbook_received(
        icat_logbook_subscriber, "Proposal set to", category="info"
    )
    icat_test_utils.assert_icat_received_current_proposal(scan_saving, icat_subscriber)

    newdataset()
    loopscan(1, 0.1, diode)
    icat_test_utils.assert_logbook_received(
        icat_logbook_subscriber, "Dataset set to", category="info"
    )
    expected = icat_test_utils.expected_icat_mq_message(scan_saving, dataset=True)
    enddataset()
    icat_test_utils.assert_icat_received(icat_subscriber, expected)


def test_inhouse_scan_saving(session, icat_subscriber, esrf_data_policy):
    scan_saving = session.scan_saving
    scan_saving_config = scan_saving.scan_saving_config
    icat_test_utils.assert_icat_received_current_proposal(scan_saving, icat_subscriber)

    inhouse_name1 = f"{scan_saving.beamline}{time.strftime('%y%m')}"
    inhouse_name2 = scan_saving.beamline + "9999"
    for bset in [False, True]:
        if bset:
            scan_saving.proposal_name = inhouse_name2
            icat_test_utils.assert_icat_received_current_proposal(
                scan_saving, icat_subscriber
            )
        assert scan_saving.beamline == scan_saving_config["beamline"]
        assert scan_saving.proposal_type == "inhouse"
        if bset:
            assert scan_saving.proposal_name == inhouse_name2
        else:
            assert scan_saving.proposal_name == inhouse_name1
        assert scan_saving.base_path == scan_saving_config["inhouse_data_root"].format(
            beamline=scan_saving.beamline
        )
        assert scan_saving.icat_base_path == scan_saving_config[
            "inhouse_data_root"
        ].format(beamline=scan_saving.beamline)
        assert_default_sample_dataset(scan_saving)


def test_visitor_scan_saving(session, icat_subscriber, esrf_data_policy):
    scan_saving = session.scan_saving
    icat_test_utils.assert_icat_received_current_proposal(scan_saving, icat_subscriber)

    scan_saving.mount_point = "fs1"
    scan_saving_config = scan_saving.scan_saving_config
    scan_saving.proposal_name = "mx415"
    assert scan_saving.proposal_type == "visitor"
    assert scan_saving.base_path == scan_saving_config["visitor_data_root"]["fs1"]
    assert scan_saving.icat_base_path == scan_saving_config["visitor_data_root"]["fs1"]
    assert os.path.join(os.sep, "mx415", "") in scan_saving.proposal.path
    assert_default_sample_dataset(scan_saving)
    icat_test_utils.assert_icat_received_current_proposal(scan_saving, icat_subscriber)


def test_crg_visitor_scan_saving(session, icat_subscriber, esrf_data_policy):
    scan_saving = session.scan_saving
    icat_test_utils.assert_icat_received_current_proposal(scan_saving, icat_subscriber)

    scan_saving.mount_point = "fs1"
    scan_saving_config = scan_saving.scan_saving_config
    scan_saving.proposal_name = "00-1234"
    assert scan_saving.proposal_type == "visitor"
    assert scan_saving.base_path == scan_saving_config["visitor_data_root"]["fs1"]
    assert scan_saving.icat_base_path == scan_saving_config["visitor_data_root"]["fs1"]
    assert (
        os.path.join(os.sep, scan_saving.beamline + "1234", "")
        in scan_saving.proposal.path
    )
    assert_default_sample_dataset(scan_saving)
    icat_test_utils.assert_icat_received_current_proposal(scan_saving, icat_subscriber)

    scan_saving.proposal_name = "00-01234"
    assert scan_saving.proposal_type == "visitor"
    assert scan_saving.base_path == scan_saving_config["visitor_data_root"]["fs1"]
    assert scan_saving.icat_base_path == scan_saving_config["visitor_data_root"]["fs1"]
    assert (
        os.path.join(os.sep, scan_saving.beamline + "1234", "")
        in scan_saving.proposal.path
    )
    assert_default_sample_dataset(scan_saving)
    icat_test_utils.assert_icat_received_current_proposal(scan_saving, icat_subscriber)

    scan_saving.mount_point = "fs1"
    scan_saving_config = scan_saving.scan_saving_config
    scan_saving.proposal_name = "01-1234"  # not a valid CRG proposal at this beamline
    assert scan_saving.proposal_type == "visitor"
    assert scan_saving.base_path == scan_saving_config["visitor_data_root"]["fs1"]
    assert scan_saving.icat_base_path == scan_saving_config["visitor_data_root"]["fs1"]
    assert os.path.join(os.sep, "011234", "") in scan_saving.proposal.path
    assert_default_sample_dataset(scan_saving)
    icat_test_utils.assert_icat_received_current_proposal(scan_saving, icat_subscriber)


def test_invalid_scan_saving(session, icat_subscriber, esrf_data_policy):
    scan_saving = session.scan_saving
    icat_test_utils.assert_icat_received_current_proposal(scan_saving, icat_subscriber)

    scan_saving.mount_point = "fs1"
    scan_saving_config = scan_saving.scan_saving_config
    scan_saving.proposal_name = "hg666"
    assert scan_saving.proposal_type == "visitor"
    assert scan_saving.base_path == scan_saving_config["visitor_data_root"]["fs1"]
    assert scan_saving.icat_base_path == scan_saving_config["visitor_data_root"]["fs1"]
    assert os.path.join(os.sep, "hg666", "") in scan_saving.proposal.path
    assert_default_sample_dataset(scan_saving)
    icat_test_utils.assert_icat_received_nothing(icat_subscriber)


def test_tmp_scan_saving(session, icat_subscriber, esrf_data_policy):
    scan_saving = session.scan_saving
    icat_test_utils.assert_icat_received_current_proposal(scan_saving, icat_subscriber)

    scan_saving.mount_point = "fs1"
    scan_saving_config = scan_saving.scan_saving_config
    scan_saving.proposal_name = "test123"
    assert scan_saving.proposal_type == "tmp"
    expected = scan_saving_config["tmp_data_root"]["fs1"].format(
        beamline=scan_saving.beamline
    )
    assert scan_saving.base_path == expected
    expected = scan_saving_config["icat_tmp_data_root"].format(
        beamline=scan_saving.beamline
    )
    assert scan_saving.icat_base_path == expected
    assert_default_sample_dataset(scan_saving)
    icat_test_utils.assert_icat_received_nothing(icat_subscriber)


def assert_default_sample_dataset(scan_saving):
    assert scan_saving.collection_name == "sample"


def test_auto_dataset_increment(session, icat_subscriber, esrf_data_policy):
    scan_saving = session.scan_saving
    assert scan_saving.dataset_name == "0001"
    with pytest.raises(AttributeError):
        scan_saving.template = "toto"
    assert scan_saving.get_path() == os.path.join(
        scan_saving.base_path,
        scan_saving.proposal_name,
        scan_saving.beamline,
        scan_saving.proposal_session_name,
        scan_saving.collection_name,
        f"{scan_saving.collection_name}_{scan_saving.dataset_name}",
    )
    scan_saving.collection_name = ""
    assert scan_saving.collection_name == "sample"
    scan_saving.dataset_name = ""
    assert scan_saving.dataset_name == "0001"
    with pytest.raises(AttributeError):
        scan_saving.template = ""
    assert scan_saving.get_path().endswith("0001")


def test_auto_dataset_increment_with_creation(
    session, icat_subscriber, esrf_data_policy
):
    scan_saving = session.scan_saving
    icat_test_utils.assert_icat_received_current_proposal(scan_saving, icat_subscriber)
    expected_dataset = icat_test_utils.expected_icat_mq_message(
        scan_saving, dataset=True
    )
    assert scan_saving.dataset_name == "0001"

    icat_test_utils.create_dataset(scan_saving)
    scan_saving.dataset_name = ""
    icat_test_utils.assert_icat_received(icat_subscriber, expected_dataset)
    expected_dataset = icat_test_utils.expected_icat_mq_message(
        scan_saving, dataset=True
    )
    assert scan_saving.dataset_name == "0002"

    icat_test_utils.create_dataset(scan_saving)
    path = scan_saving.get_path()
    new_filename = os.path.join(path, scan_saving.data_filename + ".h5")
    with open(new_filename, "w"):
        scan_saving.dataset_name = ""
        icat_test_utils.assert_icat_received(icat_subscriber, expected_dataset)
        expected_dataset = icat_test_utils.expected_icat_mq_message(
            scan_saving, dataset=True
        )
        assert scan_saving.dataset_name == "0003"
        # icat_test_utils.create_dataset(scan_saving)

    scan_saving.dataset_name = "dataset"
    # No directory -> not in ICAT
    # icat_test_utils.assert_icat_received(icat_subscriber, expected_dataset)
    expected_dataset = icat_test_utils.expected_icat_mq_message(
        scan_saving, dataset=True
    )

    icat_test_utils.create_dataset(scan_saving)
    scan_saving.dataset_name = "dataset"
    icat_test_utils.assert_icat_received(icat_subscriber, expected_dataset)
    assert scan_saving.dataset_name == "dataset_0002"
    assert scan_saving.get_path().endswith("dataset_0002")


@pytest.mark.parametrize(
    "user_action,same_sample",
    [[newdataset, True], [newsample, True], [newproposal, False], [newproposal, True]],
)
def test_close_dataset(
    session, icat_subscriber, esrf_data_policy, user_action, same_sample
):
    scan_saving = session.scan_saving
    scan_saving.writer = "hdf5"
    icat_test_utils.assert_icat_received_current_proposal(scan_saving, icat_subscriber)
    newproposal("myproposal")
    icat_test_utils.assert_icat_received_current_proposal(scan_saving, icat_subscriber)
    if same_sample:
        newsample("mysample")

    diode = session.config.get("diode")
    loopscan(1, 0.1, diode)
    expected_dataset = icat_test_utils.expected_icat_mq_message(
        scan_saving, dataset=True
    )
    user_action(None)
    icat_test_utils.assert_icat_received(icat_subscriber, expected_dataset)


def test_data_policy_objects(session, icat_subscriber, esrf_data_policy):
    scan_saving = session.scan_saving
    icat_test_utils.assert_icat_received_current_proposal(scan_saving, icat_subscriber)

    # Prepare for scanning without the Nexus writer
    diode = session.env_dict["diode"]
    scan_saving.writer = "hdf5"

    proposal = scan_saving.proposal
    collection = scan_saving.collection
    dataset = scan_saving.dataset

    assert proposal.node.type == "proposal"
    assert str(proposal) == scan_saving.proposal_name
    assert proposal.name == scan_saving.proposal_name
    assert proposal.has_samples
    assert len(list(proposal.sample_nodes)) == 1

    assert collection.node.type == "dataset_collection"
    assert str(collection) == scan_saving.collection_name
    assert collection.proposal.name == scan_saving.proposal_name
    assert collection.name == scan_saving.collection_name
    assert collection.has_datasets
    assert len(list(collection.dataset_nodes)) == 1
    assert collection.proposal.node.db_name == proposal.node.db_name

    assert dataset.node.type == "dataset"
    assert str(dataset) == scan_saving.dataset_name
    assert dataset.proposal.name == scan_saving.proposal_name
    assert dataset.collection.name == scan_saving.collection_name
    assert dataset.name == scan_saving.dataset_name
    assert not dataset.has_scans
    assert len(list(dataset.scan_nodes)) == 0
    assert dataset.proposal.node.db_name == proposal.node.db_name
    assert dataset.collection.node.db_name == collection.node.db_name

    loopscan(3, 0.01, diode)

    assert dataset.has_scans
    assert len(list(dataset.scan_nodes)) == 1

    assert dataset.path.startswith(proposal.path)
    assert dataset.path.startswith(collection.path)
    assert collection.path.startswith(proposal.path)
    assert len(dataset.path) > len(proposal.path)
    assert len(dataset.path) > len(collection.path)
    assert len(collection.path) > len(proposal.path)


def test_dataset_object(session, icat_subscriber, esrf_data_policy):
    scan_saving = session.scan_saving
    icat_test_utils.assert_icat_received_current_proposal(scan_saving, icat_subscriber)

    # Prepare for scanning without the Nexus writer
    diode = session.env_dict["diode"]
    scan_saving.writer = "hdf5"

    # First dataset
    s = loopscan(3, 0.01, diode)

    dataset = scan_saving.dataset
    assert dataset.has_scans
    assert dataset.has_data
    assert [s.name for s in dataset.scan_nodes] == [s.node.name]
    assert not dataset.is_closed

    # Second dataset
    expected_dataset = icat_test_utils.expected_icat_mq_message(
        scan_saving, dataset=True
    )
    scan_saving.dataset_name = None
    assert dataset.is_closed
    assert scan_saving._dataset_object is None
    icat_test_utils.assert_icat_received(icat_subscriber, expected_dataset)

    s = loopscan(3, 0.01, diode)
    assert scan_saving.dataset.node.db_name != dataset.node.db_name

    # Third dataset
    expected_dataset = icat_test_utils.expected_icat_mq_message(
        scan_saving, dataset=True
    )
    scan_saving.dataset_name = None
    icat_test_utils.assert_icat_received(icat_subscriber, expected_dataset)

    # Third dataset not in Redis yet
    n = get_node(session.name)
    walk_res = [d for d in n.walk(wait=False, include_filter="dataset")]
    assert len(walk_res) == 2

    s = loopscan(3, 0.01, diode, save=False)

    # Third dataset in Redis
    n = get_node(session.name)
    walk_res = [d for d in n.walk(wait=False, include_filter="dataset")]
    assert len(walk_res) == 3

    # Third dataset object does not exist yet
    assert scan_saving._dataset_object is None

    # Third dataset created upon using it
    dataset = scan_saving.dataset
    assert not dataset.is_closed
    assert dataset.has_scans
    assert not dataset.has_data
    assert [s.name for s in dataset.scan_nodes] == [s.node.name]

    # Does not go to a new dataset (because the current one has no data)
    scan_saving.dataset_name = None

    # Still in third dataset
    assert scan_saving.dataset.node.db_name == dataset.node.db_name
    assert not dataset.is_closed
    assert dataset.has_scans
    assert not dataset.has_data
    assert [s.name for s in dataset.scan_nodes] == [s.node.name]

    # Test walk on datasets
    n = get_node(session.name)
    walk_res = [d for d in n.walk(wait=False, include_filter="dataset")]
    assert len(walk_res) == 3


def test_data_policy_user_functions(
    session, icat_subscriber, icat_logbook_subscriber, esrf_data_policy
):
    scan_saving = session.scan_saving
    icat_test_utils.assert_icat_received_current_proposal(scan_saving, icat_subscriber)
    expected_dataset = icat_test_utils.expected_icat_mq_message(
        scan_saving, dataset=True
    )
    default_proposal = f"{scan_saving.beamline}{time.strftime('%y%m')}"

    assert scan_saving.proposal_name == default_proposal
    assert scan_saving.collection_name == "sample"
    assert scan_saving.dataset_name == "0001"
    icat_test_utils.create_dataset(scan_saving)

    newproposal("toto")
    icat_test_utils.assert_logbook_received(
        icat_logbook_subscriber, "toto", category="info"
    )
    icat_test_utils.assert_icat_received(icat_subscriber, expected_dataset)
    icat_test_utils.assert_icat_received_current_proposal(scan_saving, icat_subscriber)
    expected_dataset = icat_test_utils.expected_icat_mq_message(
        scan_saving, dataset=True
    )
    assert scan_saving.proposal_name == "toto"
    assert scan_saving.collection_name == "sample"
    assert scan_saving.dataset_name == "0001"
    icat_test_utils.create_dataset(scan_saving)

    newcollection("tata")
    icat_test_utils.assert_logbook_received(
        icat_logbook_subscriber, "tata", category="info"
    )
    icat_test_utils.assert_icat_received(icat_subscriber, expected_dataset)
    expected_dataset = icat_test_utils.expected_icat_mq_message(
        scan_saving, dataset=True
    )
    assert scan_saving.proposal_name == "toto"
    assert scan_saving.collection_name == "tata"
    assert scan_saving.dataset_name == "0001"
    icat_test_utils.create_dataset(scan_saving)

    newdataset("tutu")
    icat_test_utils.assert_logbook_received(
        icat_logbook_subscriber, "tutu", category="info"
    )
    icat_test_utils.assert_icat_received(icat_subscriber, expected_dataset)
    expected_dataset = icat_test_utils.expected_icat_mq_message(
        scan_saving, dataset=True
    )
    assert scan_saving.proposal_name == "toto"
    assert scan_saving.collection_name == "tata"
    assert scan_saving.dataset_name == "tutu"
    icat_test_utils.create_dataset(scan_saving)

    newproposal()
    icat_test_utils.assert_logbook_received(
        icat_logbook_subscriber, default_proposal, category="info"
    )
    icat_test_utils.assert_icat_received(icat_subscriber, expected_dataset)
    icat_test_utils.assert_icat_received_current_proposal(scan_saving, icat_subscriber)
    expected_dataset = icat_test_utils.expected_icat_mq_message(
        scan_saving, dataset=True
    )
    assert scan_saving.proposal_name == default_proposal
    assert scan_saving.collection_name == "sample"
    assert scan_saving.dataset_name == "0002"
    icat_test_utils.create_dataset(scan_saving)

    enddataset()
    icat_test_utils.assert_icat_received(icat_subscriber, expected_dataset)
    expected_dataset = icat_test_utils.expected_icat_mq_message(
        scan_saving, dataset=True
    )
    assert scan_saving.proposal_name == default_proposal
    assert scan_saving.collection_name == "sample"
    assert scan_saving.dataset_name == "0003"
    icat_test_utils.create_dataset(scan_saving)

    endproposal()
    icat_test_utils.assert_icat_received(icat_subscriber, expected_dataset)
    expected_dataset = icat_test_utils.expected_icat_mq_message(
        scan_saving, dataset=True
    )
    assert scan_saving.proposal_name == default_proposal
    assert scan_saving.collection_name == "sample"
    assert scan_saving.dataset_name == "0004"
    icat_test_utils.create_dataset(scan_saving)

    expected_proposal = icat_test_utils.expected_icat_mq_message(scan_saving)
    newproposal("toto")
    icat_test_utils.assert_icat_received(icat_subscriber, expected_proposal)
    icat_test_utils.assert_logbook_received(
        icat_logbook_subscriber, "toto", category="info"
    )
    icat_test_utils.assert_icat_received(icat_subscriber, expected_dataset)
    icat_test_utils.assert_icat_received_current_proposal(scan_saving, icat_subscriber)
    expected_dataset = icat_test_utils.expected_icat_mq_message(
        scan_saving, dataset=True
    )
    assert scan_saving.proposal_name == "toto"
    assert scan_saving.collection_name == "sample"
    assert scan_saving.dataset_name == "0002"
    icat_test_utils.create_dataset(scan_saving)

    endproposal()
    icat_test_utils.assert_icat_received(icat_subscriber, expected_dataset)
    icat_test_utils.assert_icat_received_current_proposal(scan_saving, icat_subscriber)
    assert scan_saving.proposal_name == default_proposal
    assert scan_saving.collection_name == "sample"
    assert scan_saving.dataset_name == "0005"


def test_data_policy_repeat_user_functions(
    session, icat_subscriber, icat_logbook_subscriber, esrf_data_policy
):
    scan_saving = session.scan_saving
    icat_test_utils.assert_icat_received_current_proposal(scan_saving, icat_subscriber)
    default_proposal = f"{scan_saving.beamline}{time.strftime('%y%m')}"

    assert scan_saving.proposal_name == default_proposal
    assert scan_saving.collection_name == "sample"
    assert scan_saving.dataset_name == "0001"
    assert scan_saving.collection.sample_description is None
    assert scan_saving.dataset.description is None

    scan_saving.newproposal(None)
    icat_test_utils.assert_logbook_received(
        icat_logbook_subscriber, default_proposal, category="info"
    )
    scan_saving.newcollection(None)
    icat_test_utils.assert_logbook_received(
        icat_logbook_subscriber, "sample", category="info"
    )
    scan_saving.newdataset(None)
    icat_test_utils.assert_logbook_received(
        icat_logbook_subscriber, "0001", category="info"
    )

    assert scan_saving.proposal_name == default_proposal
    assert scan_saving.collection_name == "sample"
    assert scan_saving.dataset_name == "0001"
    assert scan_saving.collection.sample_description is None
    assert scan_saving.dataset.description is None

    scan_saving.newsample(None, description="sample description")
    icat_test_utils.assert_logbook_received(
        icat_logbook_subscriber, "sample", category="info"
    )
    assert scan_saving.proposal_name == default_proposal
    assert scan_saving.collection_name == "sample"
    assert scan_saving.dataset_name == "0001"
    assert scan_saving.collection.sample_description == "sample description"
    assert scan_saving.dataset.description == "sample description"

    scan_saving.newsample(None, description="modified sample description")
    icat_test_utils.assert_logbook_received(
        icat_logbook_subscriber, "sample", category="info"
    )
    assert scan_saving.proposal_name == default_proposal
    assert scan_saving.collection_name == "sample"
    assert scan_saving.dataset_name == "0001"
    assert scan_saving.collection.sample_description == "modified sample description"
    assert scan_saving.dataset.description == "modified sample description"

    scan_saving.newdataset(None, description="toto")
    icat_test_utils.assert_logbook_received(
        icat_logbook_subscriber, "0001", category="info"
    )
    assert scan_saving.proposal_name == default_proposal
    assert scan_saving.collection_name == "sample"
    assert scan_saving.dataset_name == "0001"
    assert scan_saving.collection.sample_description == "modified sample description"
    assert scan_saving.dataset.description == "modified sample description (toto)"


def test_fresh_sample(session, esrf_data_policy):
    scan_saving = ScanSaving("my_custom_scansaving")
    scan_saving.newsample("toto")
    assert scan_saving.collection_name == "toto"
    assert scan_saving.dataset_name == "0001"


def test_fresh_newcollection(session, esrf_data_policy):
    scan_saving = ScanSaving("my_custom_scansaving")
    scan_saving.newcollection("toto")
    assert scan_saving.collection_name == "toto"
    assert scan_saving.dataset_name == "0001"


def test_fresh_newdataset(session, esrf_data_policy):
    scan_saving = ScanSaving("my_custom_scansaving")
    scan_saving.newdataset("toto")
    assert scan_saving.collection_name == "sample"
    assert scan_saving.dataset_name == "toto"


def test_data_policy_name_validation(session, esrf_data_policy):
    # `set_expiration_time` is called in this test to test that the names
    # don't cause Redis key search errors (issue #3358)

    scan_saving = session.scan_saving

    # proposal name validation (alphanumeric, space-like, dash and underscore)
    invalid_names = "with,", "with:", "with;", f"with{os.sep}", "withé"
    for name in invalid_names:
        with pytest.raises(ValueError):
            scan_saving.proposal_name = name

    # proposal name normalization
    valid_names = "HG64", " HG- 64", "HG__64", "hg_64", "  H -- G   -- 6_4  "
    for name in valid_names:
        scan_saving.proposal_name = name
        assert scan_saving.proposal_name == "hg64"
        scan_saving.proposal.set_expiration_time()

    # beamline name validation (alphanumeric, space-like, dash and underscore)
    invalid_names = "with,", "with:", "with;", f"with{os.sep}", "withé"
    for name in invalid_names:
        with pytest.raises(ValueError):
            scan_saving.proposal_name = name

    # beamline name normalization
    valid_names = "ID00", " ID- 00", "ID__00", "ID_00", "  I -- D   -- 0_0  "
    for name in valid_names:
        scan_saving.proposal_name = name
        assert scan_saving.proposal_name == "id00"
        scan_saving.proposal.set_expiration_time()

    # collection name validation (no os.sep)
    invalid_names = (f"with{os.sep}", "with}", "with{")
    for name in invalid_names:
        with pytest.raises(ValueError):
            scan_saving.collection_name = name

    # collection name normalization (not normalized!)
    valid_names = (
        "with,",
        "with:",
        "with;",
        "withé",
        " sample Name",
        "sample  Name",
        "  sample -- Name ",
    )
    for name in valid_names:
        scan_saving.collection_name = name
        assert scan_saving.collection_name == name
        scan_saving.collection.set_expiration_time()

    # dataset name validation (no os.sep)
    invalid_names = (f"with{os.sep}", "with}", "with{")
    for name in invalid_names:
        with pytest.raises(ValueError):
            scan_saving.collection_name = name

    # dataset name normalization (not normalized!)
    valid_names = (
        "with,",
        "with:",
        "with;",
        "withé",
        "with)",
        " dataset Name",
        "dataset  Name",
        "  dataset -- Name ",
    )
    for name in valid_names:
        scan_saving.dataset_name = name
        assert scan_saving.dataset_name == name
        scan_saving.dataset.set_expiration_time()


def test_session_scan_saving_clone(session, esrf_data_policy):
    scan_saving = session.scan_saving

    # just to create a tango dev proxy in scan saving
    scan_saving.icat_client

    # create a clone
    scan_saving2 = scan_saving.clone()

    # check that the clone is a clone
    # and that the SLOTS are the same (shallow copy)
    assert id(scan_saving) != id(scan_saving2)
    assert scan_saving2._icat_client is not None
    assert id(scan_saving._icat_client) == id(scan_saving2._icat_client)

    # check that the same redis structure is used by the clone
    scan_saving.proposal_name = "toto"
    assert scan_saving2.proposal_name == "toto"


def test_mount_points(session, esrf_data_policy):
    scan_saving = session.scan_saving
    scan_saving_config = scan_saving.scan_saving_config

    # Test setting mount points
    assert scan_saving.mount_points == {"", "fs1", "fs2", "fs3"}
    for mp in ["", "fs1", "fs2", "fs3"]:
        scan_saving.mount_point = mp
        assert scan_saving.mount_point == mp
    with pytest.raises(ValueError):
        scan_saving.mount_point = "non-existing"
    scan_saving.mount_point == mp

    # Test temp mount points (has sf1 and sf2 and fixed icat)
    scan_saving.proposal_name = "temp123"

    icat_expected = scan_saving_config["icat_tmp_data_root"].format(
        beamline=scan_saving.beamline
    )

    scan_saving.mount_point = ""
    expected = scan_saving_config["tmp_data_root"]["fs1"].format(
        beamline=scan_saving.beamline
    )
    assert scan_saving.base_path == expected
    assert scan_saving.icat_base_path == icat_expected

    scan_saving.mount_point = "fs1"
    expected = scan_saving_config["tmp_data_root"]["fs1"].format(
        beamline=scan_saving.beamline
    )
    assert scan_saving.base_path == expected
    assert scan_saving.icat_base_path == icat_expected

    scan_saving.mount_point = "fs2"
    expected = scan_saving_config["tmp_data_root"]["fs2"].format(
        beamline=scan_saving.beamline
    )
    assert scan_saving.base_path == expected
    assert scan_saving.icat_base_path == icat_expected

    scan_saving.mount_point = "fs3"
    expected = scan_saving_config["tmp_data_root"]["fs1"].format(
        beamline=scan_saving.beamline
    )
    assert scan_saving.base_path == expected
    assert scan_saving.icat_base_path == icat_expected

    # Test visitor mount points (has sf1 and sf3 and no fixed icat)
    scan_saving.proposal_name = "hg123"

    scan_saving.mount_point = ""
    expected = scan_saving_config["visitor_data_root"]["fs1"]
    assert scan_saving.base_path == expected
    assert scan_saving.icat_base_path == expected

    scan_saving.mount_point = "fs1"
    expected = scan_saving_config["visitor_data_root"]["fs1"]
    assert scan_saving.base_path == expected
    assert scan_saving.icat_base_path == expected

    scan_saving.mount_point = "fs2"
    expected = scan_saving_config["visitor_data_root"]["fs1"]
    assert scan_saving.base_path == expected
    assert scan_saving.icat_base_path == expected

    scan_saving.mount_point = "fs3"
    expected = scan_saving_config["visitor_data_root"]["fs3"]
    assert scan_saving.base_path == expected
    assert scan_saving.icat_base_path == expected

    # Test inhouse mount points (no named mount points)
    scan_saving.proposal_name = scan_saving.beamline + "9999"

    expected = scan_saving_config["inhouse_data_root"].format(
        beamline=scan_saving.beamline
    )

    scan_saving.mount_point = ""
    assert scan_saving.base_path == expected
    assert scan_saving.icat_base_path == expected

    scan_saving.mount_point = "fs1"
    assert scan_saving.base_path == expected
    assert scan_saving.icat_base_path == expected

    scan_saving.mount_point = "fs2"
    assert scan_saving.base_path == expected
    assert scan_saving.icat_base_path == expected

    scan_saving.mount_point = "fs3"
    assert scan_saving.base_path == expected
    assert scan_saving.icat_base_path == expected


def test_session_ending(
    session, icat_subscriber, icat_logbook_subscriber, esrf_data_policy
):
    scan_saving = session.scan_saving
    default_proposal = f"{scan_saving.beamline}{time.strftime('%y%m')}"
    icat_test_utils.assert_icat_received_current_proposal(scan_saving, icat_subscriber)

    scan_saving.newproposal("hg123")
    icat_test_utils.assert_logbook_received(
        icat_logbook_subscriber, "hg123", category="info"
    )
    icat_test_utils.assert_icat_received_current_proposal(scan_saving, icat_subscriber)
    scan_saving.newcollection("sample1")
    icat_test_utils.assert_logbook_received(
        icat_logbook_subscriber, "sample1", category="info"
    )
    icat_test_utils.create_dataset(scan_saving)

    assert scan_saving.proposal_name == "hg123"
    assert scan_saving.collection_name == "sample1"
    assert scan_saving.dataset_name == "0001"
    expected_dataset = icat_test_utils.expected_icat_mq_message(
        scan_saving, dataset=True
    )

    scan_saving.enddataset()
    assert scan_saving.proposal_name == "hg123"
    assert scan_saving.collection_name == "sample1"
    assert scan_saving.dataset_name == "0002"
    icat_test_utils.create_dataset(scan_saving)
    icat_test_utils.assert_icat_received(icat_subscriber, expected_dataset)
    expected_dataset = icat_test_utils.expected_icat_mq_message(
        scan_saving, dataset=True
    )

    scan_saving.endproposal()
    icat_test_utils.assert_icat_received(icat_subscriber, expected_dataset)
    icat_test_utils.assert_icat_received_current_proposal(scan_saving, icat_subscriber)
    assert scan_saving.proposal_name == default_proposal
    assert scan_saving.collection_name == "sample"
    assert scan_saving.dataset_name == "0001"


def test_data_policy_event(session, esrf_data_policy):
    event_channel = channels.EventChannel(f"{session.name}:esrf_data_policy")
    event_queue = gevent.queue.Queue()

    def event_callback(events):
        for event in events:
            event_queue.put(event)

    event_channel.register_callback(event_callback)

    scan_saving = session.scan_saving
    scan_saving.newproposal("hg123")
    scan_saving.newcollection("sample1")
    scan_saving.newdataset("42")
    icat_test_utils.create_dataset(scan_saving)
    scan_saving.enddataset()
    icat_test_utils.create_dataset(scan_saving)
    scan_saving.endproposal()

    session.disable_esrf_data_policy()

    event = event_queue.get(timeout=3)
    assert event["event_type"] == ESRFDataPolicyEvent.Enable

    event = event_queue.get(timeout=3)
    assert event["event_type"] == ESRFDataPolicyEvent.Change
    msg = f"Proposal set to 'hg123' (session '{scan_saving.proposal_session_name}')"
    assert event["value"]["message"] == msg

    event = event_queue.get(timeout=3)
    assert event["event_type"] == ESRFDataPolicyEvent.Change
    assert event["value"]["message"] == "Dataset collection set to 'sample1'"

    event = event_queue.get(timeout=3)
    assert event["event_type"] == ESRFDataPolicyEvent.Change
    assert event["value"]["message"] == "Dataset set to '0042'"

    event = event_queue.get(timeout=3)
    assert event["event_type"] == ESRFDataPolicyEvent.Change
    assert event["value"]["message"] == "Dataset set to '0001'"

    event = event_queue.get(timeout=3)
    assert event["event_type"] == ESRFDataPolicyEvent.Change
    msg = f"Proposal set to '{scan_saving.proposal_name}' (session '{scan_saving.proposal_session_name}')"
    assert event["value"]["message"] == msg

    event = event_queue.get(timeout=3)
    assert event["event_type"] == ESRFDataPolicyEvent.Disable


def test_date_in_basepath(session, icat_logbook_subscriber, esrf_data_policy):
    # Put date in base path template:
    scan_saving = session.scan_saving
    new_base_path = os.path.join(scan_saving.base_path, "{date}")
    scan_saving.scan_saving_config["inhouse_data_root"] = new_base_path

    # Call newproposal in the past:
    pasttime = time.time() - 3600 * 24 * 100

    def mytime():
        return pasttime

    time.time, orgtime = mytime, time.time
    inhouse_name1 = scan_saving.beamline + "9999"
    try:
        scan_saving.newproposal(inhouse_name1)
        icat_test_utils.assert_logbook_received(
            icat_logbook_subscriber, inhouse_name1, category="info"
        )
        past = scan_saving.date
        assert scan_saving.base_path.endswith(past)
    finally:
        time.time = orgtime

    # Call newproposal in the present:
    scan_saving.newproposal(inhouse_name1)
    icat_test_utils.assert_logbook_received(
        icat_logbook_subscriber, inhouse_name1, category="info"
    )
    assert scan_saving.date == past
    assert scan_saving.base_path.endswith(past)

    inhouse_name2 = scan_saving.beamline + "8888"
    scan_saving.newproposal(inhouse_name2)
    icat_test_utils.assert_logbook_received(
        icat_logbook_subscriber, inhouse_name2, category="info"
    )
    assert scan_saving.date != past
    assert not scan_saving.base_path.endswith(past)

    scan_saving.newproposal(inhouse_name1)
    icat_test_utils.assert_logbook_received(
        icat_logbook_subscriber, inhouse_name1, category="info"
    )
    assert scan_saving.date != past
    assert not scan_saving.base_path.endswith(past)


def test_parallel_sessions(
    session,
    session2,
    icat_subscriber,
    icat_logbook_subscriber,
    esrf_data_policy,
    esrf_data_policy2,
):
    # SCAN_SAVING uses the `current_session`

    def get_scan_saving1():
        set_current_session(session, force=True)
        return session.scan_saving

    def get_scan_saving2():
        set_current_session(session2, force=True)
        return session2.scan_saving

    scan_saving = get_scan_saving1()
    assert scan_saving.session == "test_session"
    assert scan_saving.dataset_name == "0001"
    icat_test_utils.assert_icat_received_current_proposal(scan_saving, icat_subscriber)

    scan_saving = get_scan_saving2()
    assert scan_saving.session == "test_session2"
    assert scan_saving.dataset_name == "0002"
    icat_test_utils.assert_icat_received_current_proposal(scan_saving, icat_subscriber)

    scan_saving = get_scan_saving1()
    inhouse_name = scan_saving.beamline + "9999"
    scan_saving.newproposal(inhouse_name)
    icat_test_utils.assert_logbook_received(
        icat_logbook_subscriber, inhouse_name, category="info"
    )
    icat_test_utils.assert_icat_received_current_proposal(scan_saving, icat_subscriber)
    assert scan_saving.dataset_name == "0001"

    scan_saving = get_scan_saving2()
    scan_saving.newproposal(inhouse_name)
    icat_test_utils.assert_logbook_received(
        icat_logbook_subscriber, inhouse_name, category="info"
    )
    icat_test_utils.assert_icat_received_current_proposal(scan_saving, icat_subscriber)
    assert scan_saving.dataset_name == "0002"

    get_scan_saving1().newdataset(None)
    icat_test_utils.assert_logbook_received(
        icat_logbook_subscriber, "0001", category="info"
    )
    get_scan_saving2().newdataset(None)
    icat_test_utils.assert_logbook_received(
        icat_logbook_subscriber, "0002", category="info"
    )
    assert get_scan_saving1().dataset_name == "0001"
    assert get_scan_saving2().dataset_name == "0002"

    get_scan_saving2().newdataset(None)
    icat_test_utils.assert_logbook_received(
        icat_logbook_subscriber, "0002", category="info"
    )
    get_scan_saving1().newdataset(None)
    icat_test_utils.assert_logbook_received(
        icat_logbook_subscriber, "0001", category="info"
    )
    assert get_scan_saving1().dataset_name == "0001"
    assert get_scan_saving2().dataset_name == "0002"

    scan_saving = get_scan_saving1()
    expected_dataset = icat_test_utils.expected_icat_mq_message(
        scan_saving, dataset=True
    )
    icat_test_utils.create_dataset(scan_saving)
    scan_saving.newdataset(None)
    icat_test_utils.assert_logbook_received(
        icat_logbook_subscriber, "0003", category="info"
    )
    icat_test_utils.assert_icat_received(icat_subscriber, expected_dataset)
    assert get_scan_saving1().dataset_name == "0003"
    assert get_scan_saving2().dataset_name == "0002"

    get_scan_saving1().newdataset("0002")
    icat_test_utils.assert_logbook_received(
        icat_logbook_subscriber, "0003", category="info"
    )
    assert get_scan_saving1().dataset_name == "0003"
    assert get_scan_saving2().dataset_name == "0002"

    get_scan_saving1().newdataset("named")
    icat_test_utils.assert_logbook_received(
        icat_logbook_subscriber, "named", category="info"
    )
    assert get_scan_saving1().dataset_name == "named"
    assert get_scan_saving2().dataset_name == "0002"

    get_scan_saving2().newdataset("named")
    icat_test_utils.assert_logbook_received(
        icat_logbook_subscriber, "named_0002", category="info"
    )
    assert get_scan_saving1().dataset_name == "named"
    assert get_scan_saving2().dataset_name == "named_0002"


def test_elog_print(session, icat_logbook_subscriber, esrf_data_policy):
    elog_print("message1")
    icat_test_utils.assert_logbook_received(
        icat_logbook_subscriber, "message1", complete=True, category="comment"
    )


def test_electronic_logbook(session, icat_logbook_subscriber, esrf_data_policy):
    lst = [
        ("info", "info"),
        ("warning", "error"),
        ("error", "error"),
        ("debug", "debug"),
        ("critical", "error"),
        ("command", "commandLine"),
        ("comment", "comment"),
        ("print", "comment"),
    ]
    beamline_only = [True, False, None]
    scan_saving = session.scan_saving
    try:
        for param in itertools.product(lst, beamline_only, beamline_only):
            (method_name, category), beamline_only, default_beamline_only = param
            logtools.elogbook.beamline_only = default_beamline_only
            msg = repr(method_name + " message")
            method = getattr(logtools.elogbook, method_name)
            if beamline_only is None:
                method(msg)
                effective_beamline_only = logtools.elogbook.beamline_only
            else:
                method(msg, beamline_only=beamline_only)
                effective_beamline_only = beamline_only
            try:
                icat_test_utils.assert_logbook_received(
                    icat_logbook_subscriber,
                    msg,
                    complete=True,
                    category=category,
                    scan_saving=scan_saving,
                    beamline_only=effective_beamline_only,
                )
            except AssertionError as e:
                raise AssertionError(str(param)) from e
    finally:
        logtools.elogbook.beamline_only = None


def test_parallel_scans(session, esrf_data_policy):
    glts = [
        gevent.spawn(session.scan_saving.clone().on_scan_run, True) for _ in range(100)
    ]
    gevent.joinall(glts, raise_error=True, timeout=10)


@pytest.mark.parametrize("missing_dataset", (True, False))
@pytest.mark.parametrize("missing_collection", (True, False))
@pytest.mark.parametrize("missing_proposal", (True, False))
@pytest.mark.parametrize("policymethod", (newdataset, newsample, newproposal))
def test_missing_icat_nodes_not_blocking(
    missing_dataset,
    missing_collection,
    missing_proposal,
    policymethod,
    session,
    esrf_data_policy,
    nexus_writer_service,
):
    diode = session.config.get("diode")
    scan_saving = session.scan_saving

    s = loopscan(1, 0.001, diode)

    old_dataset = scan_saving.dataset
    assert not old_dataset.is_closed

    if missing_dataset:
        dataset = scan_saving.dataset.node
        s.root_connection.delete(dataset.db_name)
    if missing_collection:
        collection = scan_saving.collection.node
        s.root_connection.delete(collection.db_name)
    if missing_proposal:
        proposal = scan_saving.proposal.node
        s.root_connection.delete(proposal.db_name)

    # Check that the dataset is closed despite the exception
    # due to missing nodes (i.e. incomplete ICAT metadata)
    if missing_dataset or missing_collection or missing_proposal:
        with pytest.raises(
            RuntimeError,
            match="The dataset was closed but its ICAT metadata is incomplete",
        ):
            policymethod("new")
        assert old_dataset.is_closed

    # Check that the session is not blocked
    policymethod("new")


def test_data_policy_db_names(session, esrf_data_policy):
    diode = session.config.get("diode")
    proposal = session.scan_saving.proposal
    collection = session.scan_saving.collection
    dataset = session.scan_saving.dataset
    scan = loopscan(3, 0.01, diode, save=False)
    scan_prefix = scan.node.db_name

    policy_db_names1, data_db_names1 = proposal._get_db_names(include_parents=False)
    policy_db_names2, data_db_names2 = collection._get_db_names(include_parents=False)
    policy_db_names3, data_db_names3 = dataset._get_db_names(include_parents=False)

    assert all(db_name.startswith(scan_prefix) for db_name in data_db_names1)
    assert data_db_names1 == data_db_names2
    assert data_db_names1 == data_db_names3
    assert policy_db_names2 < policy_db_names1
    assert policy_db_names3 < policy_db_names2

    policy_db_names1, data_db_names1 = proposal._get_db_names(include_parents=True)
    policy_db_names2, data_db_names2 = collection._get_db_names(include_parents=True)
    policy_db_names3, data_db_names3 = dataset._get_db_names(include_parents=True)

    assert all(db_name.startswith(scan_prefix) for db_name in data_db_names1)
    assert data_db_names1 == data_db_names2
    assert data_db_names1 == data_db_names3
    assert policy_db_names2 == policy_db_names1
    assert policy_db_names3 == policy_db_names2


def test_proposal_session(session, esrf_data_policy):
    scan_saving = session.scan_saving
    proposal_name1 = "hg999"
    proposal_name2 = "hg888"
    proposal_name3 = "hg777"
    dt = datetime.datetime.now().replace(day=1)
    year = dt.year
    session_name1 = dt.strftime(scan_saving._proposal_session_name_format)
    dt = dt.replace(year=dt.year - 1)
    session_name2 = dt.strftime(scan_saving._proposal_session_name_format)
    dt = dt.replace(year=dt.year - 1)
    session_name3a = dt.strftime(scan_saving._proposal_session_name_format)
    dt = dt.replace(year=year + 1)
    session_name3b = dt.strftime(scan_saving._proposal_session_name_format)

    # Session directory does not exist
    scan_saving.proposal_name = proposal_name1
    root_path1 = scan_saving.root_path
    assert scan_saving.proposal_session_name == session_name1

    root_path2a = root_path1.replace(proposal_name1, proposal_name2)
    root_path2b = root_path2a.replace(session_name1, session_name2)
    os.makedirs(root_path2a)

    root_path3a = root_path1.replace(proposal_name1, proposal_name3)
    root_path3a = root_path3a.replace(session_name1, session_name3a)
    root_path3b = root_path3a.replace(session_name3a, session_name3b)
    os.makedirs(root_path3a)
    os.makedirs(root_path3b)

    # Session directory exists
    scan_saving.proposal_name = proposal_name2
    assert scan_saving.proposal_name == proposal_name2
    assert scan_saving.proposal_session_name == session_name1
    actual = os.path.normpath(os.path.join(scan_saving.root_path, ".."))
    expected = os.path.normpath(os.path.join(root_path2a, ".."))
    assert actual == expected

    # Session directory does not exist
    scan_saving.proposal_session_name = session_name2
    assert scan_saving.proposal_session_name == session_name1
    os.makedirs(root_path2b)

    # Session directory exists
    scan_saving.proposal_session_name = session_name2
    assert scan_saving.proposal_session_name == session_name2
    actual = os.path.normpath(os.path.join(scan_saving.root_path, ".."))
    expected = os.path.normpath(os.path.join(root_path2b, ".."))
    assert actual == expected

    # Session directory does not exist
    scan_saving.proposal_session_name = session_name3a
    assert scan_saving.proposal_session_name == session_name2

    scan_saving.proposal_session_name = session_name1
    assert scan_saving.proposal_session_name == session_name1

    # Multiple session directories exist
    scan_saving.proposal_name = proposal_name3
    assert scan_saving.proposal_name == proposal_name3
    assert scan_saving.proposal_session_name == session_name3a
    actual = os.path.normpath(os.path.join(scan_saving.root_path, ".."))
    expected = os.path.normpath(os.path.join(root_path3a, ".."))
    assert actual == expected
