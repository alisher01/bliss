from bliss.icat.nexus import create_nxtreedict
from tests.utils.compare import deep_compare


def test_icat_metadata_to_nexus(session, esrf_data_policy):
    dataset = session.scan_saving.dataset
    dataset.add_techniques("FLUO")
    dataset.metadata.FLUO.i0 = 1
    dataset.metadata.FLUO.measurement.i0 = 0.5
    dataset.gather_metadata()
    metadict = dataset.get_current_icat_metadata()

    nxtreedict = create_nxtreedict(metadict, session.icat_metadata._icat_fields)
    expected = {
        "@NX_class": "NXentry",
        "FLUO": {
            "@NX_class": "NXsubentry",
            "i0": 1.0,
            "measurement": {
                "@NX_class": "NXcollection",
                "i0": 0.5,
                "i0@units": "photons/s",
            },
        },
        "instrument": {
            "@NX_class": "NXinstrument",
            "variables": {
                "@NX_class": "NXcollection",
                "name": ["roby", "robz"],
                "value": [0, 0],
            },
            "insertion_device": {
                "@NX_class": "NXinsertion_device",
                "gap": {
                    "@NX_class": "NXpositioner",
                    "name": ["roby", "robz"],
                    "value": [0, 0],
                },
            },
            "primary_slit": {
                "@NX_class": "NXslit",
                "name": "primary_slit",
                "horizontal_gap": 0,
                "horizontal_offset": 0,
                "vertical_gap": 0,
                "vertical_offset": 0,
            },
            "detector01": {"@NX_class": "NXdetector", "name": "diode1"},
            "detector02": {"@NX_class": "NXdetector", "name": "diode2"},
        },
        "sample": {
            "@NX_class": "NXsample",
            "name": "sample",
            "positioners": {"@NX_class": "NXpositioner", "name": "roby", "value": 0},
        },
        "definition": "FLUO",
        "start_time": nxtreedict["start_time"],
    }
    deep_compare(nxtreedict, expected)
