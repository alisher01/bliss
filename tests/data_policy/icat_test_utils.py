import os
import pytest
from gevent.queue import Empty


def expected_icat_mq_message(scan_saving, dataset=False, tango=False):
    """Information expected to be received by ICAT message queue"""
    url = "http://www.esrf.fr/icat"
    if dataset:
        start = f'<tns:dataset xmlns:tns="{url}" complete="true">'
        end = "</tns:dataset>"
        exptag = "investigation"
    else:
        start = f'<tns:investigation xmlns:tns="{url}">'
        end = "</tns:investigation>"
        exptag = "experiment"
    proposal = f"<tns:{exptag}>{scan_saving.proposal_name}</tns:{exptag}>"
    beamline = f"<tns:instrument>{scan_saving.beamline}</tns:instrument>"
    info = {"start": start, "end": end, "proposal": proposal, "beamline": beamline}
    if dataset:
        info["dataset"] = f"<tns:name>{scan_saving.dataset_name}</tns:name>"
        if tango:
            sample = f'<tns:sample xmlns:tns="{url}"><tns:name>{scan_saving.collection_name}</tns:name></tns:sample>'
        else:
            sample = f"<tns:sample><tns:name>{scan_saving.collection_name}</tns:name></tns:sample>"
        info["sample"] = sample
        info["path"] = f"<tns:location>{scan_saving.icat_root_path}</tns:location>"
    return info


def assert_icat_received(icat_subscriber, expected_message, timeout=10):
    """Check whether ICAT received the correct information"""
    print("\nWaiting for ICAT message ...")
    icat_received = icat_subscriber.get(timeout=timeout)
    print(f"Validating ICAT message:\n {icat_received}")
    for k, v in expected_message.items():
        if k == "start":
            assert icat_received.startswith(v), k
        elif k == "end":
            assert icat_received.endswith(v), k
        else:
            assert v in icat_received, k


def assert_icat_metadata_received(icat_subscriber, phrases, timeout=10):
    """Check whether ICAT received the correct information"""
    print("\nWaiting for ICAT message ...")
    icat_received = icat_subscriber.get(timeout=timeout)
    if isinstance(phrases, str):
        phrases = [phrases]
    for phrase in phrases:
        assert phrase in icat_received


def assert_logbook_received(
    icat_logbook_subscriber,
    messages,
    timeout=10,
    complete=False,
    category=None,
    scan_saving=None,
    beamline_only=None,
):
    if not category:
        category = "comment"
    print("\nWaiting of ICAT logbook message ...")
    logbook_received = icat_logbook_subscriber.get(timeout=timeout)
    print(f"Validating ICAT logbook message: {logbook_received}")
    assert logbook_received["category"] == category

    if scan_saving is not None:
        assert logbook_received["instrument"] == scan_saving.beamline
        if beamline_only:
            assert "investigation" not in logbook_received
            assert "datasetName" not in logbook_received
        else:
            assert logbook_received["investigation"] == scan_saving.proposal_name
            assert logbook_received["datasetName"] == scan_saving.dataset_name

    content = logbook_received["content"]
    if isinstance(messages, str):
        messages = [messages]
    for message, adict in zip(messages, content):
        if complete:
            assert adict["text"] == message
        else:
            assert message in adict["text"]


def assert_icat_received_current_proposal(scan_saving, icat_subscriber):
    assert_icat_received(icat_subscriber, expected_icat_mq_message(scan_saving))


def assert_icat_received_nothing(icat_subscriber, timeout=1):
    with pytest.raises(Empty):
        icat_subscriber.get(timeout=timeout)


def create_dataset(scan_saving):
    """Create the dataset on disk and in Bliss without having to run a scan"""
    paths = [scan_saving.root_path, scan_saving.icat_root_path]
    for path in paths:
        if not os.path.exists(path):
            os.makedirs(path)
    assert scan_saving.dataset is not None
