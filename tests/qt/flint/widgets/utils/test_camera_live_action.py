import pytest
from silx.gui import qt
from bliss.flint.widgets.utils import camera_live_action
from bliss.flint.manager import monitoring


@pytest.fixture
def monitoring_any_detector(mocker):
    proxy = mocker.Mock()
    proxy.acq_expo_time = 2
    proxy.latency_time = 10
    mocker.seal(proxy)

    class AnyDetectorScan(monitoring.MonitoringScan):
        def isLive(self):
            return True

        def getProxy(self):
            return proxy

    yield AnyDetectorScan()


@pytest.fixture
def monitoring_frelon(mocker):
    proxy = mocker.Mock()
    proxy.acq_expo_time = 2
    proxy.shutter_close_time = 1
    proxy.latency_time = 10
    mocker.seal(proxy)

    class FrelonScan(monitoring.MonitoringScan):
        def isLive(self):
            return True

        def getProxy(self):
            return proxy

    yield FrelonScan()


def openToolButtonMenu(toolAction):
    """Open and close a QToolButton menu sequentially"""

    def close_later():
        menu = toolAction.menu()
        if menu.isVisible():
            toolAction.menu().close()

    timer = qt.QTimer()
    timer.timeout.connect(close_later)
    timer.start(100)

    toolAction.click()


def test_camera_without_shutter(local_flint, monitoring_any_detector):
    action = camera_live_action.CameraLiveAction(None)
    action.setScan(monitoring_any_detector)
    openToolButtonMenu(action.defaultWidget())


def test_camera_with_shutter(local_flint, monitoring_frelon):
    action = camera_live_action.CameraLiveAction(None)
    action.setScan(monitoring_frelon)
    openToolButtonMenu(action.defaultWidget())
