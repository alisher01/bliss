"""Testing image plot."""

from silx.gui.plot import PlotWindow
from bliss.flint.model import flint_model
from bliss.flint.manager.manager import ManageMainBehaviours
from bliss.flint.widgets.utils import export_action


def create_flint_model():
    flint = flint_model.FlintState()
    return flint


def create_manager():
    manager = ManageMainBehaviours()
    flintModel = create_flint_model()
    manager.setFlintModel(flintModel)
    return manager


def test_logbook(local_flint, beacon, icat_backend, icat_logbook_subscriber):
    manager = create_manager()
    plot = PlotWindow()
    action = export_action.ExportToLogBookAction(plot, plot)
    action.setFlintModel(manager.flintModel())

    # The action is not available
    assert not action.isEnabled()

    # The action is now available
    kwargs = dict(icat_backend)
    kwargs["proposal"] = "id001234"
    kwargs["beamline"] = "id00"
    config = {"disable": False, "kwargs": kwargs}
    manager.setIcatClientConfig(config)
    assert action.isEnabled()

    # The action can use it
    action.trigger()

    action.deleteLater()
    plot.deleteLater()

    message = icat_logbook_subscriber.get(timeout=10)
    assert message["base64"].startswith("data:image/png;base64")
    assert message["investigation"] == "id001234"
    assert message["instrument"] == "id00"


def test_logbook_send_data(local_flint):
    class IcatClientMockup:
        def __init__(self):
            self.data = None
            self.mimetype = None

        def send_binary_data(self, data: bytes, mimetype: str):
            self.data = data
            self.mimetype = mimetype

    model = create_flint_model()
    client = IcatClientMockup()
    model.setIcatClient(client)
    plot = PlotWindow()
    action = export_action.ExportToLogBookAction(plot, plot)
    action.setFlintModel(model)

    assert action.isEnabled()
    action.trigger()

    assert client.data is not None
    assert client.mimetype == "image/png"

    action.deleteLater()
    plot.deleteLater()
