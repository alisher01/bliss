import subprocess
import pytest
import gevent
import sys

from bliss.common import scans
from bliss.testutils import process_utils

SERVICE = [sys.executable, "-u", "-m", "bliss.comm.service"]


@pytest.fixture
def sim_ct_gauss_service(beacon):
    proc = subprocess.Popen(SERVICE + ["sim_ct_gauss_service"], stdout=subprocess.PIPE)
    process_utils.wait_for(proc.stdout, "Starting service sim_ct_gauss_service")
    gevent.sleep(1)
    proc.stdout.close()
    sim = beacon.get("sim_ct_gauss_service")
    yield sim
    sim._rpc_connection.close()
    process_utils.wait_terminate(proc)


def test_simple_counter_info(sim_ct_gauss_service):
    assert "sim_ct_gauss_service" in sim_ct_gauss_service.__info__()


def test_simple_counter_ct(session, sim_ct_gauss_service):
    s = scans.ct(0, sim_ct_gauss_service)
    data = s.get_data()
    assert all([100.0] == data["sim_ct_gauss_service"])
