from collections.abc import Mapping
import numpy


def deep_compare(d, u):
    """using logic of deep update to compare two dictionaries"""
    stack = [(d, u)]
    while stack:
        d, u = stack.pop(0)
        assert set(d.keys()) == set(u.keys())
        for k, v in u.items():
            if isinstance(v, Mapping):
                stack.append((d[k], v))
            elif isinstance(v, numpy.ndarray) and v.size > 1:
                assert d[k].shape == v.shape
                assert d[k].dtype == v.dtype
                if d[k].dtype != object:
                    assert all(numpy.isnan(d[k].flatten()) == numpy.isnan(v.flatten()))
                    mask = numpy.logical_not(numpy.isnan(v.flatten()))
                    assert all((d[k].flatten() == v.flatten())[mask])
                else:
                    assert all(d[k].flatten() == v.flatten())
            elif isinstance(v, (list, tuple)):
                assert list(d[k]) == list(v)
            else:
                assert d[k] == v
