import re
import pytest
import gevent
from bliss.common.scans import loopscan
from bliss.scanning.group import Sequence
from bliss.scanning.group import Group
from bliss.testutils.data_policies import set_data_policy


MAX_TTL_RANGE = 10  # seconds
UUID_PATTERN = re.compile(
    "^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}$"
)


@pytest.fixture
def esrf_session(session, icat_backend):
    set_data_policy(session, "esrf")
    session.scan_saving.writer = "null"
    yield session


@pytest.fixture
def esrf_writer_session(session, icat_backend, nexus_writer_service):
    set_data_policy(session, "esrf")
    session.scan_saving.writer = "nexus"
    yield session


def get_parent_nodes(*db_names) -> set:
    """db_names are included in the parents"""
    parent_db_names = set()
    for db_name in db_names:
        parts = db_name.split(":")
        for n in range(1, len(parts)):
            parent_db_names.add(":".join(parts[:n]))
    return parent_db_names


def is_associated_to_parent(key, parent_db_names) -> bool:
    return any(
        key.startswith(db_name) and ":" not in key[len(db_name) :]
        for db_name in parent_db_names
    )


def assert_scan_expiration(
    redis_data_conn, session_name, scan_db_names, parents_expire=True
):
    keys = [db_name.decode() for db_name in redis_data_conn.keys("*")]
    parent_db_names = get_parent_nodes(*scan_db_names)
    parent_db_names |= set(scan_db_names)

    visited = {key: None for key in keys}
    for prefix in scan_db_names:
        children_ttl = None
        parent_ttl = None
        for key in keys:
            ttl = redis_data_conn.ttl(key)
            if not key.startswith(session_name):
                assert UUID_PATTERN.match(key), key
                visited[key] = "reader"
            elif key.startswith(prefix + ":"):
                # Redis key associated to a child node
                assert ttl > 0, key
                if children_ttl is None:
                    children_ttl = ttl
                assert abs(children_ttl - ttl) < MAX_TTL_RANGE, key
                visited[key] = "data"
            elif is_associated_to_parent(key, parent_db_names):
                # Redis key associated to the node or a parent node
                if parents_expire:
                    assert ttl > 0, key
                else:
                    assert ttl == -1, key
                if parent_ttl is None:
                    parent_ttl = ttl
                assert abs(parent_ttl - ttl) < MAX_TTL_RANGE, key
                visited[key] = "parent"
    for key, key_type in visited.items():
        assert key_type is not None, (key, key_type)


def normal_scan(session):
    diode = session.env_dict["diode"]
    scan = loopscan(1, 0.1, diode)
    return [scan.node.db_name]


def interrupted_scan(session):
    diode = session.env_dict["diode"]
    scan = loopscan(1000, 0.1, diode, run=False)

    gscan = gevent.spawn(scan.run)
    gevent.sleep(2)
    gscan.kill(KeyboardInterrupt)

    return [scan.node.db_name]


def sequence_scan(session):
    diode = session.env_dict["diode"]
    scan1 = loopscan(3, 0.1, diode)

    gevent.sleep(1)

    seq = Sequence()
    with seq.sequence_context() as seq_context:
        scan2 = loopscan(3, 0.05, diode)
        seq_context.add(scan2)
        seq_context.add(scan1)

    return [scan.node.db_name for scan in (seq, scan1, scan2)]


def group_scan(session):
    diode = session.env_dict["diode"]
    scan1 = loopscan(3, 0.1, diode)
    gevent.sleep(1)

    scan2 = loopscan(3, 0.05, diode)
    gevent.sleep(1)

    group = Group(scan2, scan1)
    return [scan.node.db_name for scan in (group, scan1, scan2)]


def test_expiration_after_scan(session, redis_data_conn):
    db_names = normal_scan(session)
    assert_scan_expiration(redis_data_conn, session.name, db_names, parents_expire=True)


def test_expiration_after_scan_esrf(esrf_session, redis_data_conn):
    db_names = normal_scan(esrf_session)
    assert_scan_expiration(
        redis_data_conn, esrf_session.name, db_names, parents_expire=False
    )


def test_expiration_after_interrupted_scan(session, redis_data_conn):
    db_names = interrupted_scan(session)
    assert_scan_expiration(redis_data_conn, session.name, db_names, parents_expire=True)


def test_expiration_after_interrupted_scan_esrf(esrf_session, redis_data_conn):
    db_names = interrupted_scan(esrf_session)
    assert_scan_expiration(
        redis_data_conn, esrf_session.name, db_names, parents_expire=False
    )


def test_expiration_after_sequence(session, redis_data_conn):
    db_names = sequence_scan(session)
    assert_scan_expiration(redis_data_conn, session.name, db_names, parents_expire=True)


def test_expiration_after_sequence_esrf(esrf_session, redis_data_conn):
    db_names = sequence_scan(esrf_session)
    assert_scan_expiration(
        redis_data_conn, esrf_session.name, db_names, parents_expire=False
    )


def test_expiration_after_group(session, redis_data_conn):
    db_names = group_scan(session)
    assert_scan_expiration(redis_data_conn, session.name, db_names, parents_expire=True)


def test_expiration_after_group_esrf(esrf_session, redis_data_conn):
    db_names = group_scan(esrf_session)
    assert_scan_expiration(
        redis_data_conn, esrf_session.name, db_names, parents_expire=False
    )


def assert_policy_expiration(
    redis_data_conn,
    session_name,
    scan_db_name,
    top_policy_db_name,
    new_top_policy_db_name=None,
    old_top_policy_db_name=None,
):
    rest_keys = {key.decode() for key in redis_data_conn.keys("*")}
    data_keys = {
        key.decode() for key in redis_data_conn.keys(scan_db_name + ":*")
    }  # scan children
    policy_keys = {
        key.decode() for key in redis_data_conn.keys(top_policy_db_name + "*")
    }  # policy node + children
    rest_keys -= set(policy_keys)
    policy_keys -= set(data_keys)
    rest_session_keys = {
        db_name for db_name in rest_keys if db_name.startswith(session_name)
    }
    rest_other_keys = rest_keys - rest_session_keys

    keep_ttl = None
    for key in data_keys:
        ttl = redis_data_conn.ttl(key)
        if keep_ttl is None:
            keep_ttl = ttl
        assert ttl > 0, key
        assert abs(keep_ttl - ttl) < MAX_TTL_RANGE, key

    keep_ttl = None
    for key in policy_keys:
        ttl = redis_data_conn.ttl(key)
        if keep_ttl is None:
            keep_ttl = ttl
        assert ttl > 0, key
        assert abs(keep_ttl - ttl) < MAX_TTL_RANGE, key

    expire_parent_db_names = set()
    if new_top_policy_db_name:
        parent_db_names1 = get_parent_nodes(top_policy_db_name)
        parent_db_names2 = get_parent_nodes(new_top_policy_db_name)
        expire_parent_db_names |= parent_db_names1 - parent_db_names2
    if old_top_policy_db_name:
        parent_db_names1 = get_parent_nodes(top_policy_db_name)
        parent_db_names2 = get_parent_nodes(old_top_policy_db_name)
        parent_db_names2.add(old_top_policy_db_name)
        expire_parent_db_names |= parent_db_names2 - parent_db_names1

    keep_ttl = None
    for key in rest_session_keys:
        ttl = redis_data_conn.ttl(key)
        if is_associated_to_parent(key, expire_parent_db_names):
            if keep_ttl is None:
                keep_ttl = ttl
            assert ttl > 0, key
            assert abs(keep_ttl - ttl) < MAX_TTL_RANGE, key
        else:
            assert ttl == -1, key

    for key in rest_other_keys:
        assert UUID_PATTERN.match(key), key


def test_expiration_esrf_dataset(esrf_writer_session, redis_data_conn):
    scan_db_name = normal_scan(esrf_writer_session)[0]
    dataset = esrf_writer_session.scan_saving.dataset
    esrf_writer_session.scan_saving.newdataset(None)
    assert dataset.is_registered
    assert_policy_expiration(
        redis_data_conn, esrf_writer_session.name, scan_db_name, dataset.node.db_name
    )


def test_expiration_esrf_collection(esrf_writer_session, redis_data_conn):
    scan_db_name = normal_scan(esrf_writer_session)[0]
    dataset = esrf_writer_session.scan_saving.dataset
    esrf_writer_session.scan_saving.newcollection("newcollection")
    assert dataset.is_registered
    assert_policy_expiration(
        redis_data_conn, esrf_writer_session.name, scan_db_name, dataset.node.db_name
    )


def test_expiration_esrf_proposal(esrf_writer_session, redis_data_conn):
    scan_db_name = normal_scan(esrf_writer_session)[0]
    dataset = esrf_writer_session.scan_saving.dataset
    proposal = esrf_writer_session.scan_saving.proposal
    esrf_writer_session.scan_saving.newproposal("new" + proposal.name)
    newproposal = esrf_writer_session.scan_saving.proposal
    assert_policy_expiration(
        redis_data_conn,
        esrf_writer_session.name,
        scan_db_name,
        proposal.node.db_name,
        new_top_policy_db_name=newproposal.node.db_name,
    )

    esrf_writer_session.scan_saving.newproposal(proposal.name)
    assert_policy_expiration(
        redis_data_conn,
        esrf_writer_session.name,
        scan_db_name,
        dataset.node.db_name,
        old_top_policy_db_name=newproposal.node.db_name,
    )
