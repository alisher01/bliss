# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2022 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import pytest
from tango.gevent import DeviceProxy


def test_remote_controller_no_server(remo):
    with pytest.raises(ValueError):
        remo.move(1)


def test_remote_controller(motor_tango_server, remo, remo2, ports):
    assert remo.controller is remo2.controller

    # DeviceProxy for bare Tango access to the real controller
    proxy = DeviceProxy(f"tango://localhost:{ports.tango_port}/id00/tango/remo_ctrl")

    remo.move(0.1)
    ctrl_pos = 0.1 * remo.steps_per_unit
    assert remo.controller.read_position(remo) == ctrl_pos
    assert proxy.read_position("remo") == ctrl_pos

    remo.acceleration = 57
    ctrl_acc = 57 * remo.steps_per_unit
    assert remo.controller.read_acceleration(remo) == ctrl_acc
    assert proxy.read_acceleration("remo") == ctrl_acc
