# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2022 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import re
import pytest

from bliss.common.closed_loop import ClosedLoopState


def test_closed_loop_any_axis(robz):
    assert hasattr(robz, "closed_loop")
    assert robz.closed_loop is None


def test_closed_loop_config(servo1):
    # incorrect state
    servo1.closed_loop.config["state"] = "foo"
    with pytest.raises(KeyError):
        servo1.closed_loop.apply_config()

    # missing state
    del servo1.closed_loop.config["state"]
    with pytest.raises(RuntimeError) as excinfo:
        servo1.closed_loop.apply_config()
    assert "configuration must contains" in str(excinfo.value)

    servo1.closed_loop.config["state"] = "on"

    # missing param
    del servo1.closed_loop.config["ki"]
    with pytest.raises(RuntimeError) as excinfo:
        servo1.closed_loop.apply_config()
    assert "configuration must contains" in str(excinfo.value)


def test_closed_loop_on_off(servo1):
    assert servo1.closed_loop.config["state"] == "on"
    assert servo1.closed_loop.state == ClosedLoopState.ON

    servo1.closed_loop.off()
    assert servo1.closed_loop.state == ClosedLoopState.OFF
    assert servo1.controller.get_closed_loop_state(servo1) == ClosedLoopState.OFF

    for _ in range(2):
        servo1.closed_loop.on()
        assert servo1.closed_loop.state == ClosedLoopState.ON
        assert servo1.controller.get_closed_loop_state(servo1) == ClosedLoopState.ON


def test_closed_loop_apply_config(servo1):
    config_kp = servo1.closed_loop.kp

    servo1.closed_loop.kp += 1
    assert config_kp + 1 == servo1.closed_loop.kp
    servo1.closed_loop.apply_config()
    assert config_kp == servo1.closed_loop.kp

    # axis.apply_config implies closed_loop.apply_config
    servo1.closed_loop.kp += 1
    assert config_kp + 1 == servo1.closed_loop.kp
    servo1.apply_config()
    assert config_kp == servo1.closed_loop.kp

    # closed_loop.apply_config doesn't imply axis.apply_config
    config_acc = servo1.acceleration
    servo1.acceleration += 100
    servo1.closed_loop.apply_config()
    assert config_acc + 100 == servo1.acceleration


def test_info_axis(servo1, capsys):
    captured = servo1.__info__()

    output = "AXIS:\n"
    output += "     name (R): servo1\n"
    output += "     unit (R): mm\n"
    output += "     offset (R): 0.00000\n"
    output += "     backlash (R): 0.00000\n"
    output += "     sign (R): 1\n"
    output += "     steps_per_unit (R): 1000.00\n"
    output += "     tolerance (R) (to check pos. before a move): 0.0001\n"
    output += "     motion_hooks (R): []\n"
    output += "     limits (RW):    Low: -1000.00000 High: 1000.00000    (config Low: -1000.00000 High: 1000.00000)\n"
    output += "     dial (RW): 0.00000\n"
    output += "     position (RW): 0.00000\n"
    output += "     state (R): READY (Axis is READY)\n"
    output += "     acceleration (RW):  200.00000  (config:  200.00000)\n"
    output += "     acctime (RW):         0.50000  (config:    0.50000)\n"
    output += "     velocity (RW):      100.00000  (config:  100.00000)\n"
    output += "     velocity_low_limit (RW):       10.00000  (config: 10.0)\n"
    output += "     velocity_high_limit (RW):      500.00000  (config: 500.0)\n"
    output += "Controller name: HIDEN_BY_TEST\n"
    output += "\n"
    output += "MOCKUP AXIS:\n"
    output += "    this axis (servo1) is a simulation axis\n"
    output += "\n"
    output += "ENCODER:\n"
    output += "     None\n"
    output += "\n"
    output += "CLOSED LOOP:\n"
    output += "     state: ON\n"
    output += "     kp: 1\n"
    output += "     ki: 2\n"
    output += "     kd: 3\n"

    # hide non deterministic name like "Mockup_8a107e8f72d892c135fe61c1601848b4"
    captured = re.sub(
        "Controller name: .*\n", "Controller name: HIDEN_BY_TEST\n", captured
    )

    assert captured == output


def test_info_closed_loop(servo1, capsys):
    captured = servo1.closed_loop.__info__()

    output = "CLOSED LOOP:\n"
    output += "     state: ON\n"
    output += "     kp: 1\n"
    output += "     ki: 2\n"
    output += "     kd: 3\n"

    assert captured == output
