# Live scatter

If the scan contains counters data which have to be displayed is 2D, the
scatter widget will be displayed automatically.

That's the case for `mesh` scans.

![Flint screenshot](img/flint-scatter-widget.png)

## APIs

Here is the available commands which can be used from the BLISS shell.

### Get the plot

```python
f = flint()
amesh(sy,-.1,.1,20,sz,-.3,0,30,.001,fluo_diode)
f.wait_end_of_scans()

p = f.get_live_plot("default-scatter")
```

### Interactive data selection

This plot provides an API to [interact with region of interest](flint_interaction.md).

### Colormap

An API is provided to custom the colormap.

```python
ct(tomocam)

# Make sure the scan was also completed in flint side
f = flint()
amesh(sy,-.1,.1,20,sz,-.3,0,30,.001,fluo_diode)
f.wait_end_of_scans()

p = f.get_live_plot(image_detector="tomocam")

p.set_colormap(lut="gray",
               vmin=0, vmax="auto",
               normalization="log",
               autoscale_mode="stddev3")
```
