Open the [Nexus compliant](https://www.nexusformat.org/) HDF5 file written by the [Nexus writer](data/config_nexus_writer.md) with [pymca](http://pymca.sourceforge.net/)

```bash
pymca /data/visitor/hg123/id21/sample/sample_0001/sample_0001.h5
```
