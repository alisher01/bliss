# AutoFilter

## Introduction
### Auto filtering for step-by-step scan
Autof stands for "automatic filter", it provides a way of activating automatically some beam filtering (attenuation) during the step-by-step scans in order to keep the beam intensity in an acceptable range of count rate and to protect the detectors.

During the scan the count (acquisition point) is validated or repeated until the detector count-rate fits with the specified range for linearity purpose for instance.

### The AutoFilter controller
The AutoFilter controller requires two input counters, one as the **detector** and one as the **monitor**. Monitor and detector can be of any type, sampling, integrating, 1D roi counter, Lima roi counter and so on.

The AutoFilter controller provides its own `scan` rountines, like ascan, dscan, a2scan, d2scan, a3scan, d3scan and finally lookupscan. The actual BLISS scan framework cannot be changed to allow repeat of count. So one will need to call <myautof>.dscan() to do a scan with auto-filter activated.

The AutoFilter provides some counters, the `transmission`, the `filter-position`, the `corrected-detector` and the `corrected-counter-to-monitor` ratio counters.
In addition one can configure a list of counters to be corrected with the current transmission. The corrected counters will be named <mycounter><corr_suffix>, the `suffix_for_corr_counter` can be set in the configuration, the default suffix is **_corr**.

### The FilterSet controllers
The AutoFilter controller needs a **FilterSet** controller to apply transmission changes during a scan. You can have several FilterSet controller defined in your session, but only one will be active during a scan. The filterset can be change at any time by using the filterset property (`.filterset`).

The filterset is responsible of the effective transmission calculation. The filterset configuration should provide the filter definitions (element, thickness or density) and the `energy axis` to read the current beamline energy in keV.

Two types of FilterSet are today supported, the **FilterSet_Wheel** and the **FilterSet_Wago**. 

The Wheel filterset provides N filters and the filter change is made via a motor position. On ESRF ID10 the wheels have 20 different filters.

The Wago filterset is the former ESRF ISG filter box, a pneumatic-driven jacks fo up to to 4 filters. Filters can be all inserted into the beam trajectory, Which makes possible to get up to 16 different transmissions. 

There are some other filterset available at ESRF for instance the Maatel filterset runs on BM28 (XMAS) which will be added.

On BM02 the wago filterset is equipped with 12 foils which means 4096 possible transmissions. They use the `enabled_filters` properter to reduce the number of total transmissions according to the experiment requirements.

The FilterSet controller calculate the effective filter tranmsission with the given energy axis and the filter definition.


## Example configuration

### AutoFilter
```yaml
- plugin: bliss
  class: AutoFilter
  name: autof_eh1
  package: bliss.common.auto_filter
  detector_counter_name: roi1
  monitor_counter_name: mon
  min_count_rate: 20000
  max_count_rate: 50000
  energy_axis: $eccmono
  filterset: $filtW1

# optionnal parameters
  always_back: True
  counters:
    - counter_name: curratt
      tag: fiteridx
    - counter_name: transm
      tag: transmission
   - counter_name: ratio
     tag: ratio
  suffix_for_corr_counter: "_corr"
  counters_for_correction:
    - det
    - apdcnt
```
### FilterSet

#### Wheel FilterSet
``` yaml
With NO density:
---------------
- name: filtW0
  package: bliss.common.auto_filter.filterset_wheel
  class: FilterSet_Wheel
  rotation_axis: $att1
  filters:
    - position: 0
      material: Cu
      thickness: 0
    - position: 1
      material: Cu
      thickness: 0.04673608
With Density:
-------------
- name: filtW0
  package: bliss.common.auto_filter.filterset_wheel
  class: FilterSet_Wheel
  rotation_axis: $att1
  filters:
    - position: 0
      material: Cu
      thickness: 0
      density: 8.94

    - position: 1
      material: Mo
      thickness: 0.055
      density: 10.22
With pairs transmission/energy:
------------------------------
- name: filtW0
  package: bliss.common.auto_filter.filterset_wheel
  class: FilterSet_Wheel
  rotation_axis: $att1
  filters:
    - position: 0
      material: Ag
      thickness: 0.1
      transmission: 0.173
      energy: 16

    - position: 1
      material: Ag
      thickness: 0.2
      transmission: 0.0412
      energy: 16
```

#### Wago FilterSet
``` yaml
With NO density:
---------------
- name: filtA
  package: bliss.common.auto_filter.filterset_wago
  class: FilterSet_Wago
  wago_controller: $wcid10f
  wago_cmd: filtA
  wago_status: fstatA
  inverted: True
  overlap_time: 0.1
  settle_time: 0.3
  filters:
    - position: 0
      material: Cu
      thickness: 0
    - position: 1
      material: Cu
      thickness: 0.04673608
    - position: 2
      material: Cu
      thickness: 0.09415565
    - position: 3
      material: Cu
      thickness: 0.14524267
With Density:
-------------
- name: filtA
  package: bliss.common.auto_filter.filterset_wago
  class: FilterSet_Wago
  wago_controller: $wcid10f
  wago_cmd: filtA
  wago_status: fstatA
  inverted: True
  overlap_time: 0.1
  settle_time: 0.3
  filters:
    - position: 0
      material: Cu
      thickness: 0
      density: 8.94
    - position: 1
      material: Mo
      thickness: 0.055
      density: 10.22
With pairs transmission/energy:
------------------------------
- name: filtA
  package: bliss.common.auto_filter.filterset_wago
  class: FilterSet_Wago
  wago_controller: $wcid10f
  wago_cmd: filtA
  wago_status: fstatA
  inverted: True
  overlap_time: 0.1
  settle_time: 0.3
  filters:
    - position: 0
      material: Ag
      thickness: 0.1
      transmission: 0.173
      energy: 16

    - position: 1
      material: Ag
      thickness: 0.2
      transmission: 0.0412
      energy: 16

```

### FilterSet counters (optional)
In addition to the configuration above, one can define counters for each filterset. The available counters are the filter position (filteridx) and the transmission. Example of counter configuration:

``` yaml
  counters:
    - name: position
      tag: filteridx
    - name: transm
      tag: transmission

```

## Usage


### How to read filterset status ?

just run the filterset:
```python

EH1_EXP [6]: filtA
Filter = 0, transm = 1 @ 20.95 keV
    Out [6]: Filterset Wago: filtA
              - Wago: wcid10f
              - Position  Mat. Thickness    Transm. @ 20.95 keV:
                ---------------------------------------------
                0         Al   0.10000000   0.9222
                1         Al   0.20000000   0.85046
                2         Al   0.40000000   0.72328
                3         Al   0.80000000   0.52313


              Combined filter set:
              - Mask  Transm. @ 20.95 keV:
                --------------------------
                0     1
                1     0.9222
                2     0.85046
                3     0.78429
                4     0.72328
                5     0.66701
                6     0.61512
                7     0.56726
                8     0.52313
                9     0.48243
                10    0.4449
                11    0.41029
                12    0.37837
                13    0.34893
                14    0.32179
                15    0.29675

             Current filter = 0, transmission = 1 @ 20.95 keV
             Enabled filters  = [0, 1, 2, 3]

```
### How to get autof controller status

```python
EH1_EXP [11]: autof_eh1
Filter = 2, transm = 0.081392 @ 20.95 keV
    Out [11]: Parameter              Value
              ---------------------  ------------------------------------
              monitor_counter_name   p201_cc1:ct2_counters_controller:mon
              detector_counter_name  p201_cc1:ct2_counters_controller:det
              min_count_rate         15000
              max_count_rate         60000
              always_back            True

              Active filterset: filtW0
              Energy axis eccmono: 20.95 keV

              Current filter = 2, transmission = 0.0813921

              Table of Effective Filters :
              Idx    Transm.    Max.cntrate    Opti.cntrate    Min.cntrate
              -----  ---------  -------------  --------------  -------------
              0      1          6e+04          0               0
              1      0.2879     2.084e+05      4.243e+04       4.243e+04
              2      0.08139    7.372e+05      1.474e+05       1.474e+05
              3      0.02087    2.875e+06      5.213e+05       5.213e+05
              5      0.001578   3.802e+07      2.033e+06       2.033e+06
              18     2.629e-10  2.283e+14      2.689e+07       2.689e+07

```

The autof controller will always optimize which transmissions to use for the working count-rate range and energy.

### Enable or disable filterset filters (foils)

Some filterset can be equiped with many filters which are not useful for your experiment. In that case you have the possibility to enable or disable filters. Use the filterset controller  `enabled_filters` property list to choose which filters to use.

The autof controller will be automatically updated with the change and it will not use the disabled filters to apply transmission changes.

```python

EH1_EXP [25]: filtA.enabled_filters
    Out [25]: [0, 1, 2, 3]

```
Now we enable only 2 filters, 0 and 3:
```python
EH1_EXP [26]: filtA.enabled_filters=[0,3]

EH1_EXP [27]: filtA
Filter = 0, transm = 1 @ 20.95 keV
    Out [27]: Filterset Wago: filtA
               - Wago: wcid10f
               - Position  Mat. Thickness    Transm. @ 20.95 keV:
                 ---------------------------------------------
                 0         Al   0.10000000   0.9222
                 1         Al   0.20000000   0.85046
                 2         Al   0.40000000   0.72328
                 3         Al   0.80000000   0.52313


               Combined filter set:
               - Mask  Transm. @ 20.95 keV:
                 --------------------------
                 0     1
                 1     0.9222
                 8     0.52313
                 9     0.48243

              Current filter = 0, transmission = 1 @ 20.95 keV
              Enabled filters  = [0, 3]

```

Looking for autof controller status, only 2 combinations of filters are kept, 0 and 8:

```python
EH1_EXP [30]: autof_eh1
Filter = 0, transm = 1 @ 20.95 keV
    Out [30]: Parameter              Value
              ---------------------  ------------------------------------
              monitor_counter_name   p201_cc1:ct2_counters_controller:mon
              detector_counter_name  p201_cc1:ct2_counters_controller:det
              min_count_rate         15000
              max_count_rate         60000
              always_back            True

              Active filterset: filtA
              Energy axis eccmono: 20.95 keV

              Current filter = 0, transmission = 1

              Table of Effective Filters :
              Idx    Transm.    Max.cntrate    Opti.cntrate    Min.cntrate
              -----  ---------  -------------  --------------  -------------
              0      1          6e+04          0               0
              8      0.5231     1.147e+05      4.243e+04       2.867e+04

```

### Get/set the filterset manually

The filterset can be used without the autof controller, if you just want to set some transmission manually before starting a standard scan.

```python
EH1_EXP [12]: filtW0.filter
Filter = 2, transm = 0.081392 @ 20.95 keV
    Out [12]: 2

BLISS [4]: filtW0.filter = 7
Change filter filtW0 from 2 to 7

EH1_EXP [13]: autof_eh1.filter
Filter = 7, transm = 0.00018419 @ 20.95 keV
    Out [16]: 7

BLISS [6]: autof_eh1.filter=5
Change filter filtW0 from 7 to 5

```
### Scan with autof controller

So simple, just use the autof controller scan routines, ascan, dscan, a2scan, d2scan ...

```python
BLISS [7]: autof_eh1.dscan(simot1_autof,-1,1,20,0.1)
```

If you have the autof controller in your measurmement group you will get  counters like the filter position, the transmission, the ratio and the detector corrected value:

```python
BLISS [13]: lscnt()

Fullname                                              Shape    Controller                            Name             Alias
----------------------------------------------------  -------  ------------------------------------  ---------------  -------
...
autof_eh1:curratt                                    0D       autof_eh1                            curratt
autof_eh1:ratio                                      0D       autof_eh1                            ratio
autof_eh1:roi1_sum_corr                              0D       autof_eh1                            roi1_sum_corr
autof_eh1:transm                                     0D       autof_eh1                            transm
...
```

### Change autof filterset
The autof controller active filterset can be changed at any time by using its `filterset` propery.
In this example we change filterset from a wheel model (filtW0) to a wago model (filtA), like it it possible on ID10 beamline at ESRF.

```python
EH1_EXP [3]: autof_eh1.filterset
Filter = 2, transm = 0.081392 @ 20.95 keV
    Out [3]: Filterset Wheel: filtW0
              - Rotation axis: watt0
              - Position Mat. Thickness    Transm. @ 20.95 keV:
                -------------------------------------------------
                0        Cu   0.00000000   1
                1        Cu   0.04673608   0.2879
                2        Cu   0.09415565   0.081392
                3        Cu   0.14524267   0.020868
                4        Cu   0.19116930   0.006139
                5        Cu   0.24215921   0.0015781
                6        Cu   0.27220901   0.00070866
                7        Cu   0.32278422   0.00018419
                8        Cu   0.37046937   5.1704e-05
                9        Cu   0.42335566   1.2636e-05
                10       Cu   0.46880012   3.7653e-06
                11       Cu   0.51600567   1.0706e-06
                12       Cu   0.55780431   3.5155e-07
                13       Cu   0.59937871   1.1613e-07
                14       Cu   0.64491601   3.452e-08
                15       Cu   0.69376533   9.3943e-09
                16       Cu   0.73600000   3.0492e-09
                17       Cu   0.78200000   8.9527e-10
                18       Cu   0.82800000   2.6285e-10
                19       Cu   0.87400000   7.7175e-11

             Current filter = 2, transmission = 0.081392 @ 20.95 keV
             Enabled filters  = [0, 1, 2, 3, 5, 18]

EH1_EXP [4]:

EH1_EXP [4]: autof_eh1.filterset=filtA
EH1_EXP [5]: autof_eh1.filterset
Filter = 0, transm = 1 @ 20.95 keV
    Out [5]: Filterset Wago: filtA
              - Wago: wcid10f
              - Position  Mat. Thickness    Transm. @ 20.95 keV:
                ---------------------------------------------
                0         Al   0.10000000   0.9222
                1         Al   0.20000000   0.85046
                2         Al   0.40000000   0.72328
                3         Al   0.80000000   0.52313


              Combined filter set:
              - Mask  Transm. @ 20.95 keV:
                --------------------------
                0     1
                1     0.9222
                2     0.85046
                3     0.78429
                4     0.72328
                5     0.66701
                6     0.61512
                7     0.56726
                8     0.52313
                9     0.48243
                10    0.4449
                11    0.41029
                12    0.37837
                13    0.34893
                14    0.32179
                15    0.29675

             Current filter = 0, transmission = 1 @ 20.95 keV
             Enabled filters  = [0, 1, 2, 3]

```

### Change the detector configuration

You can dynamically change the auto-filter configuration to set a new detector and a new count-rate range. In addition the monitor detector can be changed too.


```python
BLISS [19]: autof_eh1.max_count_rate
            Out [19]: 50000

BLISS [20]: autof_eh1.min_count_rate
            Out [20]: 20000

BLISS [21]: autof_eh1.detector_counter
            Out [21]: 'roi1_sum` counter info:
                        counter type = integrating
                        fullname = bcu_simulator2:roi_counters:roi1_sum
                        unit = None


BLISS [22]: autof_eh1.monitor_counter
            Out [22]: 'simdiode1_autof` counter info:
                        counter type = sampling
                        sampling mode = MEAN
                        fullname = simulation_diode_sampling_controller:simdiode1_autof
                        unit = None
                        mode = MEAN (1)

BLISS [19]: autof_eh1.max_count_rate = 60000; autof_eh1.min_count_rate = 10000

BLISS [24]: autof_eh1.detector_counter = bcu_simulator2.roi_counters.counters.roi1_avg

BLISS [25]: autof_eh1.monitor_counter = simdiode2_autof

BLISS [34]: autof_eh1
Filter = 0, transm = 1 @ 20 keV
            Out [34]: Parameter              Value
                      ---------------------  ----------------------------------------------------
                      monitor_counter_name   simulation_diode_sampling_controller:simdiode2_autof
                      detector_counter_name  bcu_simulator2:roi_counters:roi1_avg
                      min_count_rate         10000
                      max_count_rate         60000
                      always_back            True

                      Active filterset: filtW1
                      Energy axis energy: 20 keV

                      Active filter idx 0, transmission 1

                      Table of Effective Filters :
                      Idx    Transm.     Max.cntrate    Opti.cntrate    Min.cntrate
                      -----  ----------  -------------  --------------  -------------
                      0      1           5e+04          0               0
                      1      6.873e-07   7.275e+10      3.976e+04       3.976e+04
                      2      3.838e-13   1.303e+17      5.786e+10       5.786e+10


```