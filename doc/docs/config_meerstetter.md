# Meestetter LRT1200

Accessible via Ethernet using TEC-Family controllers: 

- TEC-1089
- TEC-1090
- TEC1091
- TEC-1122
- TEC-1123

### YAML configuration example:

```YAML
- class: Ltr1200
  module: regulation.temperature.meerstetter.ltr1200
  plugin: regulation
  #host: id10mer1
  host: 192.168.0.1

  outputs:
    - name: ltr1200output
      unit: A

  inputs:
    - name: ltr1200input
      unit: Celsius    
    - name: ltr1200inputsafety
      unit: Celsius
      channel: Sink

  ctrl_loops:
    - name: ltr1200loop
      input: $ltr1200input
      output: $ltr1200output
```
