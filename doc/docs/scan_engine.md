The *Scan engine* is build around several objects which have defined roles:

* [Scan](scan_engine_scan.md) top level object
* [Scan saving](index.md#scan-saving) file management
* [Acquisition chain](scan_engine_acquisition_chain.md) defines trigger sequencing
  and master/slave dependencies
* [Acquisition objects (master/slaves)](scan_engine_acquisition_master_and_slaves.md)
  defines device trigger and data acquisition behavior
* [Preset](scan_engine_preset.md) hook to control scan environment

## Schema

![Mind](img/scan_engine.png)
