There are currently two types of files produced by BLISS:

* the HDF5 files produced by LIMA: contains all images
* the HDF5 files produced by the [Nexus writer](config_nexus_writer.md): all other data

## File permissions

The HDF5 file and parent directories are created by the Nexus writer and are therefore owned by the user under which the server process is running. Subdirectories are created by the BLISS session (e.g. directories for lima data) and are therefore owned by the user under which the BLISS session is running. Files in those subdirectories are created by the device servers (e.g. LIMA) and are therefore owned by their associated users.

## Nexus file writer

The Nexus writer holds the HDF5 file open in append mode for the duration of the scan. The HDF5 file is [locked](https://support.hdfgroup.org/HDF5/docNewFeatures/SWMR/Design-HDF5-FileLocking.pdf) which means that

 * The HDF5 file cannot be accessed during the scan unless you [bypass](#concurrent-reading) the file lock.

 * If the HDF5 file is opened and locked by other software, new data cannot be written to this file which will prevent scans from starting: you will get a "file locked" exception in BLISS.

Flushing is done regularly so [readers](#concurrent-reading) can see the latest changes. Data from scans running in parallel and multi-top-master scans will writting concurrently.

## LIMA file writer

TODO

## Concurrent reading

A full description on how to access HDF5 files while they are being modified by a writer can be found [elsewhere](https://confluence.esrf.fr/display/SWGWK/HDF5+file+format).

In summary, to read the HDF5 files during a scan they need to be opened in read-only mode while bypassing the file lock.

!!! warning
    A reader should never open the HDF5 file in append mode (which is the default in `h5py`). Even when only performing read operations, this will result in a corrupted file!

!!! warning
    A reader which locks the HDF5 file (this happens by default, even in read-only mode) will prevent the Nexus writer from accessing the file and scans in BLISS will be prevented from starting!
