## Electronic logbook
One of the [ESRF data policy services](config_data_policy.md#configure-services) is the
[*electronic logbook*](https://data.esrf.fr). BLISS uses it to log user commands,
the start of a datasets, etc. Users can also use it to send custom messages:

```
DEMO_SESSION [1]: elog_print("user message in electronic logbook ")
```

You can configure the e-logbook (persisted in Redis) of each Bliss session to send all e-logbook messages to the beamline+proposal logbook OR beamline logbook only:

```python
DEMO_SESSION [1]: elogbook.beamline_only = True  # no elogbook messages will be send to the proposal logbook
DEMO_SESSION [2]: elog_print("hide from user")
DEMO_SESSION [3]: elogbook.beamline_only = False  # the default: messages appear in beamline AND proposal logbook
DEMO_SESSION [4]: elog_print("visible by user")
```

In addition you can overwrite the default setting for each message separately

```python
DEMO_SESSION [1]: elogbook.beamline_only = False  # messages appear in beamline AND proposal logbook
DEMO_SESSION [2]: elog_print("hide from user", beamline_only=True)
```

```python
DEMO_SESSION [1]: elogbook.beamline_only = True  # no elogbook messages will be send to the proposal logbook
DEMO_SESSION [2]: elog_print("visible by user", beamline_only=False)
```

## Scan comments
When not using the ESRF data policy, comments can be added to the scan

```python
DEMO_SESSION [1]: s = loopscan(10,0.1,run=False)
DEMO_SESSION [2]: s.add_comment("This is a comment")
DEMO_SESSION [3]: s.add_comment("This is another comment")
DEMO_SESSION [4]: s.add_comment("And another one")
DEMO_SESSION [4]: s.run()
```

These comments are saved in the HDF5 file created by the [Nexus writer](config_nexus_writer.md).
