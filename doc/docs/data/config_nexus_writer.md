The Nexus writer is a TANGO device maintained by the ESRF Data Analysis Unit (DAU) to save BLISS scan data in [Nexus compliant](https://www.nexusformat.org/) HDF5 files.

## Summary

To install and use the Nexus writer

1. [Register](#tango-device-registration) a Nexus writer for each BLISS session (*test_session* in this example) with the TANGO database:

    ```bash
    RegisterNexusWriter test_session --domain id00 --instance nexuswriters
    ```
    The instance name refers to the server name. Multiple writers can be registered with the same server,
    which means they will run in the same python process.

2. [Run](#tango-server-running) the Nexus writer server:

    ```bash
    NexusWriterService nexuswriters --log=info
    ```

3. Enable the Nexus writer in the BLISS session:

    ```python
    TEST_SESSION [1]: SCAN_SAVING.writer = "nexus"
    ```
    BLISS will discover the Nexus writer automatically. The scan will stop when the writer throws an exception.

## TANGO device registration

The data writing of one BLISS session is handled by one Nexus writer TANGO device. The device has one MANDATORY property call *test_session* which must be equal to the BLISS session name. To register the device with the TANGO database you need to specify:

|                      | example        | comment                                                |
|----------------------|----------------|--------------------------------------------------------|
| server name          | NexusWriter    | you can choose but this is recommended                 |
| server instance name | nexuswriters   | you can choose (one process for each name)             |
| server class         | NexusWriter    | MANDATORY!!!                                           |
| device domain name   | id00           | you can choose but typically this is the beamline name |
| device family name   | bliss_nxwriter | you can choose but this is recommended                 |
| device member name   | test_session   | you can choose but typically this is the session name  |

Here are three ways to register this TANGO device:

1. Installation script

    ```bash
    RegisterNexusWriter test_session --domain id00 --instance nexuswriters
    ```

2. Jive

    ![Register Nexus writer](img/register_nxwriter1.png)

    In this example we registered three Nexus writers with the same server. Specify the *session* property for each Nexus writer

    ![Nexus writer properties](img/register_nxwriter2.png)

3. Beacon configuration files

    ```yaml
    server: NexusWriter
    personal_name: nexuswriters
    device:
    - tango_name: id00/bliss_nxwriter/test_session
    class: NexusWriter
    properties:
        session: test_session
    ```

## TANGO server running

A Nexus writer TANGO server (which may serve different BLISS sessions) can be started inside the BLISS conda environment as follows

```bash
NexusWriterService nexuswriters --log=info
```

You need to specify the instance name of the TANGO server, so *nexuswriters* in the example.

## TANGO device state

The state of a Nexus writer serving a BLISS session can be

 * INIT: initializing (not accepting scans)
 * ON: accepting scans (without active scan writers)
 * RUNNING: accepting scans (with active scan writers)
 * OFF: not accepting scans
 * FAULT: not accepting scans due to exception

When the writer stays in the INIT state you can try calling the TANGO device's "init" method. This can happen when the connection to beacon fails in the initialization stage. When in the OFF state, use the TANGO device's "start" method. To stop accepting new scans, use the TANGO device's "stop" method.

### Scan writing state

![Nexus writer](img/nexus_writer.png)

Each session writer launches a separate scan writer which saves the data of a particular scan (subscans are handled by the same scan writer). The scan writer state can be

 * INIT: initializing (not accepting data yet)
 * ON: accepting data
 * OFF: not accepting data (scan is done and all data has been saved)
 * FAULT: not accepting data due to exception

The final state will always be OFF (finished succesfully) or FAULT (finished unsuccesfully). The session purges the scan writers that are finished after 5 minutes. The state of those scans (which reflects whether the data has been written succesfully or not) is lost forever.

When the state is ON while the scan is finished, the writer did not received the "END_SCAN" event. You can stop the writer with the TANGO devices's "stop_scan" method. This gracefully finalizes the writing. As a last resort you can invoke the "kill_scan" method which might result in incomplete or even corrupt data (when it is executing a write operation while you kill it).
