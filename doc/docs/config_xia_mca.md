# Installation and configuration of XIA MCA

Installation of XIA devices is common to 3 XIA MCA electronics:

* Xmap
* Mercury
* FalconX

The devices are connected to a Windows (10 pro 64 bits) computer.

BLISS must be installed on that Windows PC to be able to run a BLISS RPC server.

**Handel** is the library used to deal with XIA devices and data. It comes
with **ProSpect**, the Windows software provided by XIA for testing and
calibrating XIA devices.

BLISS running on a linux station needs a BLISS rpc server named `bliss-handel-server`.
It runs on a windows PC and uses the Handel library to access the XIA
devices.

There are 2 versions of ProSpect:

* ProSpect for Xmap and Mercury (merged with the deprecated *xManager*)
* ProSpect for FalconX


## Windows PC installation

!!! note "Windows version must be 10 pro 64 bits"
    Windows 7 is now deprecated.

### Installation of conda

* Download a conda installer:
    * [miniconda](https://repo.anaconda.com/miniconda/Miniconda3-latest-Windows-x86_64.exe)
* Miniconda Installer:
    * `Next`  /  `I agree` / tick `All users`  / `Next`
    * "Destination Folder:" `C:\ProgramData\Miniconda3` / `Next`
    * Advanced Options:
        - tick `Register`
        - untick `Add miniconda3 to PATH`
    * untick "Anaconda Individual Edition Tutorial" +  "Getting Started with Anaconda" / `Finish`
* Create a link from taskbar to anaconda powershell:
    * click on Windows Start Menu / type `anaconda` / Right click on `Anaconda Powershell Prompt`  ( not `Anaconda Prompt`) /  `Pin to taskbar`
* Create the `bliss` conda environment with python 3.7 and git:
    * Start anaconda powershell
    * `conda create -n bliss -y python=3.7 git pip gitpython`
* Activate bliss environment:
    * `conda activate bliss`
* Configue channels:

```bash
conda config --env --add channels conda-forge
conda config --env --append channels esrf-bcu
```

### Installation of BLISS

* Clone and install BLISS

```shell
git clone https://gitlab.esrf.fr/bliss/bliss bliss.git

cd bliss.git

git checkout 1.9.x

conda install -y --file requirements-win64.txt

pip install -e . --no-deps
```

!!!note
    As of March 22, 2022, Bliss `1.9.x` branch points to commit #`1391b98bf`.
        Python version is `3.7.12`.
        These are the versions used by ESRF detector pool's computers.

* test bliss installation:

```
C:\ python
>>> import bliss
>>>
```


### Installation of XIA software

* Copy into `c:\blissadm\` files an directories found in:

```
P:\\ISDD\SOFTWARE\Public\distrib\XIA\
```

!!!note "March 2022 versions."

    * *ProSpect* for Falconx
        * version 1.1.62
    * *ProSpect* for Mercury and Xmap
        * version 1.1.66
    * *Handel-all* library for Mercury/Xmap devices
        * version 1.2.28 64 bits
    * *Handel-sitoro* library for Falconx
        * version 1.1.22 64 bits


* Depending on the XIA device type, **FalconX** or **Xmap/Mercury**:
    * Install the corresponding *ProSpect*
    * Test connection to the device with *ProSpect*

!!!tip
    Avoid to rename Prospect for Xmap/Mercury installation directory.
    There is a link to it hard coded in config files. An update of the name means an update of all config files.

* If available, install configuration file(s) in: `c:\blissadm\falconx\config\id00\` or in `c:\blissadm\xmap\config\id00\`

!!! note "For FalconX"
    * Connection to the falconX can be tested with a browser using address `http://192.168.200.201`
    * Check firware version (nov. 2021: `21.2.0`)




### Server Startup script

!!!note
    Examples are given for FalconX. Xmap/Mercury behaviour is similar.

!!!important
    To ensure a peaceful coexistence between conda and powershell, the shortcut
    `falconx/falconx-start IDXX` should point to the startup script
    `falconx/falconx-server_IDXX.ps1`. They need to be customized.


* Duplicate startup script (let it in falconx/ directory) and rename it:
    - `falconx\falconx-server_IDXX.ps1` -> `falconx\falconx-server_ID00.ps1`

* Customize it according to Beamline configuration (BEACON_HOST / Device Name):
    - right-clisk / edit

* Copy and rename the shortcut `falconx\falconx-start IDXX` on the desktop:
    - drag'n drop with right mouse button OR `ctrl-c` `ctrl-v`
    - rename the desktop shortcut `falconx-start ID00` (right click / rename)

* Customize the desktop shortcut:
    - 'right-click' / 'properties' ;  Change end of "Traget" line: `IDXX` -> `ID00`



!!!note "Developer's details"
    The `bliss-handel-server` start-up script is created at installation time using
    the **entry_points** definitions in `setup.py` of the BLISS repository.
    ```python
    entry_points={
          "console_scripts": [
              ...
          "bliss-handel-server = bliss.controllers.mca.handel.server:main",
          ] }
    ```
    The wrapping of the Handel library is made with cffi.
    see: `bliss/controllers/mca/handel/_cffi.py`



## BLISS Configuration in Beacon

Example for mercury:
```yaml
- name: mercury
  module: mca
  plugin: bliss
  class: Mercury
  url: tcp://wfameid42xia:8000
  configuration_directory: C:\\blissadm\\mercury\\config\\BM16
  default_configuration: Vortex3_Mercury4ch_05us_Hg.ini
```

Example for FalconX:
```yaml
- name: fxid42
  module: mca
  plugin: bliss
  class: FalconX
  url: tcp://wid421:8000
  configuration_directory: C:\\blissadm\\falconx\\config\\ID42
  default_configuration: falconxn.ini
```

Example for Xmap:
```yaml
- name: fxid16
  module: mca
  plugin: bliss
  class: XMAP
  url: tcp://wid421:8000
  configuration_directory: C:\\blissadm\\xmap\\config\\ID42
  default_configuration: xmap.ini

```
