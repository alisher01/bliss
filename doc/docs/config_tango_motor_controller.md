# Motor Controller Tango Device Server #

A server to expose a bliss motor controller over Tango. This can be used to share controllers that would normally handle a unique connection (e.g. PI_E753 controllers).

## Launching the server process

```
(base) user@beamline:~/bliss$ conda activate bliss
(bliss) user@beamline:~/bliss$ export TANGO_HOST=localhost:20000

(bliss) user@beamline:~/bliss$ Motor -?
usage :  Motor instance_name [-v[trace level]] [-nodb [-dlist <device name list>]]
Instance name defined in database for server Motor :
        test

(bliss) user@beamline:~/bliss$ Motor test
Ready to accept request
```


### Server Configuration (with Beacon)

In a *.yml* file declare the motor controllers that should be served.

```yaml
device:
  - class: MotorControllerDevice  # one tango device class
    tango_name: id00/tango/remo_ctrl   # cf DeviceProxy( tango_name )
    properties:
      beacon_name: remo_ctrl # name of the motor controller object (not the axis name)

# - [...]

personal_name: test
server: Motor
```


- **tango_name**: the name of the Tango Device in the form *domain/family/member*
- **beacon_name**: the name of the motor controller that will be served (it must already exist in the Beacon configuration)
- **personal_name**: the name used in the command line to launch the server process


### Client Configuration (with Beacon)


In a *.yml* file declare the motor controller objects. The only difference with usual controllers is the **tango-server** section.

```yaml
- name: remo_ctrl
  class: mockup
  tango-server:
      tango_name: id00/tango/remo_ctrl
      module: motors.tango_motor_controller
      class: TangoMotorController
  axes:
    - name: remo
      steps_per_unit: 10
      velocity: 1000
      acceleration: 100
```

Now, from your Bliss session you can import the axis (and thus its controller) as usual:

``` python
BLISS [1]: config.get("remo")
  Out [1]: AXIS:
                name (R): remo
                unit (R): None
                offset (R): 0.00000
                backlash (R): 0.00000
                sign (R): 1
                steps_per_unit (R): 10.00
                tolerance (R) (to check pos. before a move): 0.0001
                motion_hooks (R): []
                limits (RW):    Low: -inf High: inf    (config Low: -inf High: inf)
                dial (RW): 0.00000
                position (RW): 0.00000
                state (R): READY (Axis is READY)
                acceleration (RW):  100.00000  (config:  100.00000)
                acctime (RW):        10.00000  (config:   10.00000)
                velocity (RW):     1000.00000  (config: 1000.00000)
                velocity_low_limit (RW):            inf  (config: inf)
                velocity_high_limit (RW):            inf  (config: inf)
           Controller: remo_ctrl (TangoMotorController)

           MOCKUP AXIS:
               this axis (remo) is a simulation axis

           ENCODER:
                None


BLISS [2]:
```

The axis and its controller can now be used the same way as in local setup.

*Note that the axis is purely local, only the controller is shared through Tango.*
*Each session using this controller will own an axis instance, but all instances are kept in sync by Redis.*
