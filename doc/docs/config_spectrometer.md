# ESRF Spectrometer

A framework for the implementation of the various spectrometers assemblies found at the ESRF.
This framework is designed for spectrometers that are made of a **set of crystal analysers** and a **2D detector**.
The *Spectrometer* class is able to compute the positions of the analysers and the detector for a given Bragg angle or energy, 
taking into account an eventual **crystal miscut** and an **adjustable focusing** in the direction of the detector.


### YML Configuration

``` yml
spectrometers:

  - name: spectro
    plugin: generic
    module: spectrometers.esrf_spectrometer
    class: Spectrometer
    surface_radius_meridional: 1000
    surface_radius_sagittal: 2000
    crystal: Si844
    dspacing: 5.543013043019761e-11
    miscut: -5.04931

    analysers:
      - name: a0
        angular_offset: 0        # ( for cylindrical referential )
        offset_on_detector: 0
        #xpos: $xa0              # ( for cartesian referential )
        #ypos: 0                 # ( for cartesian referential )
        rpos: $xa0               # ( for cylindrical referential )
        zpos: $za0
        pitch: $pita0
        yaw: $yawa0

      - name: a1
        angular_offset: -3.5
        offset_on_detector: 14
        #xpos: $xa1
        #ypos: -110.70169
        rpos: $xa1
        zpos: $za1
        pitch: $pita1
        yaw: $yawa1

      - name: a2
        angular_offset: 3.5
        offset_on_detector: -14
        #xpos: $xa2
        #ypos: 110.70169
        rpos: $xa2
        zpos: $za2
        pitch: $pita2
        yaw: $yawa2

    detector: 
      - name: det0
        xpos: $xdet
        ypos: 0
        zpos: $zdet
        pitch: $pitdet
        yaw: 0
        width: 400
        height: 200

```


### Spectrometer geometry convention: 

**S**: point of interaction between the sample and the beam (origin of spectrometer referential)

**A0**: center of the central analyser  (virtual, A0_y = A0_z = 0)

**D0**: center of the detector (D0_y = 0)

**Ai**: center of analyser i

**Di**: position on detector of the beam reflected by the analyser Ai

**+X**: colinear to S->A0 

**+Y**: colinear to incoming beam

**+Z**: X ^ Y (pointing toward sky)


