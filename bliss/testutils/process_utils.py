import psutil
import gevent
import sys
import contextlib
import subprocess
from bliss.tango.clients.utils import wait_tango_device


def eprint(*args):
    print(*args, file=sys.stderr, flush=True)


def wait_for(stream, target: bytes, timeout=None):
    """Wait for a specific bytes sequence from a stream.

    Arguments:
        stream: The stream to read
        target: The sequence to wait for
    """
    data = b""
    target = target.encode()
    while target not in data:
        char = stream.read(1)
        if not char:
            raise RuntimeError(
                "Target {!r} not found in the following stream:\n{}".format(
                    target, data.decode()
                )
            )
        data += char


@contextlib.contextmanager
def start_tango_server(*cmdline_args, **kwargs):
    device_fqdn = kwargs["device_fqdn"]
    exception = None
    for _ in range(3):
        p = subprocess.Popen(cmdline_args)
        try:
            dev_proxy = wait_tango_device(**kwargs)
        except Exception as e:
            exception = e
            wait_terminate(p)
        else:
            break
    else:
        raise RuntimeError(f"could not start {device_fqdn}") from exception

    try:
        dev_proxy.server_pid = p.pid
        yield dev_proxy
    finally:
        wait_terminate(p)


def wait_terminate(process, timeout=10):
    """
    Try to terminate a process then kill it.

    This ensure the process is terminated.

    Arguments:
        process: A process object from `subprocess` or `psutil`, or an PID int
        timeout: Timeout to way before using a kill signal

    Raises:
        gevent.Timeout: If the kill fails
    """
    if isinstance(process, int):
        try:
            name = str(process)
            process = psutil.Process(process)
        except Exception:
            # PID is already dead
            return
    else:
        name = repr(" ".join(process.args))
        if process.poll() is not None:
            eprint(f"Process {name} already terminated with code {process.returncode}")
            return
    process.terminate()
    try:
        with gevent.Timeout(timeout):
            # gevent timeout have to be used here
            # See https://github.com/gevent/gevent/issues/622
            process.wait()
    except gevent.Timeout:
        eprint(f"Process {name} doesn't finish: try to kill it...")
        process.kill()
        with gevent.Timeout(10):
            # gevent timeout have to be used here
            # See https://github.com/gevent/gevent/issues/622
            process.wait()
