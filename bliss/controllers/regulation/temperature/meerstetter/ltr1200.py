# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2022 Beamline Control Unit, ESRF
# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2022 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

"""
Meestetter LRT1200 case acessible via Ethernet using TEC-Family controllers: 
    TEC-1089, TEC-1090, TEC1091, TEC-1122, TEC-1123.

yml configuration example:
- class: Ltr1200
  module: regulation.temperature.meerstetter.ltr1200
  plugin: regulation
  #host: id10mer1
  host: 192.168.0.1

  outputs:
    - name: ltr1200output
      unit: A

  inputs:
    - name: ltr1200input
      unit: Celsius    
    - name: ltr1200inputsafety
      unit: Celsius
      channel: Sink

  ctrl_loops:
    - name: ltr1200loop
      input: $ltr1200input
      output: $ltr1200output
"""

from bliss.common.logtools import log_info, log_debug
from bliss.controllers.regulator import Controller

from bliss import global_map
from bliss.comm import tcp

import struct
from . import mecom
from bliss.common.utils import object_attribute_get, object_attribute_type_get


class ltr1200:
    """
    Low-level class which takes care of all the communication
    with the hardware with the help of other classes which
    implement MeCom protocol
    """

    def __init__(self, host=None, dev_addr=1, timeout=10, debug=False):
        self.host = host
        self.dev_addr = dev_addr
        self.timeout = timeout

        # Port is always 50000 for Meerstetter TEC controller
        self._sock = tcp.Socket(self.host, 50000, self.timeout)

        global_map.register(
            self,
            parents_list=["comms"],
            children_list=[self._sock],
            tag=f"ltr1200: {host}",
        )

        self._tec = mecom.TECFamilyProtocol(self._sock, self.dev_addr)

        log_info(self, "__init__: %s %s %d" % (host, self._sock, dev_addr))

    def exit(self):
        self._sock.close()

    def init(self):
        log_info(self, "init()")
        self.model = self._tec.getModel()
        log_debug(self, "init(): Model = %s" % (self.model))
        # TODO: see what else could add here i.e. which other
        #       operations/actions would be suitable.

    def getModel(self):
        log_info(self, "getModel()")
        self.model = self._tec.getModel()
        log_debug(self, "getModel: %s" % (self.model))
        return self.model

    def getObjectTemperature(self, instance=1):
        log_info(self, "getObjectTemperature(): instance = %d" % (instance))
        answer = self._tec._getParameter(1000, 8, instance)
        log_debug(self, "getObjectTemperature: temp = %s" % answer)
        return answer

    def getSinkTemperature(self, instance=1):
        log_info(self, "getSinkTemperature(): instance = %d" % (instance))
        answer = self._tec._getParameter(1001, 8, instance)
        log_debug(self, "getSinkTemperature: temp = %s" % answer)
        return answer

    def getTargetTemperature(self, instance=1):
        log_info(self, "getTargetTemperature(): instance = %d" % (instance))
        answer = self._tec._getParameter(1010, 8, instance)
        log_debug(self, "getTargetTemperature: temp = %s" % answer)
        return answer

    def setTargetTemperature(self, value, instance=1):
        log_info(
            self, "setTargetTemperature(): instance = %d, value = %f", instance, value
        )
        answer = self._tec._setParameter(3000, value, instance)
        log_debug(self, "setTargetTemperature: %s" % answer)  # should be ACK
        return answer

    def getOutputCurrent(self, instance=1):
        log_info(self, "getOutputCurrent(): instance = %d", instance)
        answer = self._tec._getParameter(1020, 8, instance)
        log_debug(self, "getOutputCurrent: current = %s" % answer)
        return answer

    def getOutputVoltage(self, instance=1):
        log_info(self, "getOutputVoltage(): instance = %d", instance)
        answer = self._tec._getParameter(1021, 8, instance)
        log_debug(self, "getOutputVoltage: voltage = %s" % answer)
        return answer

    def getDriverStatus(self, instance=1):
        log_info(self, "getDriverStatus(): instance = %d", instance)
        answer = self._tec._getParameter(1080, 8, instance)
        description = [
            "Init",
            "Ready",
            "Run",
            "Error",
            "Bootloader",
            "Device will Reset within 200ms",
        ]
        if answer is not None:
            answer = description[int(answer)]
        log_debug(self, "getDriverStatus: status = %s", answer)
        return answer

    # def setDriverStatus(self, value, instance=1):
    #     log_info(self, "setDriverStatus(): instance = %d", instance)
    #     answer = self._tec._setParameter(1080, value, instance)
    #     log_debug(self, "setDriverStatus: status = %s", answer)  # should be ACK
    #     return answer

    def getOutputStatus(self, instance=1):
        log_info(self, "getOutputStatus(): instance = %d", instance)
        answer = self._tec._getParameter(2010, 8, instance)
        description = [
            "Static OFF",
            "Static ON",
            "Live OFF/ON",
            "HW Enable",
        ]
        if answer is not None:
            answer = description[int(answer)]
        log_debug(self, "getOutputStatus: status = %s", answer)
        return answer

    # def setOutputStatus(self, value, instance=1):
    #     log_info(self, "setOutputStatus(): instance = %d, value = %d", instance, value)
    #     answer = self._tec._setParameter(2010, value, instance)
    #     log_debug(self, "setOutputStatus: %s" % answer)  # should be ACK
    #     return answer

    def getLiveEnable(self, instance=1):
        log_info(self, "getLiveEnable(): instance = %d", instance)
        answer = int(self._tec._getParameter(50000, 8, instance))
        description = [
            "Disable (Reset State)",
            "Enabled if Output Stage Enable is Live OFF/ON",
        ]
        if answer is not None:
            answer = description[int(answer)]
        log_debug(self, "getLiveEnable: %s" % answer)  # should be ACK
        return answer

    # def setLiveEnable(self, value, instance=1):
    #     log_info(self, "setLiveEnable(): instance = %d, value = %d", instance, value)
    #     answer = (self._tec._setParameter(50000, value, instance))
    #     log_debug(self, "setLiveEnable: %s" % answer)  # should be ACK
    #     return answer

    def ResetDevice(self):
        log_info(self, "ResetDevice()")
        self._tec.putget("RS")

    def EmergencyStop(self):
        log_info(self, "EmergencyStop()")
        self._tec.putget("ES")

    def getKp(self, instance=1):
        log_info(self, "getKp(): instance = %d" % (instance))
        answer = self._tec._getParameter(3010, 8, instance)
        log_debug(self, "getKp = %s" % answer)
        return answer

    def setKp(self, value, instance=1):
        log_info(self, "setKp(): value = %f instance = %d" % (value, instance))
        answer = self._tec._setParameter(3010, value, instance)
        log_debug(self, "getKp = %s" % answer)
        return answer

    def getKi(self, instance=1):
        log_info(self, "getKi(): instance = %d" % (instance))
        answer = self._tec._getParameter(3011, 8, instance)
        log_debug(self, "getKi = %s" % answer)
        return answer

    def setKi(self, value, instance=1):
        log_info(self, "setKi(): value = %f instance = %d" % (value, instance))
        answer = self._tec._setParameter(3011, value, instance)
        log_debug(self, "getKi = %s" % answer)
        return answer

    def getKd(self, instance=1):
        log_info(self, "getKd(): instance = %d" % (instance))
        answer = self._tec._getParameter(3012, 8, instance)
        log_debug(self, "getKd = %s" % answer)
        return answer

    def setKd(self, value, instance=1):
        log_info(self, "setKd(): value = %f instance = %d" % (value, instance))
        answer = self._tec._setParameter(3012, value, instance)
        log_debug(self, "getKd = %s" % answer)
        return answer


class Ltr1200(Controller):
    def __init__(self, config):
        super().__init__(config)

        if "host" not in config:
            raise RuntimeError("Should have host with name or IP address in config")
        host = config["host"]

        if "dev_addr" not in config:
            dev_addr = 1
        else:
            dev_addr = config["dev_addr"]

        self._Ltr1200 = ltr1200(host, dev_addr)

        Controller.__init__(self, config)

        global_map.register(self, children_list=[self._Ltr1200])

        log_info(self, "__init__: %s %d", host, dev_addr)

    # ------ init methods ------------------------

    def initialize_controller(self):
        """
        Initializes the controller (including hardware).
        """
        self._Ltr1200.init()

    def initialize_input(self, tinput):
        """
        Initializes an Input class type object

        Args:
           tinput: Input class type object
        """
        pass

    def initialize_output(self, toutput):
        """
        Initializes an Output class type object

        Args:
           toutput: Output class type object
        """
        pass

    def initialize_loop(self, tloop):
        """
        Initializes a Loop class type object

        Args:
           tloop: Loop class type object
        """
        pass

    # ------ get methods ------------------------

    def read_input(self, tinput):
        """
        Reads an Input class type object
        Raises NotImplementedError if not defined by inheriting class

        Args:
           tinput: Input class type object

        Returns:
           read value (in input unit)
        """
        log_info(self, "Controller:read_input: %s" % (tinput))
        input_channel = tinput.channel
        if input_channel == "Sink":
            return self._Ltr1200.getSinkTemperature()
        else:
            return self._Ltr1200.getObjectTemperature()

    def read_output(self, toutput):
        """
        Reads an Output class type object
        Raises NotImplementedError if not defined by inheriting class

        Args:
           toutput: Output class type object

        Returns:
           read value (in output unit)
        """
        log_info(self, "Controller:read_output: %s" % (toutput))
        output_current = self._Ltr1200.getOutputCurrent()
        output_voltage = self._Ltr1200.getOutputVoltage()
        # result = f"{output_current:.3f} A,{output_voltage:.3f} V"
        # result2 = "{}A, {}V".format(output_current,output_voltage)
        # return str(result)
        return output_current  # could also read output_voltage

    def state_input(self, tinput):
        """
        Return a string representing state of an Input object.
        Raises NotImplementedError if not defined by inheriting class

        Args:
           tinput:  Input class type object

        Returns:
           object state string. This is one of READY/RUNNING/ALARM/FAULT
        """
        log_info(self, "Controller:state_input: %s" % (tinput))
        raise NotImplementedError

    def state_output(self, toutput):
        """
        Return a string representing state of an Output object.
        Raises NotImplementedError if not defined by inheriting class

        Args:
           toutput:  Output class type object

        Returns:
           object state string. This is one of READY/RUNNING/ALARM/FAULT
        """
        log_info(self, "Controller:state_output: %s" % (toutput))
        return self._Ltr1200.getOutputStatus()

    # ------ PID methods ------------------------

    def set_kp(self, tloop, kp):
        """
        Set the PID P value
        Raises NotImplementedError if not defined by inheriting class

        Args:
           tloop: Loop class type object
           kp: the kp value
        """
        log_info(self, "Controller:set_kp: %s %s" % (tloop, kp))
        self._Ltr1200.setKp(kp)

    def get_kp(self, tloop):
        """
        Get the PID P value
        Raises NotImplementedError if not defined by inheriting class

        Args:
           tloop: Loop class type object

        Returns:
           kp value
        """
        log_info(self, "Controller:get_kp: %s" % (tloop))
        return self._Ltr1200.getKp()

    def set_ki(self, tloop, ki):
        """
        Set the PID I value
        Raises NotImplementedError if not defined by inheriting class

        Args:
           tloop: Loop class type object
           ki: the ki value
        """
        log_info(self, "Controller:set_ki: %s %s" % (tloop, ki))
        self._Ltr1200.setKi(ki)

    def get_ki(self, tloop):
        """
        Get the PID I value
        Raises NotImplementedError if not defined by inheriting class

        Args:
           tloop: Loop class type object

        Returns:
           ki value
        """
        log_info(self, "Controller:get_ki: %s" % (tloop))
        return self._Ltr1200.getKi()

    def set_kd(self, tloop, kd):
        """
        Set the PID D value
        Raises NotImplementedError if not defined by inheriting class

        Args:
           tloop: Loop class type object
           kd: the kd value
        """
        log_info(self, "Controller:set_kd: %s %s" % (tloop, kd))
        self._Ltr1200.setKd(kd)

    def get_kd(self, tloop):
        """
        Reads the PID D value
        Raises NotImplementedError if not defined by inheriting class

        Args:
           tloop: Output class type object

        Returns:
           kd value
        """
        log_info(self, "Controller:get_kd: %s" % (tloop))
        return self._Ltr1200.getKd()

    def start_regulation(self, tloop):
        """
        Starts the regulation process.
        It must NOT start the ramp, use 'start_ramp' to do so.
        Raises NotImplementedError if not defined by inheriting class

        Args:
           tloop:  Loop class type object
        """
        log_info(self, "Controller:start_regulation: %s" % (tloop))
        pass

    def stop_regulation(self, tloop):
        """
        Stops the regulation process.
        It must NOT stop the ramp, use 'stop_ramp' to do so.
        Raises NotImplementedError if not defined by inheriting class

        Args:
           tloop:  Loop class type object
        """
        log_info(self, "Controller:stop_regulation: %s" % (tloop))
        raise NotImplementedError

    # ------ setpoint methods ------------------------

    def set_setpoint(self, tloop, sp, **kwargs):
        """
        Set the current setpoint (target value).
        It must NOT start the PID process, use 'start_regulation' to do so.
        Raises NotImplementedError if not defined by inheriting class

        Args:
           tloop:  Loop class type object
           sp:     setpoint (in tloop.input unit)
           **kwargs: auxilliary arguments
        """
        log_info(self, "Controller:set_setpoint: %s %s" % (tloop, sp))
        self._Ltr1200.setTargetTemperature(sp)

    def get_setpoint(self, tloop):
        """
        Get the current setpoint (target value)
        Raises NotImplementedError if not defined by inheriting class

        Args:
           tloop:  Loop class type object

        Returns:
           (float) setpoint value (in tloop.input unit).
        """
        log_info(self, "Controller:get_setpoint: %s" % (tloop))
        return self._Ltr1200.getTargetTemperature()

    def get_working_setpoint(self, tloop):
        """
        Get the current working setpoint (setpoint along ramping)
        Raises NotImplementedError if not defined by inheriting class

        Args:
           tloop:  Loop class type object

        Returns:
           (float) working setpoint value (in tloop.input unit).
        """
        log_info(self, "Controller:get_working_setpoint: %s" % (tloop))
        return self._Ltr1200.getObjectTemperature()

    # ------ setpoint ramping methods (optional) ------------------------

    def start_ramp(self, tloop, sp, **kwargs):
        """
        Start ramping to a setpoint
        It must NOT start the PID process, use 'start_regulation' to do so.
        Raises NotImplementedError if not defined by inheriting class

        Replace 'Raises NotImplementedError' by 'pass' if the controller has ramping but doesn't have a method to explicitly starts the ramping.
        Else if this function returns 'NotImplementedError', then the Loop 'tloop' will use a SoftRamp instead.

        Args:
           tloop:  Loop class type object
           sp:       setpoint (in tloop.input unit)
           **kwargs: auxilliary arguments
        """
        log_info(self, "Controller:start_ramp: %s %s" % (tloop, sp))
        self.set_setpoint(tloop, sp)

    def stop_ramp(self, tloop):
        """
        Stop the current ramping to a setpoint
        It must NOT stop the PID process, use 'stop_regulation' to do so.
        Raises NotImplementedError if not defined by inheriting class

        Args:
           tloop:  Loop class type object
        """
        log_info(self, "Controller:stop_ramp: %s" % (tloop))
        raise NotImplementedError

    def is_ramping(self, tloop):
        """
        Get the ramping status.
        Raises NotImplementedError if not defined by inheriting class

        Args:
           tloop:  Loop class type object

        Returns:
           (bool) True if ramping, else False.
        """
        log_info(self, "Controller:is_ramping: %s" % (tloop))
        answer = self._Ltr1200.getDriverStatus()
        if answer == "Run":
            answer = True
        else:
            answer = False

        return answer

    def set_ramprate(self, tloop, rate):
        """
        Set the ramp rate
        Raises NotImplementedError if not defined by inheriting class

        Args:
           tloop:  Loop class type object
           rate:   ramp rate (in input unit per second)
        """
        log_info(self, "Controller:set_ramprate: %s %s" % (tloop, rate))
        raise NotImplementedError

    def get_ramprate(self, tloop):
        """
        Get the ramp rate
        Raises NotImplementedError if not defined by inheriting class

        Args:
           tloop:  Loop class type object

        Returns:
           ramp rate (in input unit per second)
        """
        log_info(self, "Controller:get_ramprate: %s" % (tloop))
        raise NotImplementedError

    # def set_dwell(self, tloop, dwell):
    #     """
    #     Set the dwell value (for ramp stepping mode)
    #     Raises NotImplementedError if not defined by inheriting class

    #     Args:
    #        tloop:  Loop class type object
    #        dwell
    #     """
    #     log_info(self, "Controller:set_dwell: %s %s" % (tloop, dwell))
    #     raise NotImplementedError

    # def get_dwell(self, tloop):
    #     """
    #     Get the dwell value (for ramp stepping mode)
    #     Raises NotImplementedError if not defined by inheriting class

    #     Args:
    #        tloop:  Loop class type object

    #     Returns:
    #        dwell value
    #     """
    #     log_info(self, "Controller:get_dwell: %s" % (tloop))
    #     raise NotImplementedError

    # def set_step(self, tloop, step):
    #     """
    #     Set the step value (for ramp stepping mode)
    #     Raises NotImplementedError if not defined by inheriting class

    #     Args:
    #        tloop: Loop class type object
    #        step
    #     """
    #     log_info(self, "Controller:set_step: %s %s" % (tloop, step))
    #     raise NotImplementedError

    # def get_step(self, tloop):
    #     """
    #     Get the dwell value (for ramp stepping mode)
    #     Raises NotImplementedError if not defined by inheriting class

    #     Args:
    #        tloop: Loop class type object

    #     Returns:
    #        step value
    #     """
    #     log_info(self, "Controller:get_step: %s" % (tloop))
    #     raise NotImplementedError

    # ------ raw methods (optional) ------------------------

    # def Wraw(self, string):
    #     """
    #     A string to write to the controller
    #     Raises NotImplementedError if not defined by inheriting class

    #     Args:
    #        string:  the string to write
    #     """
    #     log_info(self, "Controller:Wraw:")
    #     raise NotImplementedError

    # def Rraw(self):
    #     """
    #     Reading the controller
    #     Raises NotImplementedError if not defined by inheriting class

    #     returns:
    #        answer from the controller
    #     """
    #     log_info(self, "Controller:Rraw:")
    #     raise NotImplementedError

    # def WRraw(self, string):
    #     """
    #     Write then Reading the controller
    #     Raises NotImplementedError if not defined by inheriting class

    #     Args:
    #        string:  the string to write
    #     returns:
    #        answer from the controller
    #     """
    #     log_info(self, "Controller:WRraw:")
    #     raise NotImplementedError

    # # --- controller methods to customize the PID algo (optional) ------------------------

    # def get_sampling_frequency(self, tloop):
    #     """
    #     Get the sampling frequency (PID)
    #     Raises NotImplementedError if not defined by inheriting class

    #     Args:
    #        tloop:  Loop class type object
    #     """
    #     log_info(self, "Controller:get_sampling_frequency: %s" % (tloop))
    #     raise NotImplementedError

    # def set_sampling_frequency(self, tloop, value):
    #     """
    #     Set the sampling frequency (PID)
    #     Raises NotImplementedError if not defined by inheriting class

    #     Args:
    #        tloop: Loop class type object
    #        value: the sampling frequency [Hz]
    #     """
    #     log_info(self, "Controller:set_sampling_frequency: %s %s" % (tloop, value))
    #     raise NotImplementedError

    # def get_pid_range(self, tloop):
    #     """
    #     Get the PID range (PID output value limits)
    #     """
    #     log_info(self, "Controller:get_pid_range: %s" % (tloop))
    #     raise NotImplementedError

    # def set_pid_range(self, tloop, pid_range):
    #     """
    #     Set the PID range (PID output value limits)
    #     """
    #     log_info(self, "Controller:set_pid_range: %s %s" % (tloop, pid_range))
    #     raise NotImplementedError

    # # --- controller method to set the Output to a given value (optional) -----------

    # def set_output_value(self, toutput, value):
    #     """
    #     Set the value on the Output device.
    #     Raises NotImplementedError if not defined by inheriting class

    #     Args:
    #        toutput: Output class type object
    #        value: value for the output device (in output unit)
    #     """
    #     log_info(self, "Controller:set_output_value: %s %s" % (toutput, value))
    #     raise NotImplementedError

    # # --- controller methods to handle the ramping on the Output (optional) -----------

    # def start_output_ramp(self, toutput, value, **kwargs):
    #     """
    #     Start ramping on the output
    #     Raises NotImplementedError if not defined by inheriting class

    #     Replace 'Raises NotImplementedError' by 'pass' if the controller has output ramping but doesn't have a method to explicitly starts the output ramping.
    #     Else if this function returns 'NotImplementedError', then the output 'toutput' will use a SoftRamp instead.

    #     Args:
    #        toutput: Output class type object
    #        value:   Target value for the output (in output unit)
    #        **kwargs: auxilliary arguments
    #     """
    #     log_info(self, "Controller:start_output_ramp: %s %s" % (toutput, value))
    #     raise NotImplementedError

    # def stop_output_ramp(self, toutput):
    #     """
    #     Stop the current ramping on the output
    #     Raises NotImplementedError if not defined by inheriting class

    #     Args:
    #        toutput: Output class type object
    #     """
    #     log_info(self, "Controller:stop_output_ramp: %s" % (toutput))
    #     raise NotImplementedError

    # def is_output_ramping(self, toutput):
    #     """
    #     Get the output ramping status.
    #     Raises NotImplementedError if not defined by inheriting class

    #     Args:
    #        toutput:  Output class type object

    #     Returns:
    #        (bool) True if ramping, else False.
    #     """
    #     log_info(self, "Controller:is_output_ramping: %s" % (toutput))
    #     raise NotImplementedError

    # def set_output_ramprate(self, toutput, rate):
    #     """
    #     Set the output ramp rate
    #     Raises NotImplementedError if not defined by inheriting class

    #     Args:
    #        toutput: Output class type object
    #        rate:     ramp rate (in output unit per second)
    #     """
    #     log_info(self, "Controller:set_output_ramprate: %s %s" % (toutput, rate))
    #     raise NotImplementedError

    # def get_output_ramprate(self, toutput):
    #     """
    #     Get the output ramp rate
    #     Raises NotImplementedError if not defined by inheriting class

    #     Args:
    #        toutput: Output class type object

    #     Returns:
    #        ramp rate (in output unit per second)
    #     """
    #     log_info(self, "Controller:get_output_ramprate: %s" % (toutput))
    #     raise NotImplementedError
