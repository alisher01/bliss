# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2022 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import time
import numpy
import weakref
import enum

from bliss.common import event
from bliss.common.axis import Axis, AxisState
from bliss.common.soft_axis import SoftAxis
from bliss.common.protocols import counter_namespace
from bliss.common.utils import autocomplete_property
from bliss.common.plot import get_flint

from bliss.physics.units import ur
from bliss.physics.diffraction import CrystalPlane, MultiPlane
from bliss.controllers.motor import CalcController
from bliss.controllers.bliss_controller import BlissController
from bliss.config.settings import HashObjSetting
from bliss.common.protocols import HasMetadataForScan
from bliss.shell.formatters.table import IncrementalTable
from bliss.shell.standard import umv
from bliss import global_map

REF_MODE_ENUM = enum.IntEnum("ReferentialMode", "CARTESIAN CYLINDRICAL")
FLOAT_PRECISION = ".4"  # to print info
FLOAT_FORMAT = "f"  # to print info
TOLERANCE = 1e-4  # tolerance for the positioning

# --------------- Note about array and matrix algebra ---------------------
#
# with a vector:
# V = | 1
#     | 0
#     | 0
# expressed as: V = numpy.array( [1, 0, 0] )
#
# with a matrix:
# P = |  0  1  0 |
#     | -1  0  0 |
#     |  0  0  1 |
# expressed as: P = numpy. array( [ [0, 1, 0], [ -1, 0, 0], [0, 0, 1] ] )
#
#
# then VV = P@V  is the matrix product:
#
# | 0 = |  0  1  0 | * | 1
# |-1   | -1  0  0 |   | 0
# | 0   |  0  0  1 |   | 0
#
# with VV = numpy.array( [0, -1, 0] )
#
# --------------------------------------------------------------------------


def rotation_matrix(axis, angle):
    """
    Return the rotation matrix associated with counterclockwise rotation about
    the given axis by a given angle (degree).
    """
    theta = numpy.deg2rad(angle)
    axis = axis / getnorm(axis)
    a = numpy.cos(theta / 2.0)
    b, c, d = -axis * numpy.sin(theta / 2.0)
    aa, bb, cc, dd = a * a, b * b, c * c, d * d
    bc, ad, ac, ab, bd, cd = b * c, a * d, a * c, a * b, b * d, c * d
    return numpy.array(
        [
            [aa + bb - cc - dd, 2 * (bc + ad), 2 * (bd - ac)],
            [2 * (bc - ad), aa + cc - bb - dd, 2 * (cd + ab)],
            [2 * (bd + ac), 2 * (cd - ab), aa + dd - bb - cc],
        ]
    )


def getnorm(vector):
    return numpy.sqrt(numpy.dot(vector, vector))


def normalize(vector):
    n = getnorm(vector)
    if n == 0:
        return numpy.array([0, 0, 0])
    else:
        return vector / n


def get_normal_plane(vector, w, h, center):
    w, h = w / 2, h / 2
    ex = normalize(vector)
    ez = numpy.array([0, 0, 1])
    ey = numpy.cross(ez, ex)
    ez = numpy.cross(ex, ey)

    p0 = ez * h - ey * w
    p1 = ez * h + ey * w
    p2 = -p0
    p3 = -p1

    return [p0 + center, p1 + center, p2 + center, p3 + center]


def get_angle(v1, v2):
    n1 = getnorm(v1)
    n2 = getnorm(v2)
    return numpy.rad2deg(numpy.arccos(numpy.dot(v1, v2) / (n1 * n2)))


def transpose(data2d):
    return list(zip(*data2d))


def direction_to_angles(direction):
    x, y, z = direction
    r = getnorm(direction)
    if r != 0:
        pitch = -numpy.rad2deg(numpy.arcsin(z / r))
        yaw = numpy.rad2deg(numpy.arctan2(y, x))
    else:
        pitch = yaw = 0

    return pitch, yaw


def bragg_from_vect(incident, normal, miscut):
    beta = numpy.rad2deg(numpy.arccos(numpy.dot(incident, normal)))
    if normal[2] >= incident[2]:
        sign = 1
    else:
        sign = -1
    beta = beta * sign
    return 90 - beta + miscut


def analyser_to_spectro_matrix(analyser_position, refmode):
    """Compute the matrix to switch from an anlayser base to the spectrometer base.

    It assumes that the x axis of the analyser base is pointing to spectrometer base origin (AS).
    """
    # === analyser_position in spectro ref

    if refmode == REF_MODE_ENUM.CYLINDRICAL:
        incident = normalize(analyser_position) * -1
        ex = incident  # x axis of analyser base is along incident beam (i.e AS)
    else:
        ex = [-1, 0, 0]

    ez = numpy.array([0, 0, 1])  # analyser yaw around lab-Z
    ey = numpy.cross(ez, ex)
    ez = numpy.cross(ex, ey)  # analyser yaw around ex^ey = local ez
    p = numpy.vstack((ex, ey, ez)).T  # analyser to spectro matrix
    return p


def spectro_to_analyser_matrix(analyser_position, refmode):
    """Compute the matrix to switch from the spectrometer base to an anlayser base.

    It assumes that the x axis of the analyser base is pointing to spectrometer base origin (AS).
    """
    return numpy.linalg.inv(analyser_to_spectro_matrix(analyser_position, refmode))


class FixedAxis:
    def __init__(self, name, pos):
        self.name = name
        self._position = pos

    def position(self):
        return self._position

    def move(self, pos):
        if abs(pos - self._position) > TOLERANCE:
            print(f"Warning: FixedAxis cannot move from {self._position} to {pos}")

    def stop(self):
        pass

    def state(self):
        return AxisState("READY")


class Positioner3D:
    """Manage position (x, y, z) and direction (pitch, yaw) of an object in 3D"""

    def __init__(self, cfg):
        self._config = cfg
        self._name = cfg["name"]

    def __info__(self):
        txt = " axes:\n"
        for tag in ["xpos", "ypos", "zpos", "pitch", "yaw"]:
            txt += f"  - {tag:10s}: {str(self._get_pos(tag)):10s}\n"

        return txt

    def _get_pos(self, tag):
        raise NotImplementedError

    def _set_pos(self, tag, value):
        raise NotImplementedError

    @property
    def name(self):
        return self._name

    @property
    def config(self):
        return self._config

    @property
    def rpos(self):
        return self._get_pos("rpos")

    @rpos.setter
    def rpos(self, value):
        self._set_pos("rpos", value)

    @property
    def xpos(self):
        return self._get_pos("xpos")

    @xpos.setter
    def xpos(self, value):
        self._set_pos("xpos", value)

    @property
    def ypos(self):
        return self._get_pos("ypos")

    @ypos.setter
    def ypos(self, value):
        self._set_pos("ypos", value)

    @property
    def zpos(self):
        return self._get_pos("zpos")

    @zpos.setter
    def zpos(self, value):
        self._set_pos("zpos", value)

    @property
    def pitch(self):
        return self._get_pos("pitch")

    @pitch.setter
    def pitch(self, value):
        self._set_pos("pitch", value)

    @property
    def yaw(self):
        return self._get_pos("yaw")

    @yaw.setter
    def yaw(self, value):
        self._set_pos("yaw", value)

    @property
    def position(self):
        """Return the position (x, y, z) as a numpy array"""
        return numpy.array([self.xpos, self.ypos, self.zpos])

    @property
    def direction(self):
        """Compute the direction vector based on the current pitch and yaw angles (normalized).

        If pitch = yaw = 0 the direction vector is colinear to the x axis of this object referential.
        """

        # start with a default direction along ex and apply pitch and yaw rotations
        v = numpy.array([1, 0, 0])

        pit = numpy.deg2rad(self.pitch)
        yaw = numpy.deg2rad(self.yaw)

        cp = numpy.cos(pit)
        sp = numpy.sin(pit)
        cy = numpy.cos(yaw)
        sy = numpy.sin(yaw)

        # Rx = numpy. array([[1, 0, 0], [ 0, cr, -sr], [0, sr, cr]]) # rot mat around ex (roll)
        Ry = numpy.array(
            [[cp, 0, sp], [0, 1, 0], [-sp, 0, cp]]
        )  # rot mat around ey (pitch)
        Rz = numpy.array(
            [[cy, -sy, 0], [sy, cy, 0], [0, 0, 1]]
        )  # rot mat around ez (yaw)

        v = Rz @ Ry @ v  # Rz @ Ry @ Rx @ v  # apply rotations in the order:  Z->Y->X

        return v


class Analyser(Positioner3D):
    def __init__(self, cfg, spectro):
        super().__init__(cfg)
        self._spectro = spectro
        self._load_config()

        # create bragg calc mot
        axes_cfg = []
        for tag, axis in self.axes.items():
            axes_cfg.append({"name": axis, "tags": f"real {self.name}_{tag}"})
        axes_cfg.append({"name": f"{self.name}_bragg", "tags": "bragg"})
        self._calc_mot = AnalyserBraggCalcController(self, {"axes": axes_cfg})
        self._calc_mot._initialize_config()

        # create energy calc mot
        axes_cfg = []
        bragg_axis = self._calc_mot._tagged["bragg"][0]
        axes_cfg.append({"name": bragg_axis, "tags": "real bragg"})
        axes_cfg.append({"name": f"{self.name}_energy", "tags": "energy"})
        self._ene_calc = SpectroEnergyCalcController(self._spectro, {"axes": axes_cfg})
        self._ene_calc._initialize_config()

    def __close__(self):
        # close calc controller
        self._calc_mot.close()
        self._ene_calc.close()

    def _load_config(self):

        self._offset_on_detector = self.config["offset_on_detector"]

        axes_tags = []

        # check case (r, alpha) or (x, yoff)
        if all(i in self.config.keys() for i in ["rpos", "xpos"]):
            txt = "Spectro config error: cannot use 'rpos' and 'xpos' at the same time, select one.\n"
            txt += "\nCylindrical case:\n"
            txt += "  - 'rpos': analyser radial axis (distance to center) in the (X,Y) plan\n"
            txt += "  - 'angular_offset': analyser angular offset toward X in the (X,Y) plan\n"
            txt += "\nCartesian case:\n"
            txt += "  - 'xpos': analyser X axis (x coordinate in spectrometer referential)\n"
            txt += "  - 'ypos': analyser Y axis or position (y coordinate in spectrometer referential)\n"
            raise ValueError(txt)

        if "rpos" in self.config.keys():
            self._referential_mode = REF_MODE_ENUM.CYLINDRICAL
            self._angular_offset = self.config["angular_offset"]
            axes_tags = ["rpos", "zpos", "pitch", "yaw"]

        elif "xpos" in self.config.keys():
            self._referential_mode = REF_MODE_ENUM.CARTESIAN
            self._angular_offset = None
            axes_tags = ["xpos", "ypos", "zpos", "pitch", "yaw"]

        self._axes = {}
        self._virtual_axes = []
        for tag in axes_tags:
            axis = self.config[tag]

            if not isinstance(axis, Axis):
                name = f"{self.name}_{tag}"
                pos = float(axis)
                vaxis = FixedAxis(name, pos)
                axis = SoftAxis(
                    name,
                    vaxis,
                    position="position",
                    move="move",
                    stop="stop",
                    state="state",
                    low_limit=float("-inf"),
                    high_limit=float("+inf"),
                    tolerance=TOLERANCE,
                    export_to_session=False,
                )
                self._virtual_axes.append(axis)

            self._axes[tag] = axis

    def __info__(self):
        head, pos = ["Analyser"], [self.name]
        h, p = zip(
            *[
                (tag, self._get_pos(tag))
                for tag in ["rpos", "xpos", "ypos", "zpos", "pitch", "yaw"]
            ]
        )
        head.extend(h)
        head.extend(["bragg", "ang_off", "det_off"])
        pos.extend(p)
        pos.extend([self.geo_bragg, self.angular_offset, self.offset_on_detector])
        tab = IncrementalTable([head, pos], fpreci=FLOAT_PRECISION, dtype=FLOAT_FORMAT)

        tab.resize(minwidth=12, maxwidth=100)
        tab.add_separator(sep="-", line_index=1)
        return "\n".join(["", str(tab)])

    def _get_pos(self, tag):
        # === !!! motor position are expressed in lab ref !!!
        if self._referential_mode == REF_MODE_ENUM.CYLINDRICAL:
            if tag == "xpos":
                ang = numpy.deg2rad(self.angular_offset)
                return self._axes["rpos"].position * numpy.cos(ang)
            elif tag == "ypos":
                ang = numpy.deg2rad(self.angular_offset)
                return self._axes["rpos"].position * numpy.sin(ang)

        if self._referential_mode == REF_MODE_ENUM.CARTESIAN:
            if tag == "rpos":
                x = self._axes["xpos"].position
                y = self._axes["ypos"].position
                return numpy.sqrt(x**2 + y**2)

        return self._axes[tag].position

    def _set_pos(self, tag, value):
        # === !!! motor position are expressed in lab ref !!!
        if self._referential_mode == REF_MODE_ENUM.CYLINDRICAL:
            if tag in ["xpos", "ypos"]:
                print(f"cannot set {tag} in cylindrical mode")
                return

        if self._referential_mode == REF_MODE_ENUM.CARTESIAN:
            if tag == "rpos":
                print(f"cannot set {tag} in cartesian mode")
                return

        self._axes[tag].move(value)

    @property
    def axes(self):
        return self._axes

    @property
    def angular_offset_spectro_ref(self):
        sx, sy, sz = self._spectro.sample_position
        if (sx, sy, sz) == (0, 0, 0):
            return self.angular_offset
        else:
            raise NotImplementedError

    @property
    def angular_offset(self):
        # === !!! in lab ref !!!
        if self._referential_mode == REF_MODE_ENUM.CARTESIAN:
            return numpy.rad2deg(numpy.arctan2(self.ypos, self.xpos))
        else:
            return self._angular_offset

    @angular_offset.setter
    def angular_offset(self, value):
        # === !!! in lab ref !!!
        if self._referential_mode == REF_MODE_ENUM.CARTESIAN:
            print("cannot set angular offset in cartesian mode")
        else:
            self._angular_offset = value
            self._spectro._update()

    @property
    def offset_on_detector(self):
        return self._offset_on_detector

    @offset_on_detector.setter
    def offset_on_detector(self, value):
        self._offset_on_detector = value
        self._spectro._update()

    @property
    def incident(self):
        """Compute the normalized direction of the incident ray (from analyser to sample !)"""
        return normalize(self.position - self._spectro.sample_position) * -1

    @property
    def normal(self):
        """Compute the normal vector of the analyser (normalized).

        The normal is the vector perpendicular to the analyser surface at its center and
        expressed in the spectrometer referential.
        """
        return (
            analyser_to_spectro_matrix(
                self.position - self._spectro.sample_position,
                refmode=self._referential_mode,
            )
            @ self.direction
        )

    @property
    def reflected(self):
        """Compute the direction of the reflected ray toward surface normal (normalized).

        Takes into account the possible miscut between surface and crystal planes.
        """
        # rotation of 2*(beta-miscut) in meridional plane
        beta = numpy.rad2deg(numpy.arccos(numpy.dot(self.incident, self.normal)))
        if self.normal[2] >= self.incident[2]:
            sign = 1
        else:
            sign = -1

        beta = beta * sign
        ang = 2 * (beta - self._spectro.miscut)
        return numpy.dot(rotation_matrix(self.meridional * sign, ang), self.incident)

    @property
    def meridional(self):
        """Compute the normal to the meridional plan (normalized)."""
        return normalize(numpy.cross(self.incident, self.normal))

    @property
    def sagittal(self):
        """Compute the normal to the sagittal plan (normalized)."""
        return normalize(numpy.cross(self.normal, self.meridional))

    @property
    def geo_bragg(self):
        return bragg_from_vect(self.incident, self.normal, self._spectro.miscut)

    @autocomplete_property
    def bragg_axis(self):
        return self._calc_mot._tagged["bragg"][0]

    @autocomplete_property
    def energy_axis(self):
        return self._ene_calc._tagged["energy"][0]

    def _get_park_args(self):
        pos_dict = self.config.get("parking")
        if pos_dict is None:
            print("No parking positions defined in config!")
            return

        args = []

        for tag, pkpos in pos_dict.items():

            ax = self.axes.get(tag)
            if ax is None:
                raise RuntimeError(f"cannot find axis tag {tag}")

            if ax not in self._virtual_axes:
                args.append(ax)
                args.append(pkpos)

        return args

    def park(self):
        args = self._get_park_args()
        if len(args):
            umv(*args)

    def set_energy(self, energy, interactive=True):
        if interactive:
            print(
                "!!! Warning this will modify the offset of all real axes of the analyser !!!\n"
            )
            print("Type 'y' to confirm or anything else to abort: ")
            x = input()
            if x != "y":
                return

        bragg = self._spectro.energy2bragg(energy)
        self.set_bragg(bragg, interactive=False)

    def set_bragg(self, bragg, interactive=True):
        if interactive:
            print(
                "!!! Warning this will modify the offset of all real axes of the analyser !!!\n"
            )
            print("Type 'y' to confirm or anything else to abort: ")
            x = input()
            if x != "y":
                return

        reals = {}
        for k, v in self._spectro.compute_bragg_solution(bragg)[2].items():
            name, tag = k.split("_")
            if name == self.name:
                reals[tag] = v

        for tag, theo_pos in reals.items():
            self.axes[tag].position = theo_pos


class Detector(Positioner3D):
    def __init__(self, cfg, spectro):
        super().__init__(cfg)
        self._spectro = spectro
        self._load_config()

        # create bragg calc mot
        axes_cfg = []
        for tag, axis in self.axes.items():
            axes_cfg.append({"name": axis, "tags": f"real {self.name}_{tag}"})
        axes_cfg.append({"name": f"{self.name}_bragg", "tags": "bragg"})
        self._calc_mot = DetectorBraggCalcController(self, {"axes": axes_cfg})
        self._calc_mot._initialize_config()

        # create energy calc mot
        axes_cfg = []
        bragg_axis = self._calc_mot._tagged["bragg"][0]
        axes_cfg.append({"name": bragg_axis, "tags": "real bragg"})
        axes_cfg.append({"name": f"{self.name}_energy", "tags": "energy"})
        self._ene_calc = SpectroEnergyCalcController(self._spectro, {"axes": axes_cfg})
        self._ene_calc._initialize_config()

    def __close__(self):
        # close calc controller
        self._calc_mot.close()
        self._ene_calc.close()

    def _load_config(self):
        self._axes = {}
        self._virtual_axes = []
        for tag in ["xpos", "ypos", "zpos", "pitch", "yaw"]:
            axis = self.config[tag]
            if not isinstance(axis, Axis):
                name = f"{self.name}_{tag}"
                pos = float(axis)
                vaxis = FixedAxis(name, pos)
                axis = SoftAxis(
                    name,
                    vaxis,
                    position="position",
                    move="move",
                    stop="stop",
                    state="state",
                    low_limit=float("-inf"),
                    high_limit=float("+inf"),
                    tolerance=TOLERANCE,
                    export_to_session=False,
                )
                self._virtual_axes.append(axis)

            self._axes[tag] = axis

    def __info__(self):
        head, pos = ["Detector"], [self.name]
        h, p = zip(
            *[
                (tag, self._get_pos(tag))
                for tag in ["rpos", "xpos", "ypos", "zpos", "pitch", "yaw"]
            ]
        )
        head.extend(h)
        pos.extend(p)
        tab = IncrementalTable([head, pos], fpreci=FLOAT_PRECISION, dtype=FLOAT_FORMAT)
        tab.resize(minwidth=12, maxwidth=100)
        tab.add_separator(sep="-", line_index=1)
        return "\n".join(["", str(tab)])

    def _get_pos(self, tag):
        # === !!! motor position are expressed in lab ref !!!
        if tag == "rpos":
            x = self._axes["xpos"].position
            y = self._axes["ypos"].position
            return numpy.sqrt(x**2 + y**2)

        return self._axes[tag].position

    def _set_pos(self, tag, value):
        self._axes[tag].move(value)

    @property
    def axes(self):
        return self._axes

    @autocomplete_property
    def bragg_axis(self):
        return self._calc_mot._tagged["bragg"][0]

    @autocomplete_property
    def energy_axis(self):
        return self._ene_calc._tagged["energy"][0]

    def set_energy(self, energy, interactive=True):
        if interactive:
            print(
                "!!! Warning this will modify the offset of all real axes of the detector !!!\n"
            )
            print("Type 'y' to confirm or anything else to abort: ")
            x = input()
            if x != "y":
                return

        bragg = self._spectro.energy2bragg(energy)
        self.set_bragg(bragg, interactive=False)

    def set_bragg(self, bragg, interactive=True):
        if interactive:
            print(
                "!!! Warning this will modify the offset of all real axes of the detector !!!\n"
            )
            print("Type 'y' to confirm or anything else to abort: ")
            x = input()
            if x != "y":
                return

        reals = {}
        for k, v in self._spectro.compute_bragg_solution(bragg)[2].items():
            name, tag = k.split("_")
            if name == self.name:
                reals[tag] = v

        for tag, theo_pos in reals.items():
            self.axes[tag].position = theo_pos


class Spectrometer(BlissController, HasMetadataForScan):
    """Spectrometer geometry convention:

    Points:
      S: point of interaction between the sample and the beam
      A0: center of the central analyser  (virtual, A0_y = A0_z = 0)
      D0: center of the detector (D0_y = 0)
      Ai: center of analyser i
      Di: position on detector of the beam reflected by the analyser Ai

    Vectors:
      SA: Sample to Analyser
      SD: Sample to Detector
      NA: normal to analyser surface on point A (optical)
      NAc: normal to analyser crystal plane on point A (diffraction)

    Planes:
      Meridional plane (MeP): defined by the 3 points < S, Ai, Di >
      Scattering plane (ScP): contains SA and normal to MeP
      Sagittal   plane (SgP): contains NA and normal to MeP

    Spectrometer referentials:
      Origin: centred on S
      +X: SA0 / norm(SA0) (perpendicular to incoming beam and contained in the equatorial plane)
      +Y: colinear to incoming beam
      +Z: X ^ Y (pointing toward sky)

    Laboratory to Spectrometer referential:
      sample_position: position of the sample (S) in the laboratory referential
      !!! real motors positions are expressed in the laboratory referential !!!

    """

    def __init__(self, config):
        super().__init__(config)

        self._settings = HashObjSetting(f"{self.name}_settings")
        self._load_settings()

        self._align_info = ""
        self._align_tolerance = self.config.get("tolerance", TOLERANCE)

        self._plot = None
        self._plot_axes_connected = False
        self._plot_sagital_circles = False
        global_map.register(self, parents_list=["controllers"])

    def __del__(self):
        self.__close__()

    def __close__(self):
        # close spectro calc
        self._calc_mot.close()
        self._ene_calc.close()

        # close analysers
        for ana in self.analysers:
            ana.__close__()

        # close detector
        self.detector.__close__()

        # disconnect axes from plot
        self._disconnect_axes()

    def __info__(self):

        aligned, bragg = self._check_alignement()

        if aligned:
            aligned_txt = f"*** ALIGNED @bragg {bragg:.4f} degree ({self.bragg2energy(bragg):.4f}kev)***"
        else:
            aligned_txt = "!!! NOT ALIGNED !!!"

        txt = f"\n=== Spectrometer: {self.name} {aligned_txt} ===\n\n"

        if self._align_info:
            txt += f"{self._align_info})\n\n"

        txt += f" crystal: {self.crystal}\n"
        txt += f" miscut: {self.miscut}\n"
        txt += f" meridional curvature: {self.surface_radius_meridional}\n"
        txt += "\n"

        head = ""
        for ana in self._active_analysers:
            info = ana.__info__()
            lines = info.split("\n")
            if lines[1] != head:
                txt += info
                head = lines[1]
            else:
                txt += "\n".join(lines[3:])

            txt += "\n"

        txt += self.detector.__info__()
        txt += "\n"

        return txt

    def _get_subitem_default_class_name(self, cfg, parent_key):
        # Called when the class key cannot be found in the item_config.
        # Then a default class must be returned. The choice of the item_class is usually made from the parent_key value.
        # Elements of the item_config may also by used to make the choice of the item_class.

        """
        Return the appropriate default class name (as a string) for a given item.

        Args:
            cfg: item config node
            parent_key: the key under which item config was found
        """

        if parent_key == "analysers":
            return "Analyser"

        elif parent_key == "detector":
            return "Detector"

        else:
            raise NotImplementedError

    def _create_subitem_from_config(
        self, name, cfg, parent_key, item_class, item_obj=None
    ):
        # Called when a new subitem is created (i.e accessed for the first time via self._get_subitem)
        """
        Return the instance of a new item owned by this container.

        Args:
            name: item name
            cfg : item config
            parent_key: the config key under which the item was found (ex: 'counters').
            item_class: a class to instantiate the item (None if item is a reference)
            item_obj: the item instance (None if item is NOT a reference)

        Return: item instance

        """

        if item_obj is not None:
            return item_obj

        if parent_key == "analysers":
            return item_class(cfg, self)

        elif parent_key == "detector":
            return item_class(cfg, self)

        else:
            raise NotImplementedError

    def _load_config(self):
        # Called by the plugin via self._initialize_config
        # Called after self._subitems_config has_been filled.

        """
        Read and apply the YML configuration of this container.
        """

        self._miscut = self.config["miscut"]
        self._surface_radius_meridional = self.config["surface_radius_meridional"]

        self._analysers = []
        for name, (cfg, pkey) in self._subitems_config.items():
            if pkey == "analysers":
                ana = self._get_subitem(name)
                self._analysers.append(ana)
            elif pkey == "detector":
                self._detector = self._get_subitem(name)

        # create bragg calc mot (bragg to {bragg_i})
        axes_cfg = []
        for ana in self._analysers:
            axis = ana._calc_mot._tagged["bragg"][0]
            axes_cfg.append({"name": axis, "tags": f"real {ana.name}_bragg"})
        axis = self._detector._calc_mot._tagged["bragg"][0]
        axes_cfg.append({"name": axis, "tags": f"real {self.detector.name}_bragg"})
        axes_cfg.append({"name": f"{self.name}_bragg", "tags": "bragg"})
        self._calc_mot = SpectroBraggCalcController(self, {"axes": axes_cfg})
        self._calc_mot._initialize_config()
        self._calc_mot._tagged["bragg"][0].low_limit = self.config.get(
            "bragg_low_limit", 15
        )
        self._calc_mot._tagged["bragg"][0].high_limit = self.config.get(
            "bragg_high_limit", 89
        )

        # create energy calc mot
        axes_cfg = []
        bragg_axis = self._calc_mot._tagged["bragg"][0]
        axes_cfg.append({"name": bragg_axis, "tags": "real bragg"})
        axes_cfg.append({"name": f"{self.name}_energy", "tags": "energy"})
        self._ene_calc = SpectroEnergyCalcController(self, {"axes": axes_cfg})
        self._ene_calc._initialize_config()

    def _init(self):
        # Called by the plugin via self._initialize_config
        # Called just after self._load_config

        """
        Place holder for any action to perform after the configuration has been loaded.
        """
        self._select_crystal(self._get_setting("crystal_sel"))
        self._update()

    def _get_plot_data(self):
        # === !!! plot in lab ref !!!
        colors = [
            [0, 212, 255],
            [0, 17, 255],
            [204, 0, 255],
            [255, 0, 42],
            [75, 0, 130],
            [238, 130, 238],
            [123, 104, 238],
            [153, 50, 204],
            [255, 127, 80],
            [224, 255, 255],
            [135, 206, 250],
            [70, 130, 180],
            [65, 105, 225],
            [30, 144, 255],
            [0, 97, 255],
            [93, 0, 255],
            [255, 0, 61],
        ]

        quivers = []
        plots = []
        scatters = []
        polygons = []
        scale = self.surface_radius_meridional / 5

        sx, sy, sz = self.sample_position

        # Spectrometer base vectors
        vectors = []
        vectors.append([sx, sy, sz, scale, 0, 0])  # ex
        vectors.append([sx, sy, sz, 0, scale, 0])  # ey
        vectors.append([sx, sy, sz, 0, 0, scale])  # ez
        quivers.append((transpose(vectors), {"color": "darkgray"}))

        # Beam vector ==============================
        # vectors = []
        # vectors.append([sx, -scale+sy, sz, 0, scale, 0])
        # quivers.append((transpose(vectors), {"color": "red"}))

        # Detector
        col = (0.6, 0.6, 0.8)  # "lightgreen"
        scatters.append((self.detector.position, {"color": col}))
        quivers.append(
            (
                list(self.detector.position) + list(self.detector.direction * scale),
                {"color": col},
            )
        )

        pts = get_normal_plane(
            self.detector.direction,
            self.detector.config.get("width", scale),
            self.detector.config.get("height", scale),
            self.detector.position,
        )

        polygons.append(
            (
                [[pts]],
                {"color": col, "linewidth": 0, "antialiased": False, "alpha": 0.5},
            )
        )

        # Analysers
        for id, ana in enumerate(self._active_analysers):

            # --- current positionning and ray tracing ----------------------
            col = list(numpy.array(colors[id % len(colors)]) / 255.0)
            pos = ana.position
            meri = ana.meridional
            mradius = ana.normal * self.surface_radius_meridional
            mcenter = pos + mradius
            dist = getnorm(pos - self.sample_position)
            refl = ana.reflected * dist * 1.2

            # ray tracing: sample => analyser => reflection
            plots.append(
                (
                    transpose([[sx, sy, sz], list(pos), list(pos + refl)]),
                    {
                        "color": "red",
                        "linestyle": "solid",
                        "linewidth": 0.8,
                        "alpha": 0.3,
                    },
                )
            )

            # meridional plan center and normal
            # quivers.append((list(mcenter) + list(meri * scale), {"color": col}))

            # rowland and sagittal circles
            rowland_circles = []
            for theta in range(361):
                rowland_circles.append(
                    numpy.dot(rotation_matrix(meri, theta), mradius) + mcenter
                )
            plots.append(
                (
                    transpose(rowland_circles),
                    {"color": col, "linestyle": "dotted", "linewidth": 0.8},
                )
            )

            # meridional circle radius
            plots.append(
                (
                    transpose([pos, mcenter, mcenter + mradius]),
                    {"color": col, "linestyle": "dotted", "linewidth": 0.8},
                )
            )

            # Analyser current direction
            x, y, z = pos
            u, v, w = ana.normal * scale * 2
            quivers.append(([x, y, z, u, v, w], {"color": col}))

            # ==== current bragg solution =====
            if self._current_bragg_solution:
                [Ai, Di, Ri, Nia, Nid, pitch, yaw] = self._current_bragg_solution[1][
                    ana.name
                ]

                # express solution in lab ref
                Ai = Ai + self.sample_position
                Di = Di + self.sample_position
                Ri = Ri + self.sample_position

                # Detector position and Rowland center
                scatters.append((transpose([Di, Ri]), {"color": col}))

                # Detector direction
                # x, y, z = Di
                # u, v, w = Nid*scale
                # quivers.append(([x, y, z, u, v, w], {"color": col}))

                # Analyser direction
                # x, y, z = Ai
                # u, v, w = Nia * scale
                # quivers.append(([x, y, z, u, v, w], {"color": col}))

                # samlpe to detector
                plots.append(
                    (
                        transpose([[sx, sy, sz], Di]),
                        {"color": col, "linestyle": "dotted", "linewidth": 0.8},
                    )
                )

        return {
            "quivers": quivers,
            "plots": plots,
            "scatters": scatters,
            "polygons": polygons,
        }

    def _compute_central_analyser_solution(self, bragg):

        """Compute the position of the virtual central analyser and the corresponding detector and rowland circle centers positions.

        It assumes that the central analyser is always along the X axis of the spectrometer referential (i.e y0=0 and z0=0).

        Args:
            bragg: bragg angle in degree
        Return:
            [A0, D0, R0]: a list of 3 vectors for the positions of [Analyser, Detector, Rowland]
        """

        # ===== !!! in spectro referential !!! ===============

        bragg = numpy.deg2rad(bragg)
        miscut = numpy.deg2rad(self.miscut)
        angm = bragg - miscut
        angp = bragg + miscut

        rad = 2 * self.surface_radius_meridional  # Rowland circle

        A0x = rad * numpy.sin(angm)
        A0y = 0
        A0z = 0

        D0x = rad * numpy.cos(angp) * numpy.sin(2 * bragg)
        D0y = 0
        D0z = rad * numpy.sin(angp) * numpy.sin(2 * bragg)

        R0x = rad * numpy.sin(angm) / 2
        R0y = 0
        R0z = rad * numpy.cos(angm) / 2

        A0 = numpy.array([A0x, A0y, A0z])
        D0 = numpy.array([D0x, D0y, D0z])
        R0 = numpy.array([R0x, R0y, R0z])

        X0 = numpy.array([1, 0, 0])
        Y0 = numpy.array([0, 1, 0])

        D0norm = getnorm(D0)
        D0z = D0[2]
        D0x = D0[0]
        D0x2 = D0[0] ** 2

        # N0a = D0 - 2 * A0
        # N0a = N0a / getnorm(N0a)
        N0a = numpy.dot(
            rotation_matrix(Y0, 90 - numpy.rad2deg(bragg) + self.miscut), X0
        )

        N0d = R0 - D0
        N0d = N0d / getnorm(N0d)

        return [A0, D0, R0, X0, Y0, N0a, N0d, D0x, D0z, D0norm, D0x2]

    def _compute_analyser_solution(
        self, ana, beta, central_solution, solution, reals_pos
    ):

        # ===== !!! in spectro referential !!! ===============

        A0, D0, R0, X0, Y0, N0a, N0d, D0x, D0z, D0norm, D0x2 = central_solution

        psi = numpy.arcsin(-ana.offset_on_detector / D0z)

        Di = numpy.dot(rotation_matrix(X0, numpy.rad2deg(psi)), D0)

        # use a.cosx + b.sinx = c =>   sinx(x+p) = c/sqrt(a**2+b**2) and sin(p) = a/sqrt(a**2+b**2)
        a = D0z * (D0z * numpy.tan(beta) - D0x * numpy.sin(psi))
        b = -D0z * D0norm * numpy.cos(psi)
        c = -D0x2 * numpy.tan(beta) - D0x * D0z * numpy.sin(psi)

        nab = numpy.sqrt(a * a + b * b)
        d = numpy.arcsin(c / nab)
        e = numpy.arcsin(a / nab)
        eta = e - d

        Ai = numpy.dot(rotation_matrix(Di, numpy.rad2deg(eta)), A0)

        Ri = numpy.dot(rotation_matrix(X0, numpy.rad2deg(psi)), R0)
        Ri = numpy.dot(rotation_matrix(Di, numpy.rad2deg(eta)), Ri)

        Nia = numpy.dot(rotation_matrix(X0, numpy.rad2deg(psi)), N0a)
        Nia = -numpy.dot(rotation_matrix(Di, numpy.rad2deg(eta)), Nia)

        Nid = numpy.dot(rotation_matrix(X0, numpy.rad2deg(psi)), N0d)
        Nid = numpy.dot(rotation_matrix(Di, numpy.rad2deg(eta)), Nid)

        Nia = normalize(Nia)
        Nid = normalize(Nid)

        d = spectro_to_analyser_matrix(Ai, ana._referential_mode) @ Nia
        pitch, yaw = direction_to_angles(d)

        solution[ana.name] = [Ai, Di, Ri, Nia, Nid, pitch, yaw]  # in spectro ref

        # === !!! real motors position expressed in lab ref !!!
        lab_pos = Ai + self.sample_position
        lab_rpos = numpy.sqrt(lab_pos[0] ** 2 + lab_pos[1] ** 2)
        dico = {
            "xpos": lab_pos[0],
            "ypos": lab_pos[1],
            "zpos": lab_pos[2],
            "rpos": lab_rpos,
            "pitch": pitch,
            "yaw": yaw,
        }

        for tag in ana.axes.keys():
            reals_pos[f"{ana.name}_{tag}"] = dico[tag]

    def _search_ypos(self, ypos, ana, central_solution, xstart=0, xstep=10, tol=1e-6):
        # ===== !!! ypos in spectro referential !!! ===============

        t0 = time.clock()

        if ypos < 0:
            direction = -1
        else:
            direction = 1

        beta = xstart
        while True:

            if time.clock() - t0 > 2:
                print("cant find solution in time")
                return None

            solution = {}
            reals_pos = {}
            self._compute_analyser_solution(
                ana, numpy.deg2rad(beta), central_solution, solution, reals_pos
            )
            fypos = solution[ana.name][0][1]  # !!! in spectro ref !!!

            if numpy.isnan(fypos):
                return None

            dy = ypos - fypos
            # print(ana.name, ypos, fypos, beta, dy)
            if abs(dy) < tol:
                return beta

            elif dy * direction > 0:
                beta += xstep * direction
                continue
            else:
                beta -= xstep * direction
                xstep = xstep / 10

    def compute_bragg_solution(self, bragg_angle):
        """Compute for all analysers the positions of:
            - Ai: analyser center
            - Di: associated beam position on detector
            - Ri: associated rowland circle center
            - Nia: the normal to analyser surface
            - Nid: the normal to detector surface (pointing toward Rowland center)
            - pitch: the analyser pitch angle (degree)
            - yaw: the analyser yaw angle (degree)

        Args:
            bragg: bragg angle in degree
        Return:
            (bragg, solution, reals_pos)
            with:
                - solution = { anaylser_name: [Ai, Di, Ri, Nia, Nid, pitch, yaw], ...
                               detector_name: [D0, N0d, pitch, yaw]
                             }
                - reals_pos = {real_name: real_pos, ...}
        """

        # ===== !!! in spectro referential !!! ===============

        # compute positioning of the virtual central analyser A0
        # wich is always along the X axis of the spectrometer referential (i.e y0=0 and z0=0)
        (
            A0,
            D0,
            R0,
            X0,
            Y0,
            N0a,
            N0d,
            D0x,
            D0z,
            D0norm,
            D0x2,
        ) = central_solution = self._compute_central_analyser_solution(bragg_angle)

        solution = {}
        reals_pos = {}

        # distinguish analysers geometries
        cartesian_analysers = []
        cylindrical_analysers = []
        for ana in self._active_analysers:
            if ana._referential_mode == REF_MODE_ENUM.CARTESIAN:
                cartesian_analysers.append(ana)
            elif ana._referential_mode == REF_MODE_ENUM.CYLINDRICAL:
                cylindrical_analysers.append(ana)
            else:
                raise ValueError(
                    f"unknown analyser geometry {ana.name}: {ana._referential_mode}"
                )

        # then compute solution for all analysers
        for ana in cylindrical_analysers:
            beta = numpy.deg2rad(ana.angular_offset_spectro_ref)
            self._compute_analyser_solution(
                ana, beta, central_solution, solution, reals_pos
            )

        # handle cartesian case with ypos fixed using a simple convergence algo ! (todo: get the proper calculations)
        for ana in cartesian_analysers:
            ypos = ana.ypos - self.sample_position[1]  # !!! ypos in spectro ref !!!
            beta = self._search_ypos(ypos, ana, central_solution)
            if beta is None:
                raise RuntimeError(
                    f"cannot find a solution for analyser {ana.name} with ypos {ypos}"
                )
            beta = numpy.deg2rad(beta)
            self._compute_analyser_solution(
                ana, beta, central_solution, solution, reals_pos
            )

        # detector solution
        target = self.detector_target
        if target not in self._active_analysers:
            target = None

        if self.detector_mode == "A":
            if target is None:
                vect = A0 - D0
            else:
                Ai, Di = solution[target.name][0:2]
                vect = Ai - Di

        elif self.detector_mode == "R":
            if target is None:
                vect = N0d
            else:
                Di, Ri = solution[target.name][1:3]
                vect = Ri - Di

        pitch, yaw = direction_to_angles(vect)

        solution[self.detector.name] = [D0, N0d, pitch, yaw]  # in spectro ref

        # === !!! real motors position expressed in lab ref !!!
        lab_D0 = D0 + self.sample_position
        reals_pos[f"{self.detector.name}_xpos"] = lab_D0[0]
        reals_pos[f"{self.detector.name}_ypos"] = lab_D0[1]
        reals_pos[f"{self.detector.name}_zpos"] = lab_D0[2]
        reals_pos[f"{self.detector.name}_pitch"] = pitch
        reals_pos[f"{self.detector.name}_yaw"] = yaw

        return (bragg_angle, solution, reals_pos)

    def _update_current_bragg_solution(self, bragg_angle):
        self._current_bragg_solution = self.compute_bragg_solution(bragg_angle)

    def _load_settings(self):
        """Get from redis the persistent spectrometer parameters (redis access)"""

        cached = {}
        cached["bragg_solution"] = self._settings.get("bragg_solution", None)
        cached["frozen_analysers"] = self._settings.get("frozen_analysers", [])
        cached["detector_target"] = self._settings.get("detector_target", None)
        cached["detector_mode"] = self._settings.get("detector_mode", "A")
        cached["crystal_sel"] = self._settings.get(
            "crystal_sel", self.config["crystal"]
        )
        cached["sample_position"] = self._settings.get("sample_position", [0, 0, 0])

        self._cached_settings = cached

    def _clear_settings(self):
        self._settings.clear()
        self._load_settings()

    def _get_setting(self, key):
        """Get a persistent spectrometer parameter from local cache (no redis access)"""
        return self._cached_settings[key]

    def _set_setting(self, key, value):
        """Store a persistent spectrometer parameter in redis and update local cache (redis access)"""
        self._settings[key] = value
        self._cached_settings[key] = value

    def _get_current_ref_analyser(self):
        """Return the closest analyser to the central solution A0"""
        ref = None
        refval = None
        for ana in self._active_analysers:
            if ref is None:
                ref = ana
                refval = abs(get_angle(ref.position - self.sample_position, [1, 0, 0]))
            elif (
                abs(get_angle(ana.position - self.sample_position, [1, 0, 0])) < refval
            ):
                ref = ana
        return ref

    def _get_all_reals(self):
        axes = []
        for ana in self._analysers:
            axes.extend(list(ana.axes.values()))
        axes.extend(list(self.detector.axes.values()))
        return axes

    def _get_active_real_mot_positions(self):
        """Return the current position of all real motors (analysers and detector axes)"""
        positions_dict = {}
        for ana in self._active_analysers:
            for tag, ax in ana.axes.items():
                positions_dict[f"{ana.name}_{tag}"] = ax.position

        for tag, ax in self.detector.axes.items():
            positions_dict[f"{self.detector.name}_{tag}"] = ax.position

        return positions_dict

    def _get_theo_real_mot_positions(self, filter_name=None):
        """Return the theoritical position of real motors.

        Use filter_name to select only motors starting with this name.
        """
        if filter_name is None:
            return self._current_bragg_solution[2]
        else:
            reals = {}
            for k, v in self._current_bragg_solution[2].items():
                if k.startswith(filter_name):
                    reals[k] = v
            return reals

    def _get_current_bragg_value(self):
        if self._current_bragg_solution:
            return self._current_bragg_solution[0]
        else:
            return numpy.nan

    def _check_alignement(self):
        """compare current solution to current motor position"""
        aligned = False
        txt_info = ""
        bragg = numpy.nan

        if self._current_bragg_solution:
            aligned = True
            bragg = self._current_bragg_solution[0]
            theo_real_pos = self._get_theo_real_mot_positions()
            curr_real_pos = self._get_active_real_mot_positions()
            for tag in curr_real_pos.keys():
                if tag in theo_real_pos.keys():
                    delta = abs(curr_real_pos[tag] - theo_real_pos[tag])
                    if delta > self._align_tolerance:
                        txt_info = f"{tag} not aligned: {curr_real_pos[tag]} - {theo_real_pos[tag]} = {delta}"
                        aligned = False
                        bragg = numpy.nan
                        break

        self._align_info = txt_info
        return aligned, bragg

    def _select_crystal(self, crys):
        """Select the current crystal (used to compute the spectrometer energy).
        args:
          - crys: a string (ex: 'Si844') or a distance (ex: 5.543013043019761e-11)
        """
        if not crys:  # use the global spectrometer crystal parameter as default
            crys = self.config["crystal"]

        if isinstance(crys, str):
            self._crystal = CrystalPlane.fromstring(crys)
        else:
            self._crystal = MultiPlane(float(crys))

        self._set_setting("crystal_sel", crys)

    @property
    def _active_analysers(self):
        """Return the active analysers.

        Frozen analysers are discareded whatever the crystal type.
        Analysers that doesn't match the current crystal selection are discarded.
        """
        actives = []
        frozen = self._get_setting("frozen_analysers")
        selcrys = self._get_setting("crystal_sel")
        for ana in self._analysers:
            crys = ana.config.get("crystal") or self.config.get("crystal")
            if ana.name not in frozen:
                if crys == selcrys:
                    actives.append(ana)
        return actives

    @property
    def _current_bragg_solution(self):
        return self._get_setting("bragg_solution")

    @_current_bragg_solution.setter
    def _current_bragg_solution(self, value):
        self._set_setting("bragg_solution", value)

    @property
    def detector_target(self):
        return self._get_setting("detector_target")

    @detector_target.setter
    def detector_target(self, target):
        """Select the target for the detector alignment.

        The detector surface normal will point to that target.
        It also depends on the 'detector_mode' which decides
        if it points to the analyser center or associated rowland center.

        Possibe values for the target:
          - None: point to the virtual central analyser (default)
          - analyser: one of the spectrometer analysers
        """

        if (target is not None) and (target not in self.analysers):
            raise ValueError("target must be None or one of the spectrometer analysers")

        self._set_setting("detector_target", target)

        self._update()

    @property
    def detector_mode(self):
        return self._get_setting("detector_mode")

    @detector_mode.setter
    def detector_mode(self, mode):
        """Select the mode for the detector alignment.

        The detector surface normal will point to that target point.
        Possibe values for the mode:
          - 'A': point to the center of an analyser (default) (see self.detector_target to select the analyser)
          - 'R': point to the Rowland center of an analyser   (see self.detector_target to select the analyser)
        """
        if mode not in ["A", "R"]:
            raise ValueError("mode must be 'A' or 'R' ")

        self._set_setting("detector_mode", mode)

        self._update()

    @property
    def sample_position(self):
        return numpy.array(self._get_setting("sample_position"))

    @sample_position.setter
    def sample_position(self, sample_coords):
        """Define the position [x, y, z] of the sample in the laboratory referential"""
        if len(sample_coords) != 3:
            raise ValueError(
                f"sample coordinates must be a vector [x, y, z] not {sample_coords}"
            )
        self._set_setting("sample_position", sample_coords)
        self._update()

    @property
    def is_aligned(self):
        return self._check_alignement()[0]

    @autocomplete_property
    def analysers(self):
        return counter_namespace(self._analysers)

    @autocomplete_property
    def detector(self):
        return self._detector

    @autocomplete_property
    def crystal(self):
        return self._crystal

    @crystal.setter
    def crystal(self, value):
        self._select_crystal(value)
        self._update()
        if not numpy.isnan(self.bragg_axis.position):
            dial_pos = self.bragg2energy(self.bragg_axis.position)
            user_pos = self.energy_axis.dial2user(dial_pos)
            self.energy_axis.settings.set("dial_position", dial_pos)
            self.energy_axis.settings.set("position", user_pos)

    @property
    def surface_radius_meridional(self):
        return self._surface_radius_meridional

    @surface_radius_meridional.setter
    def surface_radius_meridional(self, value):
        self._surface_radius_meridional = value
        self._update()

    @property
    def miscut(self):
        return self._miscut

    @miscut.setter
    def miscut(self, value):
        self._miscut = value
        self._update()

    @autocomplete_property
    def energy_axis(self):
        return self._ene_calc._tagged["energy"][0]

    @autocomplete_property
    def bragg_axis(self):
        return self._calc_mot._tagged["bragg"][0]

    @property
    def plot(self):
        if self._plot is None:
            self._plot = SpectroPlot(self)
            self._connect_to_axes_moves()
            self._plot_finalizer = weakref.finalize(self._plot, self._disconnect_axes)

        if not self._plot.is_active():
            self._plot.create_plot()

        self._plot.update_plot(forced=True)

        return self._plot

    def _connect_to_axes_moves(self):
        if not self._plot_axes_connected:
            for ax in self._get_all_reals():
                event.connect(ax, "position", self._update_plot)
                event.connect(ax, "move_done", self._update_plot_on_move_done)
            self._plot_axes_connected = True

    def _disconnect_axes(self):
        if self._plot_axes_connected:
            for ax in self._get_all_reals():
                event.disconnect(ax, "position", self._update_plot)
                event.disconnect(ax, "move_done", self._update_plot)
            self._plot_axes_connected = False

    def _update_plot_on_move_done(self, position=None, sender=None):
        if position is True:
            self._update_plot(position, sender, forced=True)

    def _update_plot(self, position=None, sender=None, forced=False):
        if self._plot:
            self._plot.update_plot(forced)

    def _update(self):
        """Recompute the solution (using current bragg value) and update the plot.

        To be used after a parameter has been changed.
        """
        if self._current_bragg_solution:
            self._update_current_bragg_solution(self._current_bragg_solution[0])
        self._update_plot(forced=True)

    def energy2bragg(self, energy):
        return self.crystal.bragg_angle(energy * ur.keV).to(ur.deg).magnitude

    def bragg2energy(self, bragg):
        return self.crystal.bragg_energy(bragg * ur.deg).to(ur.keV).magnitude

    def freeze(self, *analysers):
        frozen = self._get_setting("frozen_analysers")

        for ana in analysers:
            aname = None
            if isinstance(ana, str):
                if ana in [x.name for x in self._analysers]:
                    aname = ana
            elif ana in self._analysers:
                aname = ana.name

            if aname:
                frozen.append(aname)
            else:
                print(f"cannot freeze unknown analyser: '{ana}'")

        self._set_setting("frozen_analysers", frozen)
        self._update()

    def unfreeze(self, *analysers):
        frozen = self._get_setting("frozen_analysers")

        for ana in analysers:
            aname = None
            if isinstance(ana, str):
                if ana in [x.name for x in self._analysers]:
                    aname = ana
            elif ana in self._analysers:
                aname = ana.name

            if aname:
                frozen.remove(aname)
            else:
                print(f"cannot unfreeze unknown analyser: '{ana}'")

        self._set_setting("frozen_analysers", frozen)
        self._update()

    def scan_metadata(self):
        meta_dict = {"@NX_class": "NXcollection"}
        meta_dict["crystal"] = str(self.crystal)
        meta_dict["miscut"] = self.miscut
        meta_dict["meridional_radius"] = self.surface_radius_meridional
        meta_dict["sample_position"] = self.sample_position
        return meta_dict

    @autocomplete_property
    def frozen(self):
        return self._get_setting("frozen_analysers")

    def check_energy(self, energy):
        self.check_bragg(self.energy2bragg(energy))

    def check_bragg(self, bragg):
        reals_pos = self.compute_bragg_solution(bragg)[2]
        energy = self.bragg2energy(bragg)

        txt = f"\n=== Solution @bragg {bragg:.4f} degree ({energy:.4f}kev) ===\n\n"

        txt += f" crystal: {self.crystal}\n"
        txt += f" miscut: {self.miscut}\n"
        txt += f" meridional curvature: {self.surface_radius_meridional}\n"
        txt += "\n"

        # === analysers info
        last_head = ""
        for ana in self._active_analysers:

            head, pos = ["Analyser"], [ana.name]
            h, p = zip(
                *[(tag, reals_pos[f"{ana.name}_{tag}"]) for tag in ana.axes.keys()]
            )

            head.extend(h)
            head.extend(["det_off"])
            pos.extend(p)
            pos.extend([ana.offset_on_detector])
            tab = IncrementalTable(
                [head, pos], fpreci=FLOAT_PRECISION, dtype=FLOAT_FORMAT
            )

            tab.resize(minwidth=12, maxwidth=100)
            tab.add_separator(sep="-", line_index=1)
            lines = str(tab).split("\n")

            if lines[0] == last_head:
                txt += "\n".join(lines[2:]) + "\n"
            else:
                txt += "\n".join(lines) + "\n"
                last_head = lines[0]

        # === detector info
        head, pos = ["Detector"], [self.detector.name]
        h, p = zip(
            *[
                (tag, reals_pos[f"{self.detector.name}_{tag}"])
                for tag in self.detector.axes.keys()
            ]
        )
        head.extend(h)
        pos.extend(p)
        tab = IncrementalTable([head, pos], fpreci=FLOAT_PRECISION, dtype=FLOAT_FORMAT)
        tab.resize(minwidth=12, maxwidth=100)
        tab.add_separator(sep="-", line_index=1)
        txt += "\n".join(["", str(tab)])

        txt += "\n"
        print(txt)

    def park(self):
        """Move analysers to the parking position (defined in config)"""
        args = []
        for ana in self.analysers:
            args.extend(ana._get_park_args())
        if len(args):
            umv(*args)


class BraggCalcController(CalcController):
    def __init__(self, spectro, cfg):
        super().__init__(cfg)
        self._spectro = spectro

    def axes_are_moving(self):
        """Check if pseudo axis are moving"""
        return not self._real_move_is_done


class SpectroBraggCalcController(BraggCalcController):
    def calc_to_real(self, positions_dict):
        """bragg to {bragg_i} (active analysers and detector)"""
        reals = {}
        bragg = positions_dict["bragg"]
        self._spectro._update_current_bragg_solution(bragg)  # compute solution
        for ana in self._spectro._active_analysers:
            reals[f"{ana.name}_bragg"] = bragg
        reals[f"{self._spectro.detector.name}_bragg"] = bragg
        return reals

    def calc_from_real(self, positions_dict):
        if self.axes_are_moving():
            bragg = self._spectro._get_current_ref_analyser().geo_bragg
        else:
            braggi = [
                positions_dict[f"{ana.name}_bragg"]
                for ana in self._spectro._active_analysers
            ]
            braggi.append(positions_dict[f"{self._spectro.detector.name}_bragg"])
            if len(set(braggi)) == 1:  # if all equal
                bragg = braggi[0]
            else:
                bragg = numpy.nan
        return {"bragg": bragg}


class AnalyserBraggCalcController(BraggCalcController):
    def __init__(self, analyser, cfg):
        self._analyser = analyser
        super().__init__(analyser._spectro, cfg)

    def calc_to_real(self, positions_dict):
        """bragg_i to real motors"""
        ana_bragg = positions_dict["bragg"]
        curr_bragg = self._spectro._get_current_bragg_value()
        if ana_bragg != curr_bragg:  # when moving just this analyser bragg axis
            self._spectro._update_current_bragg_solution(ana_bragg)  # compute solution
        return self._spectro._get_theo_real_mot_positions(self._analyser.name)

    def calc_from_real(self, positions_dict):
        """real motors to bragg_i"""
        if self.axes_are_moving():
            bragg = self._analyser.geo_bragg
        else:
            if self._spectro._current_bragg_solution is None:
                return {"bragg": numpy.nan}

            bragg = self._spectro._current_bragg_solution[0]
            theo_pos = self._spectro._get_theo_real_mot_positions(self._analyser.name)
            for k, v in positions_dict.items():
                pos = theo_pos.get(k)
                if pos is None:
                    return {"bragg": numpy.nan}
                if abs(pos - v) > self._spectro._align_tolerance:
                    return {"bragg": numpy.nan}

        return {"bragg": bragg}


class DetectorBraggCalcController(BraggCalcController):
    def __init__(self, detector, cfg):
        self._detector = detector
        super().__init__(detector._spectro, cfg)

    def calc_to_real(self, positions_dict):
        """bragg_i to real motors"""
        det_bragg = positions_dict["bragg"]
        curr_bragg = self._spectro._get_current_bragg_value()
        if det_bragg != curr_bragg:  # when moving just the detector bragg axis
            self._spectro._update_current_bragg_solution(det_bragg)  # compute solution
        return self._spectro._get_theo_real_mot_positions(self._detector.name)

    def calc_from_real(self, positions_dict):
        """real motors to bragg_i"""
        if self.axes_are_moving():
            bragg = self._spectro._get_current_ref_analyser().geo_bragg
        else:
            if self._spectro._current_bragg_solution is None:
                return {"bragg": numpy.nan}

            bragg = self._spectro._current_bragg_solution[0]
            theo_pos = self._spectro._get_theo_real_mot_positions(self._detector.name)
            for k, v in positions_dict.items():
                pos = theo_pos.get(k)
                if pos is None:
                    return {"bragg": numpy.nan}
                if abs(pos - v) > self._spectro._align_tolerance:
                    return {"bragg": numpy.nan}
        return {"bragg": bragg}


class SpectroEnergyCalcController(CalcController):
    def __init__(self, spectro, cfg):
        super().__init__(cfg)
        self._spectro = spectro

    def calc_to_real(self, positions_dict):
        return {"bragg": self._spectro.energy2bragg(positions_dict["energy"])}

    def calc_from_real(self, positions_dict):
        return {"energy": self._spectro.bragg2energy(positions_dict["bragg"])}


class SpectroPlot:
    def __init__(self, spectro):
        self._spectro = spectro
        self._min_refresh_time = 0.2
        self._last_refresh_time = time.time()
        self.create_plot()

    def create_plot(self):
        self.plot = get_flint().get_plot(
            "spectroplot", self._spectro.name, self._spectro.name, selected=True
        )
        d = self._spectro.surface_radius_meridional * 2
        self.plot.set_box_min_max([-d, -d, -d], [d, d, d])
        self.plot.set_data(**self._spectro._get_plot_data())

    def is_active(self):
        return self.plot.is_open()

    def update_plot(self, forced=False):
        if self.plot.is_open():
            now = time.time()
            dt = now - self._last_refresh_time
            if forced or dt >= self._min_refresh_time:
                self._last_refresh_time = now
                self.plot.set_data(**self._spectro._get_plot_data())
