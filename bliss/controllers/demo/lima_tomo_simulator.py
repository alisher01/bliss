import time
import numpy
import h5py
import tango

from bliss.common import greenlet_utils
from Lima import Simulator as LimaSimuMod
import Lima.Server.camera.Simulator as TangoSimuMod


#
# This is a plugin loaded by LimaCCDs
# As BLISS patch subprocess, this have to be reverted
# To make LimaCCDs working
#

greenlet_utils.unpatch(subprocess=True)


class LimaTomoSimulator(LimaSimuMod.Camera):
    def __init__(self):
        LimaSimuMod.Camera.__init__(self)
        self._dark = None
        self._flat = None
        self._data = None
        self._data_filename = None
        self._loaded = False

    def set_data_filename(self, data_filename):
        self._data_filename = data_filename

    def init_img(self):
        """Initialize resources.

        It is not done during object construction to speed up
        the device initialization.
        """
        if self._loaded:
            return
        self._loaded = True
        with h5py.File(self._data_filename, mode="r") as f:
            self._dark = f["scan1/dark/data"][...].astype(float)
            self._flat = f["scan1/flat_000/data"][...].astype(float)
            self._data = f["scan1/instrument/data"][0].astype(float)

    def fillData(self, data):
        """
        Callback function from the simulator
        """
        self.init_img()

        # Inverse ramp from 100% to 20% restarting back every 'duration' seconds
        duration = 20
        intensity = int(time.time()) % duration
        intensity = 0.1 + 0.9 * (duration - intensity) / duration

        image = numpy.random.poisson(self._data) * intensity

        # Clamp the image to the result data
        width = min(data.buffer.shape[0], image.shape[0])
        height = min(data.buffer.shape[1], image.shape[1])
        data.buffer[...] = 0
        data.buffer[0:width, 0:height] = image[0:width, 0:height]


class TomoSimulator(TangoSimuMod.Simulator):
    def init_device(self):
        TangoSimuMod.Simulator.init_device(self)
        self._SimuCamera.set_data_filename(self.data_filename)


class TomoSimulatorClass(TangoSimuMod.SimulatorClass):
    # Device Properties
    device_property_list = {
        "data_filename": [tango.DevString, "filename containing the data", []]
    }
    device_property_list.update(TangoSimuMod.SimulatorClass.device_property_list)


def get_control(**kwargs):
    return TangoSimuMod.get_control(
        **kwargs, _Simulator=TomoSimulator, _Camera=LimaTomoSimulator
    )


def get_tango_specific_class_n_device():
    return TomoSimulatorClass, TomoSimulator
