import logging
from typing import Union, Optional
from bliss.config.static import get_config
from bliss.icat.client.main import IcatClient
from bliss.icat.client.null import IcatNullClient


logger = logging.getLogger(__name__)


def icat_client_is_disabled() -> bool:
    config = get_config().root.get("icat_servers")
    if config:
        return config.get("disable", False)
    else:
        return True


def icat_client_config(
    bliss_session: str, beamline: str, proposal: Optional[str] = None
) -> dict:
    config = get_config().root.get("icat_servers")
    if config:
        config = dict(config)
        if proposal is not None:
            config["proposal"] = proposal
        config["beamline"] = beamline
        disable = config.pop("disable", False)
        return {"disable": disable, "kwargs": config}
    else:
        return {"disable": True}


def icat_client_from_config(config: dict):
    if config["disable"]:
        return IcatNullClient(expire_datasets_on_close=False)
    else:
        return IcatClient(**config.get("kwargs", dict()))


def is_null_client(icat_client: Union[IcatClient, IcatNullClient]):
    return isinstance(icat_client, IcatNullClient)
