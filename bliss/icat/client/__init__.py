"""Currently two ICAT clients exist with the same API:

 1. `bliss.icat.client.IcatClient`: communicates with the ICAT message broker and REST API
 2. `bliss.icat.client.IcatNullClient`: no ICAT communication

`IcatClient` is used when configured in the beacon configuration

    icat_servers:
        metadata_urls: [URL1, URL2]
        elogbook_url: URL3
        elogbook_token: elogbook-00000000-0000-0000-0000-000000000000

The default URL prefix is "tcp://" for the metadata url's and "https://"
for the e-logbook.

When not configured BLISS will fall back to `IcatNullClient`.
"""

from .config import icat_client_is_disabled  # noqa: F401
from .config import icat_client_config  # noqa: F401
from .config import icat_client_from_config  # noqa: F401
from .config import is_null_client  # noqa: F401
from .interface import DatasetId  # noqa: F401
